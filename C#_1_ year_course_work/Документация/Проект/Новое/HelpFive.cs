﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class HelpFive : Form
    {
        public static Form help6 = new HelpSix();
        public HelpFive()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //предыдущая форма
            this.Hide();
            HelpThird.help4.Show();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //следующая форма
            this.Hide();
            help6.Show();
        }
        private void HelpFive_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void HelpFive_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
