﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class HelpSix : Form
    {
        public HelpSix()
        {
            InitializeComponent();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //следующая форма
            this.Hide();
            Form q = new HelpSeven();
            q.Show();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //предыдущая форма
            this.Hide();
            HelpFour.help5.Show();           
        }
        private void HelpSix_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void HelpSix_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
