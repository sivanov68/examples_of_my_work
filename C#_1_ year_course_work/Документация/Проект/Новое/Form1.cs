﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class Form1 : Form
    {
        public static Form fn2s = new Form2s();
        public static Form Perv;
        public static Form l = new Change();
        public Form1()
        {
            InitializeComponent();
        }
        Form q = new Alloperators();//
        Form w = new FormIns();
        Form p = new Helpstart();
        public static int nomer = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            //выбран обчный режим
            if (radioButton1.Checked == true)
            {
                q.Show();
                Perv.Hide();
            }
            //выбран экспертный режим
            if (radioButton2.Checked == true)
            {
                fn2s.Show();
                nomer = 2;
                Perv.Hide();
                
            }
            //выбран экспертный режим (текстовый)
            if (radioButton3.Checked == true)
            {
                w.Show();
                Perv.Hide();
                
            }
            //выбран режим работы с памятью
            if (radioButton4.Checked == true)
            {
                p.Show();
                nomer = 1;
                Perv.Hide();
                
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //открытие формы сравнения режимов
            button1.Focus();
            l.Show();            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Perv получает ссылку на эту форму
            Perv = this;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}