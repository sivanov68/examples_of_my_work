﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class vivod1 : Form
    {
        public vivod1()
        {
            InitializeComponent();
            listBox1.Select();
            textBox1.Text = Form2l.stroka1.ToString();
            textBox4.Text = Form2l.stroka4.ToString();
            int a = 0;
            //заполнение листбокса
            for (int i = 0; i < Form2l.stroka4.Length; i++)
            {
                if (a == 1) a = 0;
                a += 16;
                string b = "";
                string h = "";
                h = K.kod(Form2l.stroka4[i]);
                b = (i + 1) + ")" + h + "  \t" + "Элемент строки " + "  '" + Form2l.stroka4[i] + "'  " + "\t" +
                "Номер первого элемента=" + (a - 15) + " \t Конечный номер элемента=" + a;
                listBox1.Items.Add(b);
            }
            label3.Text = Form2l.oper;
            label8.Text = Form2l.firstpos.ToString();
            label10.Text = Form2l.lastpos.ToString();
            //где произошли изменения
            if (Form2l.firstpos % 16 == 0) a = 0; else a = 1;
            int p = 0;
            if (Form2l.lastpos % 16 != 0) p = 1; else p = 0;
            if ((Form2l.firstpos / 16 + a) != (Form2l.lastpos / 16 + p))
                label1.Text = "Изменения произошли c " + (Form2l.firstpos / 16 + a) + " по " + (Form2l.lastpos / 16 + p) + " элементы";
            else label1.Text = "Изменения произошли в " + (Form2l.firstpos / 16 + a) + " элементе";
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Alloperators.form2l.Show();
            Form2l.stroka1 = null;//входная строка
            Form2l.stroka2 = null;//входная строка в двоичной коде
            Form2l.stroka3 = null;//промежуточная строка выполнения
            Form2l.oper = null;
            Form2l.strokaposlevipoln = null;
            Form2l.firstpos = 0;
            Form2l.lastpos = 0;
            Form2l.nomera = null;
            Form2l.stroka4 = null;
            Form2l.changeelement = null;
            Form2l.fileornot = false;
            Form2l.filename = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //закрытие родительской формы
            this.Close();
            Form1.Perv.Close();
        }

        private void vivod1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void vivod1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}