﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class perestanovka : Form
    {
        public perestanovka()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://yandex.ru/yandsearch?text=%D0%BF%D0%B5%D1%80%D0%B5%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0+%D0%B1%D0%B8%D1%82&clid=1783294&lr=213");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Form1.nomer == 1)//режим работа с памятью
            {
                try
                {
                    Form2l.oper = "< ... >";
                    Form4.strokaposle = K.perestanovka(Form4.strokado, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                            Form q = new Form4viv();
                            q.Show();
                            this.Hide();                        
                }
                catch
                {
                    MessageBox.Show("Вы ввели неверные данные.", "Ошибка входных данных", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            if (Form1.nomer == 2)//экспертный режим
            {
                try
                {
                    Form2l.oper = "< ... >";
                    Form2l.strokaposlevipoln = K.perestanovka(Form2l.stroka2, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                    Form q = new vivod1s();
                    q.Show();
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("Вы ввели неверные данные.", "Ошибка входных данных", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            if (Form1.nomer == 0)//обычный режим работы
            {
                try
                {
                    Form2l.oper = "< ... >";
                    Form2l.strokaposlevipoln = K.perestanovka(Form2l.stroka2, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                    Form q = new vivod1s();
                    q.Show();
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("Вы ввели неверные данные.", "Ошибка входных данных", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void perestanovka_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void perestanovka_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
