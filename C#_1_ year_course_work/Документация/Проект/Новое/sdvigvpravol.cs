﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class sdvigvpravol : Form
    {
        public sdvigvpravol()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%91%D0%B8%D1%82%D0%BE%D0%B2%D1%8B%D0%B5_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8#.D0.9B.D0.BE.D0.B3.D0.B8.D1.87.D0.B5.D1.81.D0.BA.D0.B8.D0.B9_.D1.81.D0.B4.D0.B2.D0.B8.D0.B3");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //проверка на нулевой сдвиг
            if ((int)numericUpDown1.Value == 0)
            {
                MessageBox.Show("Количество сдвигов не может быть равно 0", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (Form1.nomer == 1)//режим работа с памятью
                {
                    int a = 0, b = 0;
                    bool fl = true;
                    if (Form2l.firstpos == 0)
                    {
                        a = 0;
                        b = Form4.strokado.Length;
                    }
                    else
                    {
                        a = Form2l.firstpos;
                        b = Form2l.lastpos;
                    }
                    for (int i = a; i < b; i++)
                        if (i % 8 != 0)
                        {
                        }
                        else
                        {
                            if (Form4.strokado[i] == '1') fl = false;
                        }
                    //переключатель между "вдвигом" нулей и единиц
                    if (fl == false && Form4.nomer1naz != "System.Single" && Form4.nomer2naz != "System.Single" && Form4.nomer3naz != "System.Single")
                    {
                        var result = MessageBox.Show("Данная операция (сдвиг вправо двоичного представления целого ЗНАКОВОГО числа) реализована таким образом, что при наличии в знаковом разряде 1, слева зачастую ‘вдвигаются’ единицы (могут ‘вдвигаться’ и нули – это зависит от реализации и используемой платформы). Так как Вы совершаете операцию над значением ЗНАКОВОГО типа, рекомендуется использовать вариант ‘вдвига’ единиц.\n Выберите ответ ‘Да’, если Вы согласны с этим (при выборе ответа ‘Нет’ будут дописываться нули).", "Побитовые операции : Предупреждение",
                                     MessageBoxButtons.YesNo,
                                     MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            Form4.strokaposle = K.sdvigvpravos1(Form4.strokado, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                            Form q = new Form4viv();
                            q.Show();
                            this.Hide();
                        }
                        if (result == DialogResult.No)
                        {
                            Form4.strokaposle = K.sdvigvpravo(Form4.strokado, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                            Form q = new Form4viv();
                            q.Show();
                            this.Hide();
                        }
                    }
                    else
                    {
                        Form2l.oper = ">>";
                        Form4.strokaposle = K.sdvigvpravo(Form4.strokado, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                        Form q = new Form4viv();
                        q.Show();
                        this.Hide();
                    }
                }
                if (Form1.nomer == 2)//экспертный режим
                {
                    Form2l.oper = ">>";
                    Form2l.strokaposlevipoln = K.sdvigvpravo(Form2l.stroka2, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                    Form q = new vivod1s();
                    q.Show();
                    this.Hide();
                }
                if (Form1.nomer == 0)//обычный режим работы
                {
                    Form2l.oper = ">>";
                    Form2l.strokaposlevipoln = K.sdvigvpravo(Form2l.stroka2, ref Form2l.firstpos, ref Form2l.lastpos, (int)numericUpDown1.Value);
                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                    Form q = new vivod1();
                    q.Show();
                    this.Hide();
                }
            }
        }
        private void sdigvparol_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void sdvigvpravol_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

    }
}