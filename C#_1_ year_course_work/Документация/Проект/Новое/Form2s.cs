﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class Form2s : Form
    {
        public Form2s()
        {
            InitializeComponent();
            textBox1.Select();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //очистить входную строку
            textBox1.Text = "";
            textBox1.Focus();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string h;
            int ch = 0;
            h = textBox1.Text;
            //проверка на пустые данные
            if ((h == "") || (ch == h.Length))
            {
                MessageBox.Show("Вы ничего не ввели в поле ввода", "Пустые данные", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox1.Select();
            }
            else
            {
                Form2l.stroka1 = textBox1.Text;
                Form2l.nomera = new int[Form2l.stroka1.Length];
                for (int i = 0; i < Form2l.stroka1.Length; i++)
                {
                    Form2l.stroka3 = K.kod(Form2l.stroka1[i]);
                    Form2l.nomera[i] = 16;
                    Form2l.stroka2 = Form2l.stroka2 + Form2l.stroka3;
                }
                Form q = new Form3s();
                q.Show();
                textBox1.Text = "";
                textBox1.Select();
                this.Hide();
            }
        }

        private void Form2s_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void Form2s_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
