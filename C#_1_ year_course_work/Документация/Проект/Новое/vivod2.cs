﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class vivod2 : Form
    {
        public vivod2()
        {
            InitializeComponent();
            textBox1.Text = Form2l.stroka1.ToString();
            textBox3.Text = Form2l.stroka4.ToString();
            textBox2.Text = Form2l.stroka2.ToString();
            textBox2.Select();
            int a = 0;
            //где произошли изменения
            if (Form2l.fileornot == false)
            {
                if (Form2l.firstpos % 16 == 0) a = 0; else a = 1;
                int p = 0;
                if (Form2l.lastpos % 16 != 0) p = 1; else p = 0;
                if ((Form2l.firstpos / 16 + a) != (Form2l.lastpos / 16 + p))
                    label4.Text = "Изменения произошли c " + (Form2l.firstpos / 16 + a) + " по " + (Form2l.lastpos / 16 + p) + " элементы";
                else label4.Text = "Изменения произошли в " + (Form2l.firstpos / 16 + a) + " элементе";
                label3.Text = Form2l.oper;
                label8.Text = Form2l.firstpos.ToString();
                label10.Text = Form2l.lastpos.ToString();
            }
            else
            {
                //было считывание из файла
                label4.Text = "Изменения произошли со следующими номерами элементов: " + Form2l.changeelement.ToString();
                label3.Text = "Смотри файл";
                label8.Text = "Смотри файл";
                label10.Text = "Смотри файл";
            }
           
        }

        private void label3_Click(object sender, EventArgs e)
        {
            //открыть входной файл
            System.Diagnostics.Process.Start(Form3s.file);
        }

        private void label8_Click(object sender, EventArgs e)
        {
            //открыть входной файл
            System.Diagnostics.Process.Start(Form3s.file);
        }

        private void label10_Click(object sender, EventArgs e)
        {
            //открыть входной файл
            System.Diagnostics.Process.Start(Form3s.file);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form1.Perv.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1.fn2s.Show();
            Form2l.stroka1 = null;
            Form2l.stroka2 = null;
            Form2l.stroka3 = null;
            Form2l.oper = null;
            Form2l.strokaposlevipoln = null;
            Form2l.firstpos = 0;
            Form2l.lastpos = 0;
            Form2l.nomera = null;
            Form2l.stroka4 = null;
            Form2l.changeelement = null;
            Form2l.fileornot = false;
            Form2l.filename = null;
        }

        private void vivod2_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void vivod2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
