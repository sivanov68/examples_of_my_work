﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class Form2l : Form
    {
        public static string stroka1 = "";
        public static string stroka2 = "";
        public static string stroka3 = "";
        public static string oper = "";
        public static string strokaposlevipoln = "";
        public static int firstpos;
        public static int lastpos;
        public static int[] nomera;
        public static string stroka4 = "";
        public static string changeelement = "";
        public static bool fileornot = false;
        public static string filename = "";
       
        public Form2l()
        {
            InitializeComponent();
            textBox1.Select();


        }
        private void button1_Click(object sender, EventArgs e)
        {
            //очистить входную строку
            textBox1.Text = "";
            textBox1.Focus();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            string h;
            int ch = 0;
            h = textBox1.Text;
            for (int i = 0; i < h.Length; i++)
                if (h[i] == ' ') ++ch;
            //проверка на пустые данные
            if ((h == "") || (ch == h.Length))
            {
                MessageBox.Show("Вы ничего не ввели в поле ввода", "Пустые данные", MessageBoxButtons.OK, MessageBoxIcon.Information);
                textBox1.Select();
            }
            else
            {
                stroka1 = textBox1.Text;
                nomera = new int[stroka1.Length];
                for (int i = 0; i < stroka1.Length; i++)
                {
                    stroka3 = K.kod(stroka1[i]);
                    nomera[i] = 16;
                    stroka2 = stroka2 + stroka3;
                }
                Form q = new Form3l();
                q.Show();
                textBox1.Select();
                this.Hide();
            }
            textBox1.Text = "";
        }

        private void Form2l_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void Form2l_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}