﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Новое
{
    public partial class Form3l : Form
    {
        public Form3l()
        {
            InitializeComponent();
            radioButton1.Checked = true;
            listBox1.Select();
            textBox1.Text = Form2l.stroka1;
            int a = 1;
            //заполнение листбокса
            for (int i = 0; i < Form2l.stroka1.Length; i++)
            {
                if (a == 1) a = 0;
                a += 16;
                string b = "";
                string h = "";
                h = K.kod(Form2l.stroka1[i]);
                b = (i + 1) + ")" + h + "  \t" + "Элемент строки " + "  '" + Form2l.stroka1[i] + "'  " + "\t" +
                "Номер первой позиции=" + (a - 15) + " \t Номер конечной позиции= " + a;
                listBox1.Items.Add(b);
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            long kolmax = 0;
            int i = 0;
            string str = "";
            do
            {
                try
                {
                    str = listBox1.Items[i].ToString();
                    if (str != "") i++;
                }
                catch
                {
                    break;
                }

            } while (str != "");
            string[] y = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            //получение номера максимального бита
            kolmax = long.Parse(y[10] + "");
            bool fl = false;
            //проверка выбранных пользавателем позиций
            if (numericUpDown1.Value > 0) if ((numericUpDown2.Value > 0) && (numericUpDown1.Value <= numericUpDown2.Value) && (numericUpDown2.Value <= kolmax)) fl = true;
            if ((numericUpDown1.Value == 0) && (numericUpDown2.Value == 0)) fl = true;
            if (fl == true)
            {
                Form2l.firstpos = (int)numericUpDown1.Value;
                Form2l.lastpos = (int)numericUpDown2.Value;
                //проверка выбранной операции
                if (radioButton11.Checked == true)
                {
                    Form q = new perestanovka();
                    q.Show();
                }
                if (radioButton10.Checked == true)
                {
                    Form q = new reverse();
                    q.Show();
                }
                if (radioButton1.Checked == true)
                {
                    Form q = new otrl();
                    q.Show();
                }
                if (radioButton2.Checked == true)
                {
                    Form q = new and0l();
                    q.Show();
                }
                if (radioButton3.Checked == true)
                {
                    Form q = new and1l();
                    q.Show();
                }
                if (radioButton4.Checked == true)
                {
                    Form q = new or0l();
                    q.Show();
                }
                if (radioButton5.Checked == true)
                {
                    Form q = new or1l();
                    q.Show();
                }
                if (radioButton6.Checked == true)
                {
                    Form q = new xor0l();
                    q.Show();
                }
                if (radioButton7.Checked == true)
                {
                    Form q = new xor1l();
                    q.Show();
                }
                if (radioButton8.Checked == true)
                {
                    Form q = new sdvigvpravol();
                    q.Show();
                }
                if (radioButton9.Checked == true)
                {
                    Form q = new sdvigvlevol();
                    q.Show();
                }
                this.Hide();
            }
            else
            {
                MessageBox.Show("Первая выбранная позиция должна быть меньше либо равна второй выбранной позиции\nВторая выбранная позиция должна быть меньше либо равна максимальной позиции во входных данных\nВыбранные Вами позиции не могут равняться 0", "Недопустимые позиции", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Логическое отрицание (!)\nОписание:\nВ результате выполнения данной операции 0 заменяется на 1, а единица – на 0.\nВыполнение операции ‘!’ над 101 приводит к получению 010.", "Логическое отрицание (!)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Конъюнкция c 0 (&)\nОписание:\nВ результате выполнения данной операции над 0 или 1 исходного слова, дает 0.\nВыполнение операции ‘&’ (с нулем) над 101, приводит к получению 000.", "Конъюнкция c 0 (&)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Конъюнкция c 1 (&)\nОписание:\nВ результате выполнения данной операции 0 заменяется на 0, получаем 0, а единица на 1.\nВыполнение операции ‘&’ (c единицей) над 101 приводит к получению 101.", "Конъюнкция c 1 (&)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Дизъюнкция с 0 (|)\nОписание:\nВ результате выполнение данной операции 1 заменяется на 1, а ноль - на 0.\nВыполнение операции ‘|’ (с нулем) над 101 приводит к получению 101.", "Дизъюнкция c 0 (|)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Дизъюнкция с 1 (|)\nОписание:\nВ результате выполнение данной операции 1 заменяется на 1, а ноль - на 1.\nВыполнение операции ‘|’ (с единицей) над 101 приводит к получению 111.", "Дизъюнкция c 1 (|)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Строгая дизъюнкция с 0 (^)\nОписание:\nВ результате выполнение данной операции 1 заменяется на 1, а ноль - на 0.\nВыполнение операции ‘^’ (с нулем) над 101 приводит к получению 101.", "Строгая дизъюнкция c 0 (^)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Строгая дизъюнкция с 1 (^)\nОписание:\nВ результате выполнение данной операции 1 заменяется на 0, а ноль - на 1.\nВыполнение операции ‘^’ (с единицей) над 101 приводит к получению 010.", "Строгая дизъюнкция c 1 (^)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Сдвиг вправо (>>)\nОписание:\nВ результате выполнения данной операции получается последовательность из 0 и 1, смещенная вправо на заданную длину (указанное число бит).\nВНИМАНИЕ: имеются определенные нюансы выполнения этой операции, пояснение которых дается в процессе работы с программой.\nВышедшие за правую границу разрядной сетки биты отбрасываются.", "Сдвиг вправо (>>)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Сдвиг влево (<<)\nОписание:\nВ результате выполнения данной операции получается последовательность из 0 и 1, смещенная влево на заданную длину (указанное число бит). \nОсвобождающиеся в результате сдвига биты заполняются справа нулями, а вышедшие за левую границу разрядной сетки биты отбрасываются.\nВыполнение данной операции ‘<<’ над 01110101 со смещением равным 3 в восьмиразрядной сетке, приводит к получению 10101000.", "Сдвиг влево (<<)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Реверсирование (reverse)\nОписание:\nВ результате выполнения данной операции получается последовательность из 0 и 1, в которой указанная подпоследовательность бит «перевернута» (первый бит становится последним, второй бит - предпоследним и т.д.).\nВыполнение операции ‘reverse’ над 01101 с первой по третью позицию, приводит к получению 11001.", "Реверсирование (reverse)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //описание данной операции
            MessageBox.Show("Перестановка группы бит (< ... >)\nОписание:\nВ результате выполнения данной операции, получается последовательность из 0 и 1, в которой выбранная подпоследовательность бит перемещена в указанное место (позицию), а на ее место переставлена подпоследовательность, «вытесненная» первой подпоследовательностью.\nВыполнение операции ‘<..>’ над 011101, в которой подпоследовательность со второй до третьей позиции (2 бита) перемещается в пятую позицию, приводит к получению 001111.", "Перестановка группы бит (< ... >)", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Form3l_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void Form3l_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}