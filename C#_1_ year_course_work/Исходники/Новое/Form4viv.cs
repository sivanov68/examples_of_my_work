﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class Form4viv : Form
    {
        public static long nomer1posle = 0, nomer2posle = 0, nomer3posle = 0;
        public static float nomer1float = 0, nomer2float = 0, nomer3float = 0;
        public static string viv1 = "", viv2 = "", viv3 = "";
        public Form4viv()
        {
            InitializeComponent();
            label3.Text = Form2l.oper;
            label8.Text = Form2l.firstpos.ToString();
            label10.Text = Form2l.lastpos.ToString();
            //если есть первый элемент, заполняем для него данные
            if (Form4.nomer1est == true)
            {
                label23.Text = Form4.nomer1naz.ToString();
                string r = "";
                if (Form4.nomer1.Length > 8)
                {
                    for (int i = 0; i < 8; i++)
                        r += Form4.nomer1[i];
                    r += "...";
                    label13.Text = r;
                }
                else
                    label13.Text = Form4.nomer1.ToString();
                label20.Text = Form4.massuvadresovob[0];
            }
            //если есть второй элемент(массив), заполняем для него данные
            if (Form4.nomer2est == true)
            {
                label15.Text = Form4.nomer2naz.ToString();
                string r = "";
                if (Form4.nomer2all.Length > 8)
                {
                    for (int i = 0; i < 8; i++)
                        r += Form4.nomer2all[i];
                    r += "...";
                    label12.Text = r;
                }
                else
                    label12.Text = Form4.nomer2all.ToString();
                label21.Text = Form4.massuvadresov[0];
            }
            //если есть третий элемент, заполняем для него данные
            if (Form4.nomer3est == true)
            {
                label14.Text = Form4.nomer3naz.ToString();
                string r = "";
                if (Form4.nomer3.Length > 8)
                {
                    for (int i = 0; i < 8; i++)
                        r += Form4.nomer3[i];
                    r += "...";
                    label16.Text = r;
                }
                else
                    label16.Text = Form4.nomer3.ToString();
            }
            label3.Text = Form2l.oper.ToString();
            string numer1 = "";
            string numer3 = "";
            int zamena2 = 0;
            if (Form4.nomer1est == true)
            {
                zamena2 = Form4.nomer1dss.Length;
            }
            if (Form4.nomer1est == true)
            {
                for (int i = 0; i < zamena2; i++)
                    numer1 += Form4.strokaposle[i];
            }
            string[] numer2 = new string[1];
            int zamena1 = 0;
            if (Form4.nomer2est == true)
            {
                zamena1 = Form4.nomer2.Length;
            }
            if (Form4.nomer2est == true)
            {
                numer2 = new string[zamena1];
            }
            int ch = 0;
            int l = 0;
            //если есть второй элемент, определяем замену
            if (Form4.nomer2est == true)
            {
                for (int i = zamena2; i < zamena1 * Form4.nomer2raz + zamena2; i++)
                {
                    l++;
                    numer2[ch] += Form4.strokaposle[i];
                    if (((i + 1) % Form4.nomer2raz == 0) && (i != zamena2) && (l >= Form4.nomer2raz)) ++ch;
                }
            }
            //если есть третий элемент
            if (Form4.nomer3est == true)
            {
                for (int i = zamena1 * Form4.nomer2raz + zamena2; i < Form4.strokaposle.Length; i++)
                    numer3 += Form4.strokaposle[i];
            }
            string strall = "";
            string f = "";
            string h = "";
            int nomer = 0;
            int nomerob = 0;
            //если есть первый элемент, заполняем листбокс
            if (Form4.nomer1est == true)
            {
                if (Form4.longorfloat1 == "float")
                {
                    nomer1float = K.toFloat(numer1);
                    viv1 += nomer1float;
                }
                else
                {
                    nomer1posle = K.from2to10(numer1);
                    viv1 += nomer1posle;
                }
                listBox1.Items.Add("Значение первого элемента " + viv1 + " :");
                string r = "";
                if (viv1.Length > 8)
                {
                    for (int i = 0; i < 8; i++)
                        r += viv1[i];
                    r += "...";
                    label27.Text = r;
                }
                else
                    label27.Text = viv1.ToString();
                for (int i = 0; i < numer1.Length; i++)
                {

                    if ((i % 8 == 0) && (i != 0))
                    {
                        h = (i / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + " Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        strall += f + " ";
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;
                    }
                    if (i == zamena2 - 1)
                    {
                        f += numer1[i];
                        h = ((i + 1) / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + " Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        strall += f + " ";
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;
                    }
                    else
                    {
                        f += numer1[i];

                    }
                }
            }
            int nomerbaita = 0; ch = 0; int ui = 0;
            //если есть второй элемент(массив), заполняем для него данные
            if (Form4.nomer2est == true)
            {
                if (Form4.longorfloat2 == "float")
                {
                    for (int i = 0; i < numer2.Length; i++)
                    {
                        if (i == numer2.Length - 1)
                        {
                            nomer2float = K.toFloat(numer2[i]);
                            viv2 += nomer2float;
                        }
                        else
                        {
                            nomer2float = K.toFloat(numer2[i]);
                            viv2 += nomer2float + ";";
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < numer2.Length; i++)
                    {
                        if (i == numer2.Length - 1)
                        {
                            nomer2posle = K.from2to10(numer2[i]);
                            viv2 += nomer2posle;
                        }
                        else
                        {
                            nomer2posle = K.from2to10(numer2[i]);
                            viv2 += nomer2posle + ";";
                        }
                    }
                }
                listBox1.Items.Add("Значение второго элемента " + viv2 + " :");
                if (viv2.Length > 8)
                {
                    string r = "";
                    for (int i = 0; i < 8; i++)
                        r += viv2[i];
                    r += "...";
                    label28.Text = r;
                }
                else
                    label28.Text = viv2.ToString();
                label22.Text = Form4.massuvadresovob[nomerob];
                for (int j = 0; j < numer2.Length; j++)
                {
                    h = "";
                    f = "";
                    nomerbaita = ch;
                    for (int i = 0; i < numer2[j].Length; i++)
                    {
                        if ((i % 8 == 0) && (i != 0))
                        {

                            h = ((i / 8) + nomerbaita) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresov[ui] + " Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8);

                            listBox1.Items.Add(h);
                            strall += f + " ";
                            h = "";
                            f = "";
                            ch++;
                            ui++;
                            nomer++;

                        }
                        if (i == numer2[j].Length - 1)
                        {
                            f += numer2[j][i];
                            h = (((i + 1) / 8) + nomerbaita) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresov[ui] + " Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                            listBox1.Items.Add(h);
                            strall += f + " ";
                            h = "";
                            f = "";
                            ch++;
                            ui++;
                            nomer++;

                        }
                        else
                            f += numer2[j][i];

                    }
                }
            }
            //если есть третий элемент, заполняем листбокс
            if (Form4.nomer3est == true)
            {
                if (Form4.longorfloat3 == "float")
                {
                    nomer3float = K.toFloat(numer3);
                    viv3 += nomer3float;
                }
                else
                {
                    nomer3posle = K.from2to10(numer3);
                    viv3 += nomer3posle;
                }
                listBox1.Items.Add("Значение третьего элемента " + viv3 + " :");
                if (viv3.Length > 8)
                {
                    string r = "";
                    for (int i = 0; i < 8; i++)
                        r += viv3[i];
                    r += "...";
                    label29.Text = r;
                }
                else
                    label29.Text = viv3.ToString();
                label22.Text = Form4.massuvadresovob[nomerob];
                for (int i = 0; i < numer3.Length; i++)
                {

                    if ((i % 8 == 0) && (i != 0))
                    {
                        h = (i / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + " Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        strall += f + " ";
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;

                    }
                    if (i == numer3.Length - 1)
                    {
                        f += numer3[i];
                        h = ((i + 1) / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + " Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        strall += f + " ";
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;
                    }
                    else
                        f += numer3[i];
                }

            }
            textBox1.Text = strall;
            int nachpos;
            int konpos;
            nachpos = Form2l.firstpos;
            konpos = Form2l.lastpos;
            int kolvoelintextbox1 = 0;
            string stroka = "";
            int inc = -1;
            do
            {
                ++inc;
                try
                {
                    stroka = listBox1.Items[inc].ToString();
                }
                catch
                {
                    break;
                }
                if (stroka != "") kolvoelintextbox1++;
            } while (stroka != null);
            //выборка адресов байт, которые изменились и запись их в листбокс2
            for (int i = 0; i < kolvoelintextbox1; i++)
            {
                try
                {
                    stroka = listBox1.Items[i].ToString();
                    string[] a = stroka.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                    long np = long.Parse(a[8]);
                    long kp = long.Parse(a[10]);
                    if ((np <= konpos) && (kp >= nachpos))
                    {
                        listBox2.Items.Add(a[5]);
                    }
                }
                catch
                {
                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Helpstart.fn4.Show();
            nomer1posle = 0;
            nomer2posle = 0;
            nomer3posle = 0;
            nomer1float = 0;
            nomer2float = 0;
            nomer3float = 0;
            viv1 = "";
            viv2 = "";
            viv3 = "";
            Form4.nomer1 = null;
            Form4.nomer1naz = null;
            Form4.nomer1raz = 0;
            Form4.nomer2raz = 0;
            Form4.nomer2naz = null;
            Form4.nomer3naz = null;
            Form4.nomer3 = null;
            Form4.nomer3raz = 0;
            Form4.adres1 = null;
            Form4.adres2 = null;
            Form4.adres3 = null;
            Form4.nomer1dss = null;
            Form4.nomer3dss = null;
            Form4.strokado = null;
            Form4.strokaposle = null;
            Form4.nomer1est = false;
            Form4.nomer2est = false;
            Form4.nomer3est = false;
            Form4.longorfloat1 = null;
            Form4.longorfloat2 = null;
            Form4.longorfloat3 = null;
        }

        private void Form4viv_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void Form4viv_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
         
