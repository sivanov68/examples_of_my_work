﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Новое
{
    public partial class Form4 : Form
    {
        //получаем массив из двоичных кодов от дробей, которые записаны в массив
        public static string[] Intomass(string k,int df)
        {
            string[] a;
            float y;
            int kolvonomer2 = 0;
            for (int i = 0; i < k.Length; i++)
                if (k[i] == ';') kolvonomer2++;
            kolvonomer2++;
            a = new string[kolvonomer2];
            string t = "";
            int nomer2massiv = 0;
            for (int i = 0; i < k.Length; i++)
            {
                if ((i == k.Length - 1) || (k[i] == ';'))
                {
                    if (i == k.Length - 1) t += k[i];
                    try
                    {
                        int w = int.Parse(t);
                        a[nomer2massiv] = inttobin(w, df);
                    }
                    catch
                    {
                        y = float.Parse(t);
                        a[nomer2massiv] = K.FloatToBin(y);
                    }
                    
                    nomer2massiv++;
                    t = "";
                }
                else
                    t += k[i];
            }
            return a;
        }
        //получение двоичного кода от числа заданной длины
        public static string inttobin(long a, int b)
        {
            int nomerpos = 0;
            bool fl = false;
            string b1 = "", b2 = "";
            string b4 = "";
            string b5 = "";
            string b6 = "";
            long ost, cel;
            cel = Math.Abs(a);
            do
            {
                ost = cel % 2;
                cel = cel / 2;
                b1 += ost;
            } while (cel != 0);
            for (int i = b1.Length - 1; i >= 0; i--)
            {
                b2 += b1[i];
            }
            //длина 11
            if (b == 11)
            {
                if (b2.Length != 11)
                {
                    for (int j = 0; j < 11 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            //длина 8
            if (b == 8)
            {
                if (b2.Length != 8)
                {
                    for (int j = 0; j < 8 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            //длина 16
            if (b == 16)
            {
                if (b2.Length != 16)
                {
                    for (int j = 0; j < 16 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            //длина 32
            if (b == 32)
            {
                if (b2.Length != 32)
                {
                    for (int j = 0; j < 32 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            //длина 64
            if (b == 64)
            {
                if (b2.Length != 64)
                {
                    for (int j = 0; j < 64 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            if (a < 0)
            {
                for (int i = 0; i < b4.Length; i++)
                {
                    if (b4[i] == '0') b5 += '1'; else b5 += '0';
                }
                do
                {
                    b4 = "";
                    for (int i = b5.Length - 1; i >= 0; i--)
                        if (b5[i] == '0')
                        {
                            b4 += '1';
                            nomerpos = i;
                            fl = true;
                            break;
                        }
                        else
                        {
                            b4 += '0';
                        }
                } while (fl == false);
                char[] massiv = new char[b4.Length];
                for (int i = 0; i < b4.Length; i++)
                    massiv[i] = b4[i];
                Array.Reverse(massiv);
                b4 = "";
                for (int i = 0; i < massiv.Length; i++)
                    b4 += massiv[i];

                for (int i = 0; i < nomerpos; i++)
                    b6 += b5[i];
                b5 = "";
                b5 = b6 + b4;
                b4 = b5;
            }
            return b4;
        }
        public Form4()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 5;
            comboBox4.SelectedIndex = 5;
            comboBox6.SelectedIndex = 5;
        }
        public static string[] massuvadresovob = new string[] { "0X06beea98", "0X06beea99", "0X06beea9a", "0X06beea9b", "0X06beea9c", "0X06beea9d", "0X06beea9e", "0X06beea9f", "0X06beeaa0", "0X06beeaa1", "0X06beeaa2", "0X06beeaa3", "0X06beeaa4", "0X06beeaa5", "0X06beeaa6", "0X06beeaa7" };
        public static string[] massuvadresov = new string[] { "0X025d96bc", "0X025d96bd", "0X025d96be", "0X025d96bf", "0X025d96c0", "0X025d96c1", "0X025d96c2", "0X025d96c3", "0X025d96c4", "0X025d96c5", "0X025d96c6", "0X025d96c7", "0X025d96c8", "0X025d96c9", "0X025d96ca", "0X025d96cb", "0X025d96cc", "0X025d96cd", "0X025d96ce", "0X025d96cf", "0X025d96d0", "0X025d96d1", "0X025d96d2", "0X025d96d3", "0X025d96d4", "0X025d96d5", "0X025d96d6", "0X025d96d7", "0X025d96d8", "0X025d96d9", "0X025d96da", "0X025d96db", "0X025d96dc", "0X025d96dd", "0X025d96de", "0X025d96df", "0X025d96e0", "0X025d96e1", "0X025d96e2", "0X025d96e3", "0X025d96e4", "0X025d96e5", "0X025d96e6", "0X025d96e7", "0X025d96e8", "0X025d96e9", "0X025d96ea", "0X025d96eb", "0X025d96ec", "0X025d96ed", "0X025d96ee", "0X025d96ef", "0X025d96f0", "0X025d96f1", "0X025d96f2", "0X025d96f3", "0X025d96f4", "0X025d96f5", "0X025d96f6", "0X025d96f7", "0X025d96f8", "0X025d96f9", "0X025d96fa", "0X025d96fb", "0X025d96fc", "0X025d96fd", "0X025d96fe", "0X025d96ff", "0X025d9700", "0X025d9701", "0X025d9702", "0X025d9703", "0X025d9704", "0X025d9705", "0X025d9706", "0X025d9707", "0X025d9708", "0X025d9709", "0X025d970a", "0X025d970b", "0X025d970c", "0X025d970d", "0X025d970e", "0X025d970f", "0X025d9710" };
        public static string nomer1 = null;
        public static string nomer1naz = null;
        public static string nomer3all = null;
        public static int nomer1raz = 0;
        public static int nomer2raz = 0;
        public static string nomer2naz = null;
        public static string nomer3naz = null;
        public static string nomer3 = null;
        public static int nomer3raz=0;
        public static string[] nomer2;
        public static string nomer2all = null;
        public static string adres1 = null;
        public static string adres2 = null;
        public static string adres3 = null;
        public static string nomer1dss = null;
        public static string nomer3dss = null;
        public static string strokado = null;
        public static string strokaposle=null;
        public static bool nomer1est=false;
        public static bool nomer2est=false;
        public static bool nomer3est=false;
        public bool fl1 = true, fl2 = true, fl3 = true;
        public static string longorfloat1 = "", longorfloat2 = "", longorfloat3 = "";
        private void button1_Click_1(object sender, EventArgs e)
        {
            //все элементы пустые
            if (((textBox1.Text == "") && (textBox2.Text == "") && (textBox3.Text == "")) || ((comboBox1.SelectedIndex == 5) && (comboBox4.SelectedIndex == 5) && (comboBox6.SelectedIndex == 5)))
            {
                MessageBox.Show("Для продолжения работы, нужно выбрать хотя бы один элемент", "Пустые данные", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    nomer1raz = 8;
                    nomer1naz = "System.Sbyte";
                    nomer1est = true;
                }
                if (comboBox1.SelectedIndex == 4)
                {
                    nomer1raz = 32;
                    nomer1naz = "System.Single";
                    nomer1est = true;
                    longorfloat1 = "float";
                }

                if (comboBox1.SelectedIndex == 1)
                {
                    nomer1raz = 16;
                    nomer1naz = "System.Int16";
                    nomer1est = true;
                }

                if (comboBox1.SelectedIndex == 2)
                {
                    nomer1raz = 32;
                    nomer1naz = "System.Int32";
                    nomer1est = true;
                    longorfloat1 = "int";
                }
                if (comboBox1.SelectedIndex == 3)
                {
                    nomer1raz = 64;
                    nomer1naz = "System.Int64";
                    nomer1est = true;
                    
                }
                if (comboBox4.SelectedIndex == 4)
                {
                    nomer2raz = 32;
                    nomer2naz = "System.Single[ ]";
                    nomer2est = true;
                    longorfloat2 = "float";
                }
                if (comboBox4.SelectedIndex == 0)
                {
                    nomer2raz = 8;
                    nomer2naz = "System.Sbyte[ ]";
                    nomer2est = true;
                }
                if (comboBox4.SelectedIndex == 1)
                {
                    nomer2raz = 16;
                    nomer2naz = "System.Int16[ ]";
                    nomer2est = true;
                }
                if (comboBox4.SelectedIndex == 2)
                {
                    nomer2raz = 32;
                    nomer2naz = "System.Int32[ ]";
                    nomer2est = true;
                    longorfloat2 = "int";
                }
                if (comboBox4.SelectedIndex == 3)
                {
                    nomer2raz = 64;
                    nomer2naz = "System.Int64[ ]";
                    nomer2est = true;
                    
                }
                if (comboBox6.SelectedIndex == 4)
                {
                    nomer3raz = 32;
                    nomer3naz = "System.Single";
                    nomer3est = true;
                    longorfloat3 = "float";
                }
                if (comboBox6.SelectedIndex == 0)
                {
                    nomer3raz = 8;
                    nomer3naz = "System.Sbyte";
                    nomer3est = true;
                }
                if (comboBox6.SelectedIndex == 1)
                {
                    nomer3raz = 16;
                    nomer3naz = "System.Int16";
                    nomer3est = true;
                }
                if (comboBox6.SelectedIndex == 2)
                {
                    nomer3raz = 32;
                    nomer3naz = "System.Int32";
                    nomer3est = true;
                    longorfloat3 = "int";
                }
                if (comboBox6.SelectedIndex == 3)
                {
                    nomer3raz = 64;
                    nomer3naz = "System.Int64";
                    nomer3est = true;
                    
                }
                //если первый(левый) элемент задан
                if (nomer1est == true)
                {
                    fl1 = true;
                    if (textBox1.Text == "")
                        fl1 = false;                 
                        if ((longorfloat1 == "float") && (fl1==true))
                        {
                            int kolvotz = 0;
                            int kolvozap = 0;
                            string h = textBox1.Text;
                            for (int i = 0; i < h.Length; i++)
                            {
                                if (h[i] == ';') kolvotz++;
                                if (h[i] == ',') kolvozap++;
                            }
                            if (kolvotz != kolvozap - 1)
                            {                            
                                fl1 = false;
                            }
                        }
                        if (fl1==true)
                        {
                            sbyte q1;
                            short q2;
                            int q3;
                            long q4;
                            float w;
                            try
                            {
                                //получение значения элемента
                                if (nomer1raz == 8) q1 = checked(sbyte.Parse(textBox1.Text));
                                if (nomer1raz == 16) q2 = checked(short.Parse(textBox1.Text));
                                if (nomer1raz == 64) q4 = checked(long.Parse(textBox1.Text));
                                if ((nomer1raz == 32) && (longorfloat1 == "int")) q3 = checked(int.Parse(textBox1.Text));
                                if ((nomer1raz == 32) && (longorfloat1 == "float")) w = checked(float.Parse(textBox1.Text));
                                fl1 = true;
                            }
                            catch
                            {
                                fl1 = false;
                            }
                            if (fl1 == true)
                            {
                                nomer1 = textBox1.Text;
                                try
                                {
                                    nomer1dss = inttobin(long.Parse(nomer1), nomer1raz);
                                }
                                catch
                                {
                                    nomer1dss = K.FloatToBin(float.Parse(nomer1));
                                }
                                strokado += nomer1dss;
                            }
                        }
                    }
                if (fl1 == false)
                    MessageBox.Show("Возможные ошибки:\n1) синтаксическая ошибка ввода\n2) при выборе типа float, если вы хотите ввести целочисленное значение, допишите ,0.Например, если вы хотите записать число 4 в тип float, то запишите 4,0\n3) введенное значение не соответсвует диапозону значений выбранного типа", "Ошибка ввода данных первого элемента", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //если второй(центральный) элемент задан
                if (nomer2est == true)
                {
                    try
                    {
                        fl2 = true;
                        if (textBox2.Text == "")
                            fl2 = false;
                        if (fl2 == true)
                        {
                            nomer2all = textBox2.Text;
                            int kovozap = 0;
                            for (int i = 0; i < nomer2all.Length; i++)
                                if (nomer2all[i] == ';') kovozap++;
                            if (kovozap > 10)
                            {
                                fl2 = false;
                            }
                        }
                        if (fl2 == true)
                        {
                            string n1 = "", n3 = "";
                            int p = -1;
                            sbyte q1;
                            short q2;
                            int q3;
                            long q4;
                            float w;
                            n1 = textBox2.Text;
                            do
                            {
                                p++;
                                if ((n1[p] != ';')) n3 += n1[p];
                                if ((n1[p] == ';') || (p == n1.Length - 1))
                                {
                                    try
                                    {
                                        //получение элементов массива
                                        if (nomer2raz == 8) q1 = checked(sbyte.Parse(n3));
                                        if (nomer2raz == 16) q2 = checked(short.Parse(n3));
                                        if (nomer2raz == 64) q4 = checked(long.Parse(n3));
                                        if ((nomer2raz == 32) && (longorfloat2 == "int")) q3 = checked(int.Parse(n3));
                                        if ((nomer2raz == 32) && (longorfloat2 == "float")) w = checked(float.Parse(n3));
                                        fl2 = true;
                                        n3 = "";
                                    }
                                    catch
                                    {
                                        fl2 = false;
                                        break;
                                    }
                                }
                            } while (p != n1.Length - 1);
                            if (fl2 == true)
                            {
                                nomer2all = textBox2.Text;
                                nomer2 = Intomass(textBox2.Text, nomer2raz);
                                foreach (string ty in nomer2)
                                    strokado += ty;
                            }
                        }
                    }
                    catch
                    {
                        fl2 = false;
                    }
                    if ((fl2 == false) && (fl1 == true))
                        MessageBox.Show("Возможные ошибки:\n1) синтаксическая ошибка ввода\n2) при выборе типа float, если вы хотите ввести целочисленное значение, допишите ,0.Например, если вы хотите записать число 4 в тип float, то запишите 4,0\n3) введенное значение не соответствует диапазону значений выбранного типа\n4) количество элементов массива больше 10", "Ошибка ввода данных второго элемента", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //если третий(правый) элемент задан
                }
                if (nomer3est == true)
                {
                    fl3 = true;
                    if (textBox3.Text == "")
                        fl3 = false;
                        if ((longorfloat3 == "float") && (fl3==true))
                        {
                            int kolvotz = 0;
                            int kolvozap = 0;
                            string h = textBox3.Text;
                            for (int i = 0; i < h.Length; i++)
                            {
                                if (h[i] == ';') kolvotz++;
                                if (h[i] == ',') kolvozap++;
                            }
                            if (kolvotz != kolvozap - 1)
                            {                               
                                fl3 = false;
                            }
                        }
                        if (fl3==true)
                        {
                            sbyte q1;
                            short q2;
                            int q3;
                            long q4;
                            float w;
                            try
                            {
                                //получение значения элемента
                                if (nomer3raz == 8) q1 = checked(sbyte.Parse(textBox3.Text));
                                if (nomer3raz == 16) q2 = checked(short.Parse(textBox3.Text));
                                if (nomer3raz == 64) q4 = checked(long.Parse(textBox3.Text));
                                if ((nomer3raz == 32) && (longorfloat3 == "int")) q3 = checked(int.Parse(textBox3.Text));
                                if ((nomer3raz == 32) && (longorfloat3 == "float")) w = checked(float.Parse(textBox3.Text));
                                fl3 = true;
                            }
                            catch
                            {
                                fl3 = false;                                
                            }
                            if (fl3 == true)
                            {
                                nomer3 = textBox3.Text;
                                try
                                {
                                    nomer3dss = inttobin(long.Parse(nomer3), nomer3raz);
                                }
                                catch
                                {
                                    nomer3dss = K.FloatToBin(float.Parse(nomer3));
                                }
                                strokado += nomer3dss;
                            }
                        }
                    }
                //получение фиксированных адресов в памяти 
                if (textBox1.Text.Length > 0) adres1 = "0X06b8e7a8";
                    if (textBox2.Text.Length > 0) adres2 = "0X025d96bc";
                    if ((fl1 == true) && (fl3 == true) && (fl2 == true))
                    {
                        this.Hide();
                        Form q = new Form4res();
                        q.Show();
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        comboBox1.SelectedIndex = 5;
                        comboBox4.SelectedIndex = 5;
                        comboBox6.SelectedIndex = 5;
                    }
                }
            if ((fl3 == false)&&(fl1==true)&&(fl2==true))
                MessageBox.Show("Возможные ошибки:\n1) синтаксическая ошибка ввода\n2) при выборе типа float, если вы хотите ввести целочисленное значение, допишите ,0.Например, если вы хотите записать число 4 в тип float, то запишите 4,0\n3) введенное значение не соответствует диапазону значений выбранного типа", "Ошибка ввода данных третьего элемента", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //допустимые значения
            MessageBox.Show("sbyte    MIN: -128                                  MAX: 127\nshort    MIN: -32768                               MAX: 32767\nint        MIN: -2147483648                     MAX: 2147483647\nlong     MIN: -9223372036854775808   MAX: 9223372036854775807\nfloat     MIN: -3.402823E+38                 MAX: 3.402823E+38  ", "Допустимые значения типов данных", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void Form4_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}