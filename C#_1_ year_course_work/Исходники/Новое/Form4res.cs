﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class Form4res : Form
    {
        public Form4res()
        {
            InitializeComponent();
            //если есть первый элемент, заполняем для него данные
            if (Form4.nomer1est == true)
            {
                label5.Text = Form4.nomer1naz.ToString();
                string r="";
                if (Form4.nomer1.Length > 8)
                {
                    for (int i = 0; i < 8; i++)
                        r += Form4.nomer1[i];
                    r += "...";
                    label9.Text = r;
                }
                else
                label9.Text = Form4.nomer1.ToString();
                label20.Text = Form4.adres1.ToString();
            }
            //если есть второй элемент(массив), заполняем для него данные
            if (Form4.nomer2est == true)
            {
                label6.Text = Form4.nomer2naz.ToString();
                string r = "";
                if (Form4.nomer2all.Length > 8)
                {
                    for (int i = 0; i < 8; i++)
                        r += Form4.nomer2all[i];
                    r+="...";
                    label10.Text = r;
                }
                else
                label10.Text = Form4.nomer2all.ToString();
                label21.Text = Form4.adres2.ToString();
            }
            //если есть третий элемент, заполняем для него данные
            if (Form4.nomer3est == true)
            {
                label7.Text = Form4.nomer3naz.ToString();
                string r = "";
                if (Form4.nomer3.Length > 8)
                {
                    for (int i = 0; i < 8; i++)
                        r += Form4.nomer3[i];
                    r += "...";
                    label16.Text = r;
                }
                else
                label16.Text = Form4.nomer3.ToString();
            }
            string f = "";
            string h = "";
            int nomer = 0;
            int nomerob = 0;
            //если есть первый элемент, заполняем листбокс
            if (Form4.nomer1est == true)
            {
                listBox1.Items.Add("Значение первого элемента " + Form4.nomer1 + " :");
                for (int i = 0; i < Form4.nomer1dss.Length; i++)
                {

                    if ((i % 8 == 0) && (i != 0))
                    {
                        h = (i / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + "   Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;
                    }
                    if (i == Form4.nomer1dss.Length - 1)
                    {
                        f += Form4.nomer1dss[i];
                        h = ((i + 1) / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + "   Номера битов: " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;
                    }
                    else
                        f += Form4.nomer1dss[i];
                }
            }
            int nomerbaita = 0; int ch = 0; int ui = 0;
            //если есть второй элемент(массив), заполняем листбокс
            if (Form4.nomer2est == true)
            {
                listBox1.Items.Add("Значение второго элемента " + Form4.nomer2all + " :");
                for (int j = 0; j < Form4.nomer2.Length; j++)
                {
                    h = "";
                    f = "";
                    nomerbaita = ch;
                    for (int i = 0; i < Form4.nomer2[j].Length; i++)
                    {
                        if ((i % 8 == 0) && (i != 0))
                        {

                            h = ((i / 8) + nomerbaita) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresov[ui] + "   Номера битов " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8);

                            listBox1.Items.Add(h);
                            h = "";
                            f = "";
                            ch++;
                            ui++;
                            nomer++;
                        }
                        if (i == Form4.nomer2[j].Length - 1)
                        {
                            f += Form4.nomer2[j][i];
                            h = (((i + 1) / 8) + nomerbaita) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresov[ui] + "   Номера битов " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                            listBox1.Items.Add(h);
                            h = "";
                            f = "";
                            ch++;
                            ui++;
                            nomer++;
                        }
                        else
                            f += Form4.nomer2[j][i];

                    }
                }
            }
            //если есть третий элемент, заполняем листбокс
            if (Form4.nomer3est == true)
            {
                listBox1.Items.Add("Значение третьего элемента " + Form4.nomer3 + " :");
                label22.Text = Form4.massuvadresovob[nomerob];
                for (int i = 0; i < Form4.nomer3dss.Length; i++)
                {

                    if ((i % 8 == 0) && (i != 0))
                    {
                        h = (i / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + "   Номера битов " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;
                    }
                    if (i == Form4.nomer3dss.Length - 1)
                    {
                        f += Form4.nomer3dss[i];
                        h = ((i + 1) / 8) + " Байт  " + f + "  " + " Адрес памяти: " + Form4.massuvadresovob[nomerob] + "   Номера битов " + (nomer * 8 + 1) + " - " + (nomer * 8 + 8); ;
                        listBox1.Items.Add(h);
                        h = "";
                        f = "";
                        nomer++;
                        nomerob++;
                    }
                    else
                        f += Form4.nomer3dss[i];
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            long kolmax = 0;
            int i = 0;
            string str = "";
            do
            {
                try
                {
                    str = listBox1.Items[i].ToString();
                    if (str != "") i++;
                }
                catch
                {
                    break;
                }

            } while (str != "");
            string[] y = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            //получение номера максимального бита
            kolmax = long.Parse(y[10] + "");
            bool fl = false;
            //проверка выбранных пользавателем позиций
            if (numericUpDown1.Value > 0) if ((numericUpDown2.Value > 0) && (numericUpDown1.Value <= numericUpDown2.Value) && (numericUpDown2.Value <= kolmax)) fl = true;
            if ((numericUpDown1.Value == 0) && (numericUpDown2.Value == 0)) fl = true;
            if (fl == true)
            {
                Form2l.firstpos = (int)numericUpDown1.Value;
                Form2l.lastpos = (int)numericUpDown2.Value;
                //проверка выбранной операции
                if (radioButton1.Checked == true)
                {
                    Form q = new otrl();
                    q.Show();
                }
                if (radioButton2.Checked == true)
                {
                    Form q = new and0l();
                    q.Show();
                }
                if (radioButton3.Checked == true)
                {
                    Form q = new and1l();
                    q.Show();
                }
                if (radioButton4.Checked == true)
                {
                    Form q = new or0l();
                    q.Show();
                }
                if (radioButton5.Checked == true)
                {
                    Form q = new or1l();
                    q.Show();
                }
                if (radioButton6.Checked == true)
                {
                    Form q = new xor0l();
                    q.Show();
                }
                if (radioButton7.Checked == true)
                {
                    Form q = new xor1l();
                    q.Show();
                }
                if (radioButton8.Checked == true)
                {
                    if ((Form4.nomer1naz == "System.Single") || (Form4.nomer2naz == "System.Single") || (Form4.nomer3naz == "System.Single"))
                    {
                        var result = MessageBox.Show("Помните, что данная операция (сдвиг вправо двоичного представления числа) НЕ применима к значениям вещественного типа! Тем не менее, в программе реализован сдвиг соответствующей вещественному числу двоичной последовательности (можно на секунду забыть, что это – двоичное представление числа типа float…), несмотря на то, что смысла такой сдвиг НЕ имеет.\nВыберите ответ ‘Да’, если Вы все-таки хотите выполнить такой сдвиг, или ответ ‘Нет’ – для отказа от этой затеи (рекомендуется).", "Побитовые операции : Предупреждение",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            Form q = new sdvigvpravol();
                            q.Show();
                        }
                        if (result == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        Form q = new sdvigvpravol();
                        q.Show();
                    }
                }
                if (radioButton9.Checked == true)
                {
                    if ((Form4.nomer1naz == "System.Single") || (Form4.nomer2naz == "System.Single") || (Form4.nomer3naz == "System.Single"))
                    {
                        var result = MessageBox.Show("Помните, что данная операция (сдвиг влево двоичного представления числа) НЕ применима к значениям вещественного типа! Тем не менее, в программе реализован сдвиг соответствующей вещественному числу двоичной последовательности (можно на секунду забыть, что это – двоичное представление числа типа float…), несмотря на то, что смысла такой сдвиг НЕ имеет.\nВыберите ответ ‘Да’, если Вы все-таки хотите выполнить такой сдвиг, или ответ ‘Нет’ – для отказа от этой затеи (рекомендуется).", "Побитовые операции : Предупреждение",
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            Form q = new sdvigvlevol();
                            q.Show();
                        }
                        if (result == DialogResult.No)
                        {
                            return;
                        }
                    }
                    else
                    {
                        Form q = new sdvigvlevol();
                        q.Show();
                    }
                }
                if (radioButton10.Checked == true)
                {
                    Form q = new reverse();
                    q.Show();
                }
                if (radioButton11.Checked == true)
                {
                    Form q = new perestanovka();
                    q.Show();
                }
                this.Hide();
            }
            else
            {
                MessageBox.Show("Первая выбранная позиция должна быть меньше либо равна второй выбранной позиции\nВторая выбранная позиция должна быть меньше либо равна максимальной позиции во входных данных\nВыбранные Вами позиции не могут равняться 0", "Недопустимые позиции", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Form4res_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void Form4res_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}