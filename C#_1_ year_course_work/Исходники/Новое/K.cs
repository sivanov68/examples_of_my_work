﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Новое
{
    public class K
    {
        //метод получения десятичного числа из двоичного кода, в том числе отрицательного
        public static long from2to10(string b4)
        {
            long p = 0;
            bool g = false;
            if (b4[0] == '1')
            {
                g = true;
                string b5 = "";
                string b6 = "";
                bool fl = false;
                int nomerpos = 0;
                for (int i = 0; i < b4.Length; i++)
                {
                    if (b4[i] == '0') b5 += '1'; else b5 += '0';
                }
                do
                {
                    b4 = "";
                    for (int i = b5.Length - 1; i >= 0; i--)
                        if (b5[i] == '0')
                        {
                            b4 += '1';
                            nomerpos = i;
                            fl = true;
                            break;
                        }
                        else
                        {
                            b4 += '0';
                        }
                } while (fl == false);
                char[] massiv = new char[b4.Length];
                for (int i = 0; i < b4.Length; i++)
                    massiv[i] = b4[i];
                Array.Reverse(massiv);
                b4 = "";
                for (int i = 0; i < massiv.Length; i++)
                    b4 += massiv[i];

                for (int i = 0; i < nomerpos; i++)
                    b6 += b5[i];
                b5 = "";
                b5 = b6 + b4;
                b4 = b5;
            }
            for (int i = 0; i < b4.Length; i++)
                p += long.Parse(b4[i] + "") * (long)Math.Pow(2, b4.Length - 1 - i);
            if (g == true) p = (-1) * p;
            return p;

        }
        //метод реализующий перестановку бит
        public static string perestanovka(string str, ref int  nach, ref int konec, int kyda)
        {
            //проверка на ошибку в позициях
            if (((nach > 0) && (nach <= str.Length)) && ((konec > 0) && (konec <= str.Length)) && ((kyda > 0) && (kyda <= str.Length)))
            {
                if (((kyda + (konec - nach)) > str.Length) || ((kyda >= nach) && (kyda <= konec)))
                {
                    str = "ошибка";
                    throw new Exception("ошибка");
                }
                else
                {
                    if (kyda <= nach)
                    {
                        if (kyda + (konec - nach) >= nach)
                        {
                            str = "ошибка";
                            throw new Exception("ошибка");
                        }
                        else
                        {
                            if ((nach == 0) && (konec == 0))
                            {
                                nach = 0;
                                konec = str.Length - 1;
                            }
                            else
                            {
                                nach--;
                                konec--;
                            }
                            kyda--;
                            string a = "";
                            if (kyda < nach)
                            {
                                for (int i = 0; i < kyda; i++)
                                    a += str[i];
                                for (int i = nach; i <= konec; i++)
                                    a += str[i];
                                for (int i = kyda + konec - nach + 1; i < nach; i++)
                                    a += str[i];
                                for (int i = kyda; i < kyda + (konec - nach + 1); i++)
                                    a += str[i];
                                for (int i = konec + 1; i < str.Length; i++)
                                    a += str[i];
                                str = "";
                                str = a;
                                nach++;
                                konec++;

                            }
                            else
                            {
                                for (int i = 0; i < nach; i++)
                                    a += str[i];
                                for (int i = kyda; i < kyda + konec - nach + 1; i++)
                                    a += str[i];
                                for (int i = konec + 1; i < kyda; i++)
                                    a += str[i];
                                for (int i = nach; i <= konec; i++)
                                    a += str[i];
                                for (int i = kyda + konec - nach + 1; i < str.Length; i++)
                                    a += str[i];
                                str = "";
                                str = a;
                                nach++;
                                konec++;
                            }
                            return str;
                        }
                    }
                    else
                    {
                        if ((nach == 0) && (konec == 0))
                        {
                            nach = 0;
                            konec = str.Length - 1;
                        }
                        else
                        {
                            nach--;
                            konec--;
                        }
                        kyda--;
                        string a = "";
                        if (kyda < nach)
                        {
                            for (int i = 0; i < kyda; i++)
                                a += str[i];
                            for (int i = nach; i <= konec; i++)
                                a += str[i];
                            for (int i = kyda + konec - nach + 1; i < nach; i++)
                                a += str[i];
                            for (int i = kyda; i < kyda + (konec - nach + 1); i++)
                                a += str[i];
                            for (int i = konec + 1; i < str.Length; i++)
                                a += str[i];
                            str = "";
                            str = a;
                            nach++;
                            konec++;
                        }
                        else
                        {
                            for (int i = 0; i < nach; i++)
                                a += str[i];
                            for (int i = kyda; i < kyda + konec - nach + 1; i++)
                                a += str[i];
                            for (int i = konec + 1; i < kyda; i++)
                                a += str[i];
                            for (int i = nach; i <= konec; i++)
                                a += str[i];
                            for (int i = kyda + konec - nach + 1; i < str.Length; i++)
                                a += str[i];
                            str = "";
                            str = a;
                            nach++;
                            konec++;
                        }
                        return str;
                    }
                }                
            }
            else
            {
                str = "ошибка";
                throw new Exception("ошибка");
            }
            
        }
        //метод возвращает номера позиций элементов через запятую, которые разные в строке a и b
        public static string izmen(string a, string b)
        {
            string h = "";
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i]) h += (i + 1)+",";
            }
            string y="";
            for (int i = 0; i < h.Length; i++)
            {
                if (i == h.Length - 1)
                {
                }
                else
                y += h[i];
            }
            return y;
        }
        //метод получения конечного результата после преобразований, получаем преобразованную строку неких символов
        public static string end(int[] nomera, string strokaposle)
        {
            string stroka4 = "";
            for (int i = 1; i <= nomera.Length; i++)
            {
                char d;
                d = K.nomerintochar(strokaposle, i, nomera);
                stroka4 = stroka4 + d.ToString();
            }
            return stroka4;
        }
        //метод получения символа от двоичного кода
        public static char kodotstroki(string h)
        {
            long a = 0;
            char t;
            for (int i = 0; i < h.Length; i++)
                a += ((int)h[i] - 48) * (int)Math.Pow(2, h.Length - 1 - i);
            t = (char)a;
            return t;
        }
        //метод получения i-ого символа из строки(строка-двоичный код некой длины)
        public static char nomerintochar(string stroka, int a, int[] b)
        {
            string g = "";
            int kol = 0;
            char ch;
            for (int i = 0; i < a - 1; i++)
                kol += b[i];
            for (int i = kol; i < kol + b[a - 1]; i++)
                g += stroka[i];
            ch = kodotstroki(g);
            return ch;
        }
        //метод получения двоичного кода от символа
        public static string kod(char symb)
        {
            int a;
            a = (int)symb;
            string s = "";
            int MASK = 32768;
            int maxSize = 16;
            for (int i = 0; i < maxSize; i++)
            {
                if ((a & MASK) == MASK)
                {
                    s = s + "1";
                }
                else
                {
                    s = s + "0";
                }
                a = a << 1;
            }
            return s;
        }
        //метод реализующий сдвиг влево
        public static string sdvigvlevo(string k, ref int a, ref int b, int v)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            char[] newchar = new char[k.Length];
            for (int i = 0; i < newchar.Length; i++)
                newchar[i] = k[i];
            for (int j = 1; j <= v; j++)
            {
                for (int i = a - 1; i < b - 1; i++)
                {
                    newchar[i] = newchar[i + 1];
                }
                newchar[b - 1] = '0';
            }
            string h = "";
            for (int i = 0; i < newchar.Length; i++)
                h += newchar[i];
            return h;
        }
        //метод реализующий сдвиг вправо с "вдвигом" единиц
        public static string sdvigvpravos1(string k, ref int a, ref int b, int v)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            char[] newchar = new char[k.Length];
            for (int i = 0; i < newchar.Length; i++)
                newchar[i] = k[i];
            for (int j = 1; j <= v; j++)
            {
                for (int i = b - 1; i >= a; i--)
                    newchar[i] = newchar[i - 1];
                newchar[a - 1] = '1';
            }
            string h = "";
            for (int i = 0; i < newchar.Length; i++)
                h += newchar[i];
            return h;
        }
        //метод реализующий сдвиг вправо с "вдвигом" нулей
        public static string sdvigvpravo(string k, ref int a, ref int b, int v)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            char[] newchar = new char[k.Length];
            for (int i = 0; i < newchar.Length; i++)
                newchar[i] = k[i];
            for (int j = 1; j <= v; j++)
            {
                for (int i = b - 1; i >= a; i--)
                    newchar[i] = newchar[i - 1];
                newchar[a - 1] = '0';
            }
            string h = "";
            for (int i = 0; i < newchar.Length; i++)
                h += newchar[i];
            return h;
        }
        //метод реализующий реверсирование
        public static string reverse(string str, ref int a, ref int b)
        {
            string voz = "";
            if ((a == 0) && (b == 0))
            {
                a = 0;
                b = str.Length - 1;
            }
            else
            {
                a--;
                b--;
            }
            for (int i = 0; i < a; i++)
                voz += str[i];
            for (int i = b; i >= a; i--)
                voz += str[i];
            for (int i = b + 1; i < str.Length; i++)
                voz += str[i];
            str = "";
            str = voz;
            a++;
            b++;
            return str;
        }
        //метод реализующий and с 0
        public static string and0l(string k, ref int a, ref int b)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            string t = "";
            for (int i = 0; i < a - 1; i++)
                t = t + k[i];
            for (int i = a - 1; i < b; i++)
                if (k[i] == '0') t = t + '0'; else t = t + '0';
            for (int i = b; i < k.Length; i++)
                t = t + k[i];
            return t;
        }
        //метод реализующий and с 1
        public static string and1l(string k, ref int a, ref int b)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            string t = "";
            for (int i = 0; i < a - 1; i++)
                t = t + k[i];
            for (int i = a - 1; i < b; i++)
                if (k[i] == '0') t = t + '0'; else t = t + '1';
            for (int i = b; i < k.Length; i++)
                t = t + k[i];
            return t;
        }
        //метод реализующий xor с 1
        public static string xor1l(string k, ref int a, ref int b)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            string t = "";
            for (int i = 0; i < a - 1; i++)
                t = t + k[i];
            for (int i = a - 1; i < b; i++)
                if (k[i] == '0') t = t + '1'; else t = t + '0';
            for (int i = b; i < k.Length; i++)
                t = t + k[i];
            return t;
        }
        //метод реализующий xor с 0
        public static string xor0l(string k, ref int a, ref int b)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            string t = "";
            for (int i = 0; i < a - 1; i++)
                t = t + k[i];
            for (int i = a - 1; i < b; i++)
                if (k[i] == '0') t = t + '0'; else t = t + '1';
            for (int i = b; i < k.Length; i++)
                t = t + k[i];
            return t;
        }
        //метод реализующий отрицание 
        public static string otrl(string k, ref int a, ref int b)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            string t = "";
            for (int i = 0; i < a - 1; i++)
                t = t + k[i];
            for (int i = a - 1; i < b; i++)
                if (k[i] == '0') t = t + '1'; else t = t + '0';
            for (int i = b; i < k.Length; i++)
                t = t + k[i];
            return t;
        }
        //метод реализующий or с 0
        public static string or0l(string k, ref int a, ref int b)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            string t = "";
            for (int i = 0; i < a - 1; i++)
                t = t + k[i];
            for (int i = a - 1; i < b; i++)
                if (k[i] == '0') t = t + '0'; else t = t + '1';
            for (int i = b; i < k.Length; i++)
                t = t + k[i];
            return t;
        }
        //метод реализующий or с 1
        public static string or1l(string k, ref int a, ref int b)
        {
            if (a == 0 && b == 0)
            {
                a = 1;
                b = k.Length;
            }
            string t = "";
            for (int i = 0; i < a - 1; i++)
                t = t + k[i];
            for (int i = a - 1; i < b; i++)
                if (k[i] == '0') t = t + '1'; else t = t + '1';
            for (int i = b; i < k.Length; i++)
                t = t + k[i];
            return t;
        }
        //перевод строки из 0 и 1(двоичный код) в десятичное число(без отрицательного) числа
        public static int To10(string adres)
        {
            int sum = 0;
            for (int i = 0; i < adres.Length; i++)
            {
                int a;
                a = adres[i] - 48;
                sum += a * (int)(Math.Pow(2, adres.Length - i - 1));
            }
            return sum;
        }
        //метод получение степени 10 (когда работает с типом float)
        public static int sm(string c)
        {
            string h = c.ToString();
            int kol = 0;
            if (h[0] != '0')
                for (int i = 1; i < h.Length; i++)
                {
                    if (h[i] == ',') break;
                    if (h[i] != ',') kol++;
                }
            else
            {
                for (int i = 2; i < h.Length; i++)
                {
                    if (h[i] == '0') kol--;
                    if (h[i] != '0') break;
                }
                kol--;
            }
            return kol;
        }
        //метод получения кода после запятой (когда работает с типом float)
        public static string kodotdrobi(double k)
        {
            string t = k.ToString();
            bool fl = false;
            for (int i = 0; i < t.Length; i++)
            {
                if (t[i] == ',')
                {
                    fl = true;
                    break;
                }
            }
            if (fl == false)
            {
                t += ",0";
            }
            string novper = "";
            string nov = "";
            for (int i = t.Length - 1; i >= 0; i--)
            {
                if (t[i] != ',')
                    novper += t[i];
                if (t[i] == ',') break;
            }
            nov += "0,";
            for (int i = novper.Length - 1; i >= 0; i--)
                nov += novper[i];
            k = double.Parse(nov);
            string viv = "";
            for (int i = 1; i < 33; i++)
                if ((k - Math.Pow(2, (-1) * i)) >= 0)
                {
                    k -= Math.Pow(2, (-1) * i);
                    viv += "1";
                }

                else viv += "0";
            return viv;
        }
        //метод получение от дроби только целой части (когда работает с типом float)
        public static long celchast(double k)
        {
            long r = 0;
            string t = k.ToString();
            string nov = "";
            for (int i = 0; i < t.Length; i++)
            {
                if (t[i] != ',')
                    nov += t[i];
                if (t[i] == ',') break;
            }
            r = long.Parse(nov);
            return r;
        }
        //метод получения двоичного кода целой части без первых нулей (когда работает с типом float)
        public static string obrezkacel(string h)
        {
            string nov = "";
            int i = 0;
            for (i = 0; i < h.Length; i++)
                if (h[i] != '0') break;
            for (int j = i; j < h.Length; j++)
                nov += h[j];
            return nov;
        }
        //метод получения двоичного кода дробной части без последних нулей (когда работает с типом float)
        public static string obrezkadrob(string h)
        {
            string nov = "";
            string nov2 = "";
            string nov3 = "";
            int i = 0;
            for (i = h.Length - 1; i >= 0; i--)
                if (h[i] != '0') break;
            for (int j = i; j >= 0; j--)
                nov += h[j];
            for (int j = 0; j < nov.Length; j++)
                nov2 += nov[j];
            for (int j = nov2.Length - 1; j >= 0; j--)
            {
                nov3 += nov2[j];
            }
            return nov3;
        }
        //метод получение мантисы (когда работает с типом float)
        public static string mantisa(string a, string b)
        {
            int nomer = 0;
            string viv = "";
            if (a != "0")
            {
                for (int i = 1; i < a.Length; i++)
                {
                    viv += a[i];
                    nomer++;
                }
                for (int i = 0; i < b.Length; i++)
                {
                    viv += b[i];
                    nomer++;
                    if (nomer == 23) break;
                }
                if (nomer < 23)
                    for (int i = viv.Length + 1; i <= 23; i++)
                        viv += "0";
                return viv;
            }
            else
            {
                int y = 0;
                for (y = 0; y < b.Length; y++)
                    if (b[y] == '1') break;
                for (int i = y + 1; i < b.Length; i++)
                {
                    viv += b[i];
                    nomer++;
                    if (nomer == 23) break;
                }
                if (nomer < 23)
                    for (int i = viv.Length + 1; i <= 23; i++)
                        viv += "0";
                return viv;
            }
        }
        //метод перевода числа в двоичное представление с заданной длиной строки (когда работает с типом float)
        public static string inttobin(long a, int b)
        {
            int nomerpos = 0;
            bool fl = false;
            string b1 = "", b2 = "";
            string b4 = "";
            string b5 = "";
            string b6 = "";
            long ost, cel;
            cel = Math.Abs(a);
            do
            {
                ost = cel % 2;
                cel = cel / 2;
                b1 += ost;
            } while (cel != 0);
            for (int i = b1.Length - 1; i >= 0; i--)
            {
                b2 += b1[i];
            }
            if (b == 16)
            {
                if (b2.Length != 16)
                {
                    for (int j = 0; j < 16 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            if (b == 8)
            {
                if (b2.Length != 8)
                {
                    for (int j = 0; j < 8 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            if (b == 32)
            {
                if (b2.Length != 32)
                {
                    for (int j = 0; j < 32 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            if (b == 64)
            {
                if (b2.Length != 64)
                {
                    for (int j = 0; j < 64 - b2.Length; j++)
                        b4 += '0';
                }
                b4 += b2;
            }
            if (a < 0)
            {
                for (int i = 0; i < b4.Length; i++)
                {
                    if (b4[i] == '0') b5 += '1'; else b5 += '0';
                }
                do
                {
                    b4 = "";
                    for (int i = b5.Length - 1; i >= 0; i--)
                        if (b5[i] == '0')
                        {
                            b4 += '1';
                            nomerpos = i;
                            fl = true;
                            break;
                        }
                        else
                        {
                            b4 += '0';
                        }
                } while (fl == false);
                char[] massiv = new char[b4.Length];
                for (int i = 0; i < b4.Length; i++)
                    massiv[i] = b4[i];
                Array.Reverse(massiv);
                b4 = "";
                for (int i = 0; i < massiv.Length; i++)
                    b4 += massiv[i];

                for (int i = 0; i < nomerpos; i++)
                    b6 += b5[i];
                b5 = "";
                b5 = b6 + b4;
                b4 = b5;
            }
            return b4;
        }
        //метод возвращающий двоичный код от дробного числа (когда работает с типом float)
        public static string FloatToBin(float z)
        {
            double n = Math.Abs(z);
            string drob;
            drob = kodotdrobi(n);
            drob = obrezkadrob(drob);
            long cel = 0;
            if (z < 1) cel = 0;
            else cel = celchast(n);
            string celin2 = "";
            celin2 = inttobin(cel, 32);
            celin2 = obrezkacel(celin2);
            if (celin2 == "") celin2 += "0";
            string celsdrob = "";
            celsdrob = celin2 + "," + drob;
            Console.WriteLine(celsdrob);
            int a = 0;
            a = sm(celsdrob);
            Console.WriteLine(a);
            a += 127;
            string stepen = "";
            stepen = inttobin(a, 8);
            Console.WriteLine(stepen);
            string all = "";
            if (z >= 0) all += "0";
            else all += "1";
            string j = "";
            Console.WriteLine(celin2 + "    " + drob);
            j = mantisa(celin2, drob);
            Console.WriteLine("mant" + j);
            all += stepen;
            all += j;
            return all;
        }
        //метод получения дробной части из двоичного кода (когда работает с типом float)
        public static float To10drobshast(string adres)
        {
            long v = To10(adres);
            float l = (float)Math.Pow(2, 23);
            float r = v / l;
            return r;
        }
        //метод получение дроби из двоичного кода (когда работает с типом float)
        public static float toFloat(string h)
        {
            float a = 1;
            string znak = h[0] + "";
            string stepen10 = "";
            string drob = "";
            long b;
            float drobindec;
            for (int i = 1; i < 9; i++)
            {
                stepen10 += h[i];
            }
            for (int i = 9; i < h.Length; i++)
            {
                drob += h[i];
            }
            b = To10(stepen10);
            b -= 127;
            drobindec = To10drobshast(drob);
            a *= (float)Math.Pow(2, b);
            a *= (1 + drobindec);
            if (znak == "1") a *= (-1);
            return a;
        }
    }
}
           