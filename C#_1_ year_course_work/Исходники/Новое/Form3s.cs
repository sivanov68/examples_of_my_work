﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Новое
{
    public partial class Form3s : Form
    {
        public static string file = "";
        public static bool fl = false;
        public Form3s()
        {
            InitializeComponent();
            listBox1.Select();
            radioButton1.Checked = true;
            textBox1.Text = Form2l.stroka1;
            int a = 1;
            //заполнение листбокса
            for (int i = 0; i < Form2l.stroka1.Length; i++)
            {
                if (a == 1) a = 0;
                a += 16;
                string b = "";
                string h = "";
                h = K.kod(Form2l.stroka1[i]);
                b = (i + 1) + ")" + h + "  \t" + "Элемент строки " + "  '" + Form2l.stroka1[i] + "'  " + "\t" +
                "Номер начальной позиции=" + (a - 15) + " \t Конечный конечной позиции= " + a;
                listBox1.Items.Add(b);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            long kolmax = 0;
            int i =0;
            string str = "";
            do
            {
                try
                {
                    str = listBox1.Items[i].ToString();
                    if (str != "") i++;
                }
                catch
                {
                    break;
                }
            } while (str != "");
            string[] y = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            //получение номера максимального бита
            kolmax = long.Parse(y[10]+"");
            bool fl = false;
            //проверка выбранных пользавателем позиций
            if (numericUpDown1.Value > 0) if ((numericUpDown2.Value > 0)&&(numericUpDown1.Value <= numericUpDown2.Value)&&(numericUpDown2.Value <= kolmax)) fl = true;
            if ((numericUpDown1.Value == 0)&&(numericUpDown2.Value == 0))fl=true;
            if (fl==true)
            {
                Form2l.firstpos = (int)numericUpDown1.Value;
                Form2l.lastpos = (int)numericUpDown2.Value;
                //проверка выбранной операции
                if (radioButton11.Checked == true)
                {
                    Form q = new perestanovka();
                    q.Show();
                }
                if (radioButton10.Checked == true)
                {
                    Form q = new reverse();
                    q.Show();
                }
                if (radioButton1.Checked == true)
                {
                    Form q = new otrl();
                    q.Show();
                }
                if (radioButton2.Checked == true)
                {
                    Form q = new and0l();
                    q.Show();
                }
                if (radioButton3.Checked == true)
                {
                    Form q = new and1l();
                    q.Show();
                }
                if (radioButton4.Checked == true)
                {
                    Form q = new or0l();
                    q.Show();
                }
                if (radioButton5.Checked == true)
                {
                    Form q = new or1l();
                    q.Show();
                }
                if (radioButton6.Checked == true)
                {
                    Form q = new xor0l();
                    q.Show();
                }
                if (radioButton7.Checked == true)
                {
                    Form q = new xor1l();
                    q.Show();
                }
                if (radioButton8.Checked == true)
                {
                    Form q = new sdvigvpravol();
                    q.Show();
                }
                if (radioButton9.Checked == true)
                {
                    Form q = new sdvigvlevol();
                    q.Show();
                }
                this.Hide();
            }
            else
            {
                MessageBox.Show("Первая выбранная позиция должна быть меньше либо равна второй выбранной позиции\nВторая выбранная позиция должна быть меньше либо равна максимальной позиции во входных данных\nВыбранные Вами позиции не могут равняться 0", "Недопустимые позиции", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //считывание операций из файла
                bool fl = true;
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    file = openFileDialog.FileName;
                    StreamReader streamreader = new StreamReader(openFileDialog.FileName);
                    Form2l.filename = openFileDialog.FileName;
                    string str = "";
                    int first = 0, last = 0;
                    string oper = "";
                    streamreader.BaseStream.Position = 0;
                    do
                    {
                        str = streamreader.ReadLine();
                        if (str == null) break;
                        string[] k = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                        int.TryParse(k[0] + "", out first);
                        int.TryParse(k[1] + "", out last);
                        oper = k[2].ToString();
                        //выбор нужной операции
                        switch (oper)
                        {

                            case "xor1":
                                {
                                    Form2l.stroka2 = K.xor1l(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "and1":
                                {
                                    Form2l.stroka2 = K.and1l(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "xor0":
                                {
                                    Form2l.stroka2 = K.xor0l(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "and0":
                                {
                                    Form2l.stroka2 = K.and0l(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "!":
                                {
                                    Form2l.stroka2 = K.otrl(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "or0":
                                {
                                    Form2l.stroka2 = K.or0l(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "or1":
                                {
                                    Form2l.stroka2 = K.or1l(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "reverse":
                                {
                                    Form2l.stroka2 = K.reverse(Form2l.stroka2, ref first, ref last);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case ">>":
                                {
                                    string y;
                                    y = k[3].ToString();
                                    int r = int.Parse(y);
                                    Form2l.stroka2 = K.sdvigvpravo(Form2l.stroka2, ref first, ref last, r);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "<<":
                                {
                                    string y;
                                    y = k[3].ToString();
                                    int r = int.Parse(y);
                                    Form2l.stroka2 = K.sdvigvlevo(Form2l.stroka2, ref first, ref last, r);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            case "perestanovka":
                                {
                                    string y;
                                    y = k[3].ToString();
                                    int r = int.Parse(y);
                                    Form2l.stroka2 = K.perestanovka(Form2l.stroka2, ref first, ref last, r);
                                    Form2l.strokaposlevipoln = Form2l.stroka2;
                                    Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                    break;
                                }
                            default:
                                {
                                    fl = false;
                                    MessageBox.Show("Ошибка в данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    break;
                                }
                        }
                    } while (str != "");
                    streamreader.Close();
                }
                if (fl == true)
                {
                    Form2l.changeelement = K.izmen(Form2l.stroka1, Form2l.stroka4);
                    Form2l.fileornot = true;
                    Form q = new vivod2();
                    q.Show();
                    this.Hide();
                }
                else
                {
                    fl = false;
                }
            }
            catch
            {
                MessageBox.Show("Произошла ошибка при работе с файлом.\nПроверьте корректность входных данных.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Form3s_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //инструкция
            MessageBox.Show("Входной файл может содержать строки следующих форматов:\nВариант #1:\nномер_начальной_позиции <пробел> номер_конечной_позиции <пробел> операция\nВариант #2:\nномер_начальной_позиции <пробел> номер_конечной_позиции <пробел> операция <пробел> некоторое_число\n\nВарианты записи операций:\n\nВариант #1:\n! - Логическое отрицание (!)\nand0 - Конъюнкция c 0 (&)\nand1 - Конъюнкция c 1 (&)\nor0 - Дизъюнкция с 0 (|)\nor1 - Дизъюнкция с 1 (|)\nxor0 - Строгая дизъюнкция с 0 (^)\nxor1 - Строгая дизъюнкция с 1 (^)\nreverse - Реверсирование\n\nВариант #2:\nperestanovka - Перестановка группы бит (некоторое_число – номер позиции, в которую будет переставлена группа бит).\n<< - Сдвиг влево (некоторое_число – число бит, на которое сдвигается исходное значение).\n>> - Сдвиг вправо (некоторое_число – число бит, на которое сдвигается исходное значение).", "Оформление входного файла", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Form3s_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
       