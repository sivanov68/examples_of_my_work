﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Новое
{
    public partial class Form3Text : Form
    {
        public Form3Text()
        {
            InitializeComponent();
            button4.Enabled = false;
        }
        public static string strokavxoda;
        StreamReader StreamRead;
        StreamWriter StreamWrite;
        public static string file = "";
        
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //открытие входного файла
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.FileName = "";
                openFileDialog.Filter = ".txt|*.txt";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamRead = new StreamReader(openFileDialog.FileName, System.Text.Encoding.GetEncoding(1251));
                    StreamRead.BaseStream.Position = 0;
                }
                string h = openFileDialog.FileName;
                string a = "";
                try
                {
                    for (int i = 0; i < 40; i++)
                        a += h[i];
                }
                catch
                {
                }
                a += "...";
                label2.Text = a;
                strokavxoda = StreamRead.ReadLine();
            }
            catch
            {
                MessageBox.Show("Входной файл не выбран.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                //получение выходного файла
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.FileName = "";
                openFileDialog.Filter = ".txt|*.txt";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWrite = new StreamWriter(openFileDialog.FileName);
                    StreamWrite.BaseStream.Position = 0;
                }
                string h = openFileDialog.FileName;
                file = openFileDialog.FileName;
                string a = "";
                try
                {
                    for (int i = 0; i < 40; i++)
                        a += h[i];
                }
                catch
                {
                }
                a += "...";
                label4.Text = a;
            }
            catch
            {
                MessageBox.Show("Выходной файл не выбран.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            //считывание слова и операций из входного файла
            bool fl = true;
            try
            {
                StreamWrite.WriteLine(strokavxoda + "   Входное слово");
                StreamWrite.Flush();
                Form2l.stroka1 = strokavxoda;
                Form2l.nomera = new int[Form2l.stroka1.Length];
                for (int i = 0; i < Form2l.stroka1.Length; i++)
                {
                    Form2l.stroka3 = K.kod(Form2l.stroka1[i]);
                    Form2l.nomera[i] = 16;
                    Form2l.stroka2 = Form2l.stroka2 + Form2l.stroka3;
                }
                string str;
                do
                {
                    str = StreamRead.ReadLine();
                    if (str == null) break;
                    string[] k = str.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                    int first = 0, last = 0;
                    int.TryParse(k[0] + "", out first);
                    int.TryParse(k[1] + "", out last);
                    string oper = k[2].ToString();
                    //выбор операции
                    switch (oper)
                    {

                        case "xor1":
                            {
                                Form2l.stroka2 = K.xor1l(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "and1":
                            {
                                Form2l.stroka2 = K.and1l(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "xor0":
                            {
                                Form2l.stroka2 = K.xor0l(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "and0":
                            {
                                Form2l.stroka2 = K.and0l(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "!":
                            {
                                Form2l.stroka2 = K.otrl(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "or0":
                            {
                                Form2l.stroka2 = K.or0l(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "or1":
                            {
                                Form2l.stroka2 = K.or1l(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "reverse":
                            {
                                Form2l.stroka2 = K.reverse(Form2l.stroka2, ref first, ref last);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case ">>":
                            {
                                string y;
                                y = k[3].ToString();
                                int r = int.Parse(y);
                                Form2l.stroka2 = K.sdvigvpravo(Form2l.stroka2, ref first, ref last, r);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "<<":
                            {
                                string y;
                                y = k[3].ToString();
                                int r = int.Parse(y);
                                Form2l.stroka2 = K.sdvigvlevo(Form2l.stroka2, ref first, ref last, r);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        case "perestanovka":
                            {
                                string y;
                                y = k[3].ToString();
                                int r = int.Parse(y);
                                Form2l.stroka2 = K.perestanovka(Form2l.stroka2, ref first, ref last, r);
                                Form2l.strokaposlevipoln = Form2l.stroka2;
                                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                                break;
                            }
                        default:
                            {
                                fl = false;
                                break;
                            }
                    }
                } while (str != "");
                if (fl == false)
                {                    
                    StreamWrite.Close();
                    StreamRead.Close();
                    button4.Enabled = false;
                }
                else
                {
                    StreamWrite.WriteLine(Form2l.stroka4 + "   Выходное слово");
                    StreamWrite.Close();
                    StreamRead.Close();
                }
            }
            catch
            {               
                fl = false;
            }
            if (fl == false)
            {
                MessageBox.Show("Ошибка чтения/записи/выполнения операции.\nПроверьте входные данные.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
                try
                {
                    StreamRead.Close();
                    StreamWrite.Close();
                }
                catch
                {
                }
                label2.Text = null;
                label4.Text = null;
            }
            else
            {
                MessageBox.Show("Запись прошла успешно.\nВы можете посмотреть выходной файл.", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button4.Enabled = true;
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                //Открыть входной файл
                System.Diagnostics.Process.Start(file);
            }
            catch
            {
                MessageBox.Show("Невозможно открыть файл.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            button4.Enabled = false;
            strokavxoda = null;
            StreamRead = null;
            StreamWrite = null;
            file = null;
            label2.Text = null;
            label4.Text = null;
        }

        private void Form3Text_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1.Perv.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //закрытие родительской формы
            this.Close();
            Form1.Perv.Close();
        }
        private void button7_Click(object sender, EventArgs e)
        {
            //открытие инструкция
            MessageBox.Show("В первой строке входного файла должно быть записано входное слово.\nДалее во входном файле должны следовать строки следующих форматов:\nВариант #1:\nномер_начальной_позиции <пробел> номер_конечной_позиции <пробел> операция\nВариант #2:\nномер_начальной_позиции <пробел> номер_конечной_позиции <пробел> операция <пробел> некоторое_число\n\nВарианты записи операций:\n\nВариант #1:\n! - Логическое отрицание (!)\nand0 - Конъюнкция c 0 (&)\nand1 - Конъюнкция c 1 (&)\nor0 - Дизъюнкция с 0 (|)\nor1 - Дизъюнкция с 1 (|)\nxor0 - Строгая дизъюнкция с 0 (^)\nxor1 - Строгая дизъюнкция с 1 (^)\nreverse - Реверсирование\n\nВариант #2:\nperestanovka - Перестановка группы бит (некоторое_число – номер позиции, в которую будет переставлена группа бит).\n<< - Сдвиг влево (некоторое_число – число бит, на которое сдвигается исходное значение).\n>> - Сдвиг вправо (некоторое_число – число бит, на которое сдвигается исходное значение).", "Оформление входного файла", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Form3Text_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}