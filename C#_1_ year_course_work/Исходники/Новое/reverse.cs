﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class reverse : Form
    {
        public reverse()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%E5%E2%E5%F0%F1");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Form1.nomer == 1)//режим работа с памятью
            {
                Form2l.oper = "-> <-";
                Form4.strokaposle = K.reverse(Form4.strokado, ref Form2l.firstpos, ref Form2l.lastpos);
                Form q = new Form4viv();
                q.Show();
                this.Hide();
            }
            if (Form1.nomer == 2)//экспертный режим
            {
                Form2l.oper = "-> <-";
                Form2l.strokaposlevipoln = K.reverse(Form2l.stroka2, ref Form2l.firstpos, ref Form2l.lastpos);
                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                Form q = new vivod1s();
                q.Show();
                this.Hide();
            }
            if (Form1.nomer == 0)//обычный режим работы
            {
                Form2l.oper = "-> <-";
                Form2l.strokaposlevipoln = K.reverse(Form2l.stroka2, ref Form2l.firstpos, ref Form2l.lastpos);
                Form2l.stroka4 = K.end(Form2l.nomera, Form2l.strokaposlevipoln);
                Form q = new vivod1();
                q.Show();
                this.Hide();
            }
        }

        private void reverse_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void reverse_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}