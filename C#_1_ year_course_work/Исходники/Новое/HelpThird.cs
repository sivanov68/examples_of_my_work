﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class HelpThird : Form
    {
        public static Form help4 = new HelpFour();
        public HelpThird()
        {
            InitializeComponent();
            button1.Focus();
        }

        private void HelpThird_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //предыдущая форма
            this.Hide();
            Helpfirst.help2.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //следующая форма
            this.Hide();
            help4.Show();
           
        }

        private void HelpThird_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
