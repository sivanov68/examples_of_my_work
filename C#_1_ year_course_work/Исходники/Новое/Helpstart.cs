﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class Helpstart : Form
    {
        public static Form help1 = new Helpfirst();
        public static Form fn4 = new Form4();
        public Helpstart()
        {
            
            InitializeComponent();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //отказ от инструкции
            this.Hide();
            fn4.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //ознакомиться с правилами работы
            help1.Show();
            this.Hide();
        }

        private void Helpstart_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрытие родительской формы
            Form1.Perv.Close();
        }

        private void Helpstart_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
