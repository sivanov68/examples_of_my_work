﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Новое
{
    public partial class Alloperators : Form
    {
        public static Form form2l;
        public Alloperators()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //проверка на галочку в поле "Я ознакомлен"
            if (checkBox1.Checked == true)
            {
                this.Hide();
                form2l = new Form2l();
                form2l.Show();

            }
            else MessageBox.Show("Чтобы продолжить-поставьте галочку в поле < Я ознакомлен >", "! Вы не ознакомились с операциями !", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BD%D1%8C%D1%8E%D0%BD%D0%BA%D1%86%D0%B8%D1%8F");

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BD%D1%8C%D1%8E%D0%BD%D0%BA%D1%86%D0%B8%D1%8F");
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%94%D0%B8%D0%B7%D1%8A%D1%8E%D0%BD%D0%BA%D1%86%D0%B8%D1%8F");
        }

        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%94%D0%B8%D0%B7%D1%8A%D1%8E%D0%BD%D0%BA%D1%86%D0%B8%D1%8F");
        }

        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/Xor");
        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/Xor");
        }

        private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%91%D0%B8%D1%82%D0%BE%D0%B2%D1%8B%D0%B5_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8#.D0.9B.D0.BE.D0.B3.D0.B8.D1.87.D0.B5.D1.81.D0.BA.D0.B8.D0.B9_.D1.81.D0.B4.D0.B2.D0.B8.D0.B3");
        }

        private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%91%D0%B8%D1%82%D0%BE%D0%B2%D1%8B%D0%B5_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8#.D0.9B.D0.BE.D0.B3.D0.B8.D1.87.D0.B5.D1.81.D0.BA.D0.B8.D0.B9_.D1.81.D0.B4.D0.B2.D0.B8.D0.B3");
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%9E%D1%82%D1%80%D0%B8%D1%86%D0%B0%D0%BD%D0%B8%D0%B5");
        }

        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ru.wikipedia.org/wiki/%D0%E5%E2%E5%F0%F1");
        }

        private void linkLabel11_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //открытие сайта с описанием данной операции
            System.Diagnostics.Process.Start("http://ya.ru");
        }

        private void Alloperators_FormClosed(object sender, FormClosedEventArgs e)
        {
            //закрыть родительскую форму
            Form1.Perv.Close();
        }

        private void Alloperators_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                try
                {
                    System.Diagnostics.Process.Start(@"helpbit.chm");
                }
                catch
                {
                    MessageBox.Show("Справка не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}