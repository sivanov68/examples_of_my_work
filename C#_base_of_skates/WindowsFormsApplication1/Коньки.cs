﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1
{
    public class Коньки
    {
        string фирма_изготовитель;//фирма изготовитель
        uint размер;//размер
        string материал_верха;//материал верха
        public string Фирма_изготовитель
        {
            get { return фирма_изготовитель; }
            set { фирма_изготовитель = value; }
        }
        public uint Размер
        {
            get { return размер; }
            set { размер = value; }
        }
        public string Материал_верха
        {
            get { return материал_верха; }
            set { материал_верха = value; }
        }
        /// <summary>
        /// Возвращает строку содержащую название коньков
        /// </summary>
        public virtual string Name_of_Skates
        {
            get { return "Коньки"; }
        }
        /// <summary>
        /// Конструктор класса Коньки
        /// </summary>
        /// <param name="фирма_изготовитель">фирма изготовитель</param>
        /// <param name="размер">размер</param>
        /// <param name="материал_верха">материал верха</param>
        protected Коньки(string фирма_изготовитель, uint размер, string материал_верха)
        {
            Фирма_изготовитель = фирма_изготовитель;
            Размер = размер;
            Материал_верха = материал_верха;
        }
        /// <summary>
        ///  Конструктор класса Коньки без параметров
        /// </summary>
        protected Коньки()
        {
            Фирма_изготовитель = null;
            Размер = 0;
            Материал_верха = null;
        }
        /// <summary>
        /// Метод считывания данных об объекте из строки
        /// </summary>
        /// <param name="s">входная строка</param>
        public virtual void ReadIn(string s)
        {
            string[] k=s.Split((char[]) null, StringSplitOptions.RemoveEmptyEntries);
            Фирма_изготовитель = k[3];
            Размер = uint.Parse(k[4]);
            Материал_верха = k[5];
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns></returns>
        public virtual string WriteIn()
        {
            return String.Format(Name_of_Skates + " {0,7} {1,2} {2,7} "
                , Фирма_изготовитель, Размер, Материал_верха);
        }

    }
    public class Хоккейные_коньки : Коньки
    {
        string материал_мыса;//материал мыса
        uint высота_сапожка;//высота сапожка
        public string Материал_мыса
        {
            get { return материал_мыса; }
            set { материал_мыса = value; }
        }
        public uint Высота_сапожка
        {
            get { return высота_сапожка; }
            set { высота_сапожка = value; }
        }
        /// <summary>
        /// Конструктор класса Хоккейные_коньки
        /// </summary>
        /// <param name="фирма_изготовитель">фирма изготовитель</param>
        /// <param name="размер">размер</param>
        /// <param name="материал_верха">материал верха</param>
        /// <param name="материал_мыса">материал мыса</param>
        /// <param name="высота_сапожка">высота сапожка</param>
        public Хоккейные_коньки(string фирма_изготовитель, uint размер, string материал_верха, string материал_мыса, uint высота_сапожка)
        {
            Фирма_изготовитель = фирма_изготовитель;
            Размер = размер;
            Материал_верха = материал_верха;
            Материал_мыса = материал_мыса;
            Высота_сапожка = высота_сапожка;
        }
        /// <summary>
        /// Конструктор класса Хоккейные_коньки без параметров
        /// </summary>
        public Хоккейные_коньки()
        {
            Фирма_изготовитель = null;
            Размер = 0;
            Материал_верха = null;
            Материал_мыса = null;
            Высота_сапожка = 0;
        }
        /// <summary>
        /// Возвращает строку содержащую название коньков
        /// </summary>
        public override string Name_of_Skates
        {
            get { return "Хоккейные коньки"; }
        }
        /// <summary>
        /// Метод считывания данных об объекте из строки
        /// </summary>
        /// <param name="s">входная строка</param>
          public override void ReadIn(string s)
        {
            string[] k = s.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Фирма_изготовитель = k[2];
            Размер = uint.Parse(k[3]);
            Материал_верха = k[4];
            Материал_мыса = k[5];
            Высота_сапожка = uint.Parse(k[6]);
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns></returns>
          public override string WriteIn()
          {
              return String.Format(Name_of_Skates + " {0,7} {1,2} {2,7} {3,7} {4,3}"
                  , Фирма_изготовитель, Размер, Материал_верха,Материал_мыса,Высота_сапожка);
          }
    }
    public class Фигурные_коньки : Коньки
    {
        string материал_подошвы;//материал подошвы
        string наличие_нижнего_зубца;//наличие нижнего зубца
        public string Материал_подошвы
        {
            get { return материал_подошвы; }
            set { материал_подошвы = value; }
        }
        public string Наличие_нижнего_зубца
        {
            get { return наличие_нижнего_зубца; }
            set { наличие_нижнего_зубца = value; }
        }
        /// <summary>
        /// Конструктор класса Фигурные_коньки
        /// </summary>
        /// <param name="фирма_изготовитель">фирма изготовитель</param>
        /// <param name="размер">размер</param>
        /// <param name="материал_верха">материал верха</param>
        /// <param name="материал_подошвы">материал подошвы</param>
        /// <param name="наличие_нижнего_зубца">наличие нижнего зубца</param>
        public Фигурные_коньки(string фирма_изготовитель, uint размер, string материал_верха, string материал_подошвы, string наличие_нижнего_зубца)
        {
            Фирма_изготовитель = фирма_изготовитель;
            Размер = размер;
            Материал_верха = материал_верха;
            Материал_подошвы = материал_подошвы;
            Наличие_нижнего_зубца = наличие_нижнего_зубца;
        }
        /// <summary>
        /// Конструктор класса Фигурные_коньки без параметров
        /// </summary>
        public Фигурные_коньки()
        {
            Фирма_изготовитель =null ;
            Размер = 0;
            Материал_верха = null;
            Материал_подошвы = null;
            Наличие_нижнего_зубца = null;
        }
        /// <summary>
        /// Возвращает строку содержащую название коньков
        /// </summary>
        public override string Name_of_Skates
        {
            get { return "Фигурные коньки"; }
        }
        /// <summary>
        /// Метод считывания данных об объекте из строки
        /// </summary>
        /// <param name="s">входная строка</param>
        public override void ReadIn(string s)
        {
            string[] k = s.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Фирма_изготовитель = k[2];
            Размер = uint.Parse(k[3]);
            Материал_верха = k[4];
            Материал_подошвы = k[5];
            Наличие_нижнего_зубца = k[6];
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns></returns>
        public override string WriteIn()
        {
            return String.Format(Name_of_Skates + " {0,7} {1,2} {2,7} {3,7} {4,3} "
                , Фирма_изготовитель, Размер, Материал_верха, Материал_подошвы, Наличие_нижнего_зубца);
        }
    }
    public class Беговые_коньки : Коньки
    {
        string наличие_откидывания_пятки;//наличие откидывания пятки
        public string Наличие_откидывания_пятки
        {
            get { return наличие_откидывания_пятки; }
            set { наличие_откидывания_пятки = value; }
        }
        /// <summary>
        /// Конструктор класса Беговые_коньки
        /// </summary>
        /// <param name="фирма_изготовитель">фирма изготовитель</param>
        /// <param name="размер">размер</param>
        /// <param name="материал_верха">материал верха</param>
        /// <param name="наличие_откидывания_пятки">наличие откидывания пятки</param>
        public Беговые_коньки(string фирма_изготовитель, uint размер, string материал_верха, string наличие_откидывания_пятки)
        {
            Фирма_изготовитель = фирма_изготовитель;
            Размер = размер;
            Материал_верха = материал_верха;
            Наличие_откидывания_пятки = наличие_откидывания_пятки;
        }
        /// <summary>
        /// Конструктор класса Беговые_коньки без параметров
        /// </summary>
        public Беговые_коньки()
        {
            Фирма_изготовитель = null;
            Размер=0;
            Материал_верха = null;
            Наличие_откидывания_пятки = null;
        }
        /// <summary>
        /// Возвращает строку содержащую название коньков
        /// </summary>
        public override string Name_of_Skates
        {
            get { return "Беговые коньки"; }
        }
        /// <summary>
        /// Метод считывания данных об объекте из строки
        /// </summary>
        /// <param name="s">входная строка</param>
        public override void ReadIn(string s)
        {
            string[] k = s.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            Фирма_изготовитель = k[2];
            Размер = uint.Parse(k[3]);
            Материал_верха = k[4];
            Наличие_откидывания_пятки = k[5];
        }
        /// <summary>
        /// Метод, который возвращает информацию об объекте
        /// </summary>
        /// <returns></returns>
        public override string WriteIn()
        {
            return String.Format(Name_of_Skates + " {0,7} {1,2} {2,7} {3,3} "
                , Фирма_изготовитель, Размер, Материал_верха, Наличие_откидывания_пятки);
        }
    }
}
