﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.RowCount = 0;
            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");
            ToolTip toolTip1 = new ToolTip();
            toolTip1.ShowAlways = true;
            toolTip1.SetToolTip(this.label5, "Материал верха");
            toolTip1.SetToolTip(this.label7, "Материал мыса");
            toolTip1.SetToolTip(this.label8, "Высота сапожка");
            toolTip1.SetToolTip(this.label9, "Материал подошвы");
            toolTip1.SetToolTip(this.label10, "Наличие нижнего зубца");
            toolTip1.SetToolTip(this.label11, "Наличие откидывающиеся пятки");
            mass.Add(new Хоккейные_коньки("Адидас",24,"Капровелюр","Кожа",10));
            mass.Add(new Фигурные_коньки("Рибок", 35, "Капровелюр", "Кожа","Есть"));
            mass.Add(new Беговые_коньки("Пума", 42, "Капровелюр", "Да"));
        }
        StreamReader StreamRead;
        List<Коньки> mass = new List<Коньки>();
        int kol = 0;
        bool flforsave = true;
        string stroka;
        string namefailsave = "";
        Хоккейные_коньки w=new Хоккейные_коньки();
        
       

        //Выход из программы
        private void button1_Click(object sender, EventArgs e)
        {
            Exit();
        }

        //О программе
        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Автор: Иванов Сергей\nГруппа: 171ПИ(1)\nВариант: №7", "Автор", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Удаление элемента, элементов
        private void элементToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 1)
            {
                if (DialogResult.No == MessageBox.Show("Вы действительно хотите удалить эти объекты (" + dataGridView1.SelectedRows.Count.ToString() + " шт.)?", "Подтверждение удаления группы элементов", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                {
                    return;
                }
            }
            if (dataGridView1.SelectedRows.Count == 1)
            {
                if (DialogResult.No == MessageBox.Show("Вы действительно хотите удалить этот объект?", "Подтверждение удаления элемента списка", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation))
                {
                    return;
                }
            }
            for (int k = dataGridView1.Rows.Count - 1; k >= 0; k--)
            {
                if (dataGridView1.SelectedRows.Contains(dataGridView1.Rows[k]))
                {
                    mass.RemoveAt(k);
                }
            }
            ListToMatr();
            flforsave = false;
          }

        //Добавление элемента
        private void button3_Click(object sender, EventArgs e)
        {
            if (Prover()) return;
            try
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    mass.Add(new Хоккейные_коньки(textBox3.Text, uint.Parse(textBox4.Text), textBox5.Text, textBox6.Text, uint.Parse(textBox7.Text)));
                }
                if (comboBox1.SelectedIndex == 1)
                {
                    mass.Add(new Фигурные_коньки(textBox3.Text, uint.Parse(textBox4.Text), textBox5.Text, textBox8.Text, textBox9.Text));
                }
                if (comboBox1.SelectedIndex == 2)
                {
                    mass.Add(new Беговые_коньки(textBox3.Text, uint.Parse(textBox4.Text), textBox5.Text, textBox10.Text));
                }
            }
            catch
            {
                MessageBox.Show("Неверное заполнение полей", "Ошибка");
            }
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            ListToMatr();
            flforsave = false;

            //Проверка не выбран ли вывод одного из типов коньков
            if (radioButton4.Checked == true)
            {
                OnlyBeg();
            }
            if (radioButton5.Checked == true)
            {
                OnlyFigur();
            }
            if (radioButton6.Checked == true)
            {
                OnlyHockey();
            }
            flforsave = false;
        }

        //Закрытие формы
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveYesNo();
        }

        //Создать новый список
        private void создатьToolStripButton_Click(object sender, EventArgs e)
        {
            SaveYesNo();
            this.Text = "Список коньков";
            namefailsave = "";
            mass.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");
            flforsave = true;
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
            textBox7.Clear();
            textBox8.Clear();
            textBox9.Clear();
            textBox10.Clear();
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            radioButton4.Checked = false;
            radioButton5.Checked = false;
            radioButton6.Checked = false;
        }

        //Открыть файл
        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mass = new List<Коньки>();
            OpenFile();
            ListToMatr();
            flforsave = true;
        }

        //Открыть файл
        private void открытьToolStripButton_Click(object sender, EventArgs e)
        {
            mass = new List<Коньки>();
            SaveYesNo();
            OpenFile();
            ListToMatr();
            flforsave = true;
        }

        //Сохранить файл
        private void сохранитьToolStripButton_Click(object sender, EventArgs e)
        {
            if (namefailsave == "") SaveFileAs();
            else SaveFile();
        }

        //Очистить список и матрицу
        private void очиститьСписокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");
            mass = new List<Коньки>();
        }

        //вывести количество хоккейных коньков
        private void хоккейныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kol = 0;
            for (int i = 0; i < mass.Count; i++)
                if (mass[i] is Хоккейные_коньки) kol++;
            MessageBox.Show("Всего имеется: " + kol, "Хоккейные коньки");
        }

        //Вывести количество беговых коньков
        private void беговыеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kol = 0;
            for (int i = 0; i < mass.Count; i++)
                if (mass[i] is Беговые_коньки) kol++;
            MessageBox.Show("Всего имеется: " + kol, "Беговые коньки");
        }

        //Вывести количество фигурных коньков
        private void фигурныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kol = 0;
            for (int i = 0; i < mass.Count; i++)
                if (mass[i] is Фигурные_коньки) kol++;
            MessageBox.Show("Всего имеется: " + kol, "Фигурные коньки");
        }

        //Сохранить файл
        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }

        //Сохранить файл как
        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileAs();
        }

        //Поиск по размеру, фирмы, верху
        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");

            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            if (mass.Count != 0)
            {
                dataGridView1.RowCount = mass.Count;

            }
            else
            {
                return;
            }
            ListToMatr();

            if (radioButton2.Checked == true)
            {
                for (int i = 0; i < mass.Count; i++)
                    if (mass[i].Материал_верха != textBox2.Text)
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
            }
            if (radioButton1.Checked == true)
            {
                try
                {
                    for (int i = 0; i < mass.Count; i++)
                        if (mass[i].Размер != int.Parse(textBox2.Text))
                        {
                            dataGridView1.Rows[i].Visible = false;
                        }
                }
                catch
                {
                    MessageBox.Show("Неверный формат введенных данных", "Ошибка");
                    textBox2.Text = "";
                }

            }
            if (radioButton3.Checked == true)
            {
                for (int i = 0; i < mass.Count; i++)
                    if (mass[i].Фирма_изготовитель != textBox2.Text)
                    {
                        dataGridView1.Rows[i].Visible = false;
                    }
            }
            radioButton4.Checked = false;
            radioButton5.Checked = false;
            radioButton6.Checked = false;
        }

        //Показать весь список
        private void показатьВесьСписокToolStripMenuItem_Click(object sender, EventArgs e)
        {
            radioButton4.Checked = false;
            radioButton5.Checked = false;
            radioButton6.Checked = false;
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            textBox2.Text = "";
            ListToMatr();

        }

        //Показать только беговые коньки
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            OnlyBeg();
        }

        //Показать только фигурные коньки
        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            OnlyFigur();
        }

        //Показать только хоккейные коньки
        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            OnlyHockey();
        }

        //Проверка на корректность добавленного элемента
        private bool Prover()
        {
            textBox3.Text.Trim();
            textBox4.Text.Trim();
            textBox5.Text.Trim();
            textBox6.Text.Trim();
            textBox7.Text.Trim();
            textBox8.Text.Trim();
            textBox9.Text.Trim();
            textBox10.Text.Trim();

            //Выбран класс Хоккейные_коньки, обработаем все входные строки из формы для данного класса
            if (comboBox1.SelectedIndex == 0)
            {
                if (textBox3.Text == "")
                {
                    MessageBox.Show("Введите название фирмы изготовителя", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox3.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox5.Text == "")
                {
                    MessageBox.Show("Введите материал верха", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox5.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox6.Text == "")
                {
                    MessageBox.Show("Введите материал мыса", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox6.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                uint razmer;
                if (!uint.TryParse(textBox4.Text, out razmer))
                {
                    MessageBox.Show("Проверьте корректность входных данных!\n Размер является целым положительным число.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox4.Text = "";
                    textBox4.Focus();
                    return true;
                }
                if (razmer >= 60)
                {
                    if (DialogResult.No == MessageBox.Show("Вы уверены, что такой размер?", "!", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        return true;
                    }
                }
                uint visota;
                if (!uint.TryParse(textBox7.Text, out visota))
                {
                    MessageBox.Show("Проверьте корректность входных данных!\n Высота является целым положительным число.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox7.Text = "";
                    textBox7.Focus();
                    return true;
                }
                if (visota >= 60)
                {
                    if (DialogResult.No == MessageBox.Show("Вы уверены, что такая высота сапожка?", "!", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        return true;
                    }
                }
            }

            //Выбран класс Беговые_коньки, обработаем все входные строки из формы для данного класса
            if (comboBox1.SelectedIndex == 1)
            {
                if (textBox3.Text == "")
                {
                    MessageBox.Show("Введите название фирмы изготовителя", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox3.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox5.Text == "")
                {
                    MessageBox.Show("Введите материал верха", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox5.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox8.Text == "")
                {
                    MessageBox.Show("Введите материал подошвы", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox6.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox9.Text == "")
                {
                    MessageBox.Show("Введите наличие нижнего зубца", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox6.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                uint razmer;
                if (!uint.TryParse(textBox4.Text, out razmer))
                {
                    MessageBox.Show("Проверьте корректность входных данных!\n Размер является целым число.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox4.Text = "";
                    textBox4.Focus();
                    return true;
                }
                if (razmer >= 60)
                {
                    if (DialogResult.No == MessageBox.Show("Вы уверены, что такой размер?", "!", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        return true;
                    }
                }
            }

            //Выбран класс Фигурные_коньки, обработаем все входные строки из формы для данного класса
            if (comboBox1.SelectedIndex == 2)
            {
                if (textBox3.Text == "")
                {
                    MessageBox.Show("Введите название фирмы изготовителя", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox3.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox5.Text == "")
                {
                    MessageBox.Show("Введите материал верха", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox5.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox8.Text == "")
                {
                    MessageBox.Show("Введите материал подошвы", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox6.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                if (textBox10.Text == "")
                {
                    MessageBox.Show("Введите наличие откидывания пятки", "Неполные данные", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox3.Focus();
                    return true;
                }
                foreach (char c in textBox10.Text)
                {
                    if (c == '|')
                    {
                        MessageBox.Show("Символ '|' недопутсим во входных данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        textBox2.Focus();
                        return true;
                    }
                }
                uint razmer;
                if (!uint.TryParse(textBox4.Text, out razmer))
                {
                    MessageBox.Show("Проверьте корректность входных данных!\n Размер является целым положительным число.", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    textBox4.Text = "";
                    textBox4.Focus();
                    return true;
                }
                if (razmer >= 60)
                {
                    if (DialogResult.No == MessageBox.Show("Вы уверены, что такой размер?", "!", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //Метод записи из файла в список
        private void OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.FileName = "";
            openFileDialog.Filter = ".txt|*.txt";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bool flag = true;
                StreamRead = new StreamReader(openFileDialog.FileName, System.Text.Encoding.GetEncoding(1251));
                StreamRead.BaseStream.Position = 0;
                int i = -1;
                try
                {
                    do
                    {
                        if (flag == false) break;
                        i++;
                        stroka = StreamRead.ReadLine();
                        if (stroka == null) break;
                        stroka.Trim();
                        string str = "";
                        string[] n = stroka.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                        str += n[0] + " " + n[1];
                        switch (str)
                        {
                            case "Хоккейные коньки":
                                Хоккейные_коньки a = new Хоккейные_коньки();
                                a.ReadIn(stroka);
                                mass.Add((Хоккейные_коньки)a);
                                break;
                            case "Фигурные коньки":
                                Фигурные_коньки b = new Фигурные_коньки();
                                b.ReadIn(stroka);
                                mass.Add((Фигурные_коньки)b);
                                break;
                            case "Беговые коньки":
                                Беговые_коньки c = new Беговые_коньки();
                                c.ReadIn(stroka);
                                mass.Add((Беговые_коньки)c);
                                break;
                            default:
                                MessageBox.Show("Данные записаны неверно!\nНапоминание:данные состоящие из двух слов вводятся через подчеркивание!", "Ошибка", MessageBoxButtons.OK);
                                flag = false;
                                break;
                        }
                        flforsave = true;
                        namefailsave = openFileDialog.FileName;
                        this.Text = namefailsave + " - Список коньков";
                    }
                    while (stroka != null);
                   
                }
                catch
                {
                    MessageBox.Show("Данные записаны неверно!\nНапоминание:данные состоящие из двух слов вводятся через подчеркивание!", "Ошибка", MessageBoxButtons.OK);
                    namefailsave = "";
                    mass.Clear();
                    this.Text = "Список коньков";
                }
                if (flag == false)
                {
                    namefailsave = "";
                    mass.Clear();
                    this.Text = "Список коньков";
                }
                StreamRead.Close();
            }
        }

        //Метод для сохранения списка в новый файл
        private void SaveFileAs()
        {
            SaveFileDialog fas = new SaveFileDialog();
            fas.FileName = "";
            fas.Filter = ".txt|*.txt";
            fas.Title = "Сохранить данные";
            if (fas.ShowDialog() == DialogResult.OK)
            {
                namefailsave = fas.FileName;
                if (namefailsave.EndsWith(".txt"))
                {
                    try
                    {
                        FileStream fs = new FileStream(namefailsave, FileMode.Create, FileAccess.Write);
                        StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
                        foreach (Коньки kon in mass)
                        {
                            sw.WriteLine(kon.WriteIn());
                            sw.Flush();
                        }
                        sw.Close();
                        namefailsave = fas.FileName;
                        this.Text = namefailsave + " - Список коньков";
                        flforsave = true;

                    }
                    catch
                    {
                        MessageBox.Show("Ошибка записи в файл", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
            }
        }

        //Метод записи списка в dataGridView
        private void ListToMatr()
        {
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");

            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            if (mass.Count != 0)
            {
                dataGridView1.RowCount = mass.Count;

            }
            else
            {
                return;
            }

            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                dataGridView1[1, i].Value = mass[i].Фирма_изготовитель;
                dataGridView1[2, i].Value = mass[i].Размер;
                dataGridView1[3, i].Value = mass[i].Материал_верха;
                if (mass[i] is Хоккейные_коньки)
                {
                    Хоккейные_коньки element = (Хоккейные_коньки)mass[i];
                    dataGridView1[0, i].Value = "Хоккейные";
                    dataGridView1[4, i].Value = element.Материал_мыса;
                    dataGridView1[5, i].Value = element.Высота_сапожка;
                    dataGridView1[6, i].Value = " - ";
                    dataGridView1[7, i].Value = " - ";
                    dataGridView1[8, i].Value = " - ";
                }
                if (mass[i] is Фигурные_коньки)
                {
                    Фигурные_коньки element = (Фигурные_коньки)mass[i];
                    dataGridView1[0, i].Value = "Фигурные";
                    dataGridView1[4, i].Value = " - ";
                    dataGridView1[5, i].Value = " - ";
                    dataGridView1[6, i].Value = element.Материал_подошвы;
                    dataGridView1[7, i].Value = element.Наличие_нижнего_зубца;
                    dataGridView1[8, i].Value = " - ";
                }
                if (mass[i] is Беговые_коньки)
                {
                    Беговые_коньки element = (Беговые_коньки)mass[i];
                    dataGridView1[0, i].Value = "Беговые";
                    dataGridView1[4, i].Value = " - ";
                    dataGridView1[5, i].Value = " - ";
                    dataGridView1[6, i].Value = " - ";
                    dataGridView1[7, i].Value = " - ";
                    dataGridView1[8, i].Value = element.Наличие_откидывания_пятки;
                }
            }
        }

        //Метод сохранения списка в файл
        private void SaveFile()
        {
            if (namefailsave == "")
            {
                SaveFileAs();
            }
            else
            {
                FileStream fs = new FileStream(namefailsave, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
                foreach (Коньки al in mass)
                {
                    sw.WriteLine(al.WriteIn());
                    sw.Flush();
                }
                sw.Close();
            }
            flforsave = true;
        }

        //Метод вывода только хоккейных коньков
        private void OnlyHockey()
        {
            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (mass.Count != 0)
            {
                dataGridView1.RowCount = mass.Count;

            }
            else
            {
                return;
            }
            ListToMatr();

            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (mass[i] is Беговые_коньки)
                {
                    dataGridView1.Rows[i].Visible = false;
                }
                if (mass[i] is Фигурные_коньки)
                {
                    dataGridView1.Rows[i].Visible = false;
                }
            }
            if (dataGridView1.RowCount > 0)
            {
                dataGridView1.Rows[0].Selected = false;
            }

        }

        ////Метод вывода только фигурных коньков
        private void OnlyFigur()
        {
            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (mass.Count != 0)
            {
                dataGridView1.RowCount = mass.Count;

            }
            else
            {
                return;
            }
            ListToMatr();

            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (mass[i] is Беговые_коньки)
                {
                    dataGridView1.Rows[i].Visible = false;
                }
                if (mass[i] is Хоккейные_коньки)
                {
                    dataGridView1.Rows[i].Visible = false;
                }
            }
            if (dataGridView1.RowCount > 0)
            {
                dataGridView1.Rows[0].Selected = false;
            }
        }

        ////Метод вывода только беговых коньков
        private void OnlyBeg()
        {
            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("", "");
            dataGridView1.Columns.Add("", "Фирма");
            dataGridView1.Columns.Add("", "Размер");
            dataGridView1.Columns.Add("", "Материал верха");
            dataGridView1.Columns.Add("", "Материал мыса");
            dataGridView1.Columns.Add("", "Высота сапожка");
            dataGridView1.Columns.Add("", "Материал подошвы");
            dataGridView1.Columns.Add("", "Наличие нижрего зубца");
            dataGridView1.Columns.Add("", "Наличие откидывающиеся пятки");
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                dataGridView1.Columns[j].SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            if (mass.Count != 0)
            {
                dataGridView1.RowCount = mass.Count;

            }
            else
            {
                return;
            }
            ListToMatr();

            for (int i = 0; i < dataGridView1.RowCount; i++)
            {
                if (mass[i] is Хоккейные_коньки)
                {
                    dataGridView1.Rows[i].Visible = false;
                }
                if (mass[i] is Фигурные_коньки)
                {
                    dataGridView1.Rows[i].Visible = false;
                }
            }
            if (dataGridView1.RowCount > 0)
            {
                dataGridView1.Rows[0].Selected = false;
            }
        }

        //Метод, который предлагает сохранить пользователю список
        private void SaveYesNo()
        {
            {

                if (flforsave == false)
                {
                    if (DialogResult.Yes == MessageBox.Show("Сохранить изменения" + (namefailsave != "" ? (" в \"" + namefailsave + "\"") : ("")) + "?", "Список коньков", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                    {
                        SaveFile();
                    }
                }
            }
        }

        //Метод, который спрашивает у пользователя точно ли он хочет выйти
        private void Exit()
        {
            DialogResult res = MessageBox.Show("Уверены?", "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (res == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}