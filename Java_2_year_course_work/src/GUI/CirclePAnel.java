package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * @author Ivanov Sergey
 */
public class CirclePAnel extends JPanel {
    
    public Color thisColor;
    public CirclePAnel() {
        this.thisColor = Color.RED; 
        this.setSize(new Dimension(21, 21));
    }
    
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(thisColor);
        g.fillOval(0, 0, 20, 20); 
    }
}
