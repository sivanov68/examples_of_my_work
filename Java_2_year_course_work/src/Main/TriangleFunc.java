package Main;

/**
 * @author Ivanov Sergey
 */
public class TriangleFunc extends Func{

    public TriangleFunc(double a, double b, double c) {
        super(a, b, c);
    }

    @Override
    public double getValue(double x) {
        if (x <= a) {
            return 0;
        }
        if (c <= x) {
            return 0;
        }
        if (x >= a && x <= b) {
            return (x - a) / b - a;
        }
        if (b <= x && x <= c) {
            return (c - x) / (c - b);
        }
        return 0;
    }
    
    @Override
    public double[] getParametr() {
        double[] paramtr = new double[]{a,b,c};
        return paramtr;
    }
    
    @Override
    public String getNameOfClass() {
        return "TriangleFunc";
    }
}