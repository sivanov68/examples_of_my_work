package Main;

/**
 * @author Ivanov Sergey
 */
public class Statement {

    private Variable variable;
    private FuzzySet fuzzySet;

    public String getNameVar() {
        return variable.getName();
    }

    public String getNameFuzz() {
        return fuzzySet.getName();
    }

    public Statement(Variable var, FuzzySet fuz) {
        this.variable = var;
        this.fuzzySet = fuz;
    }
}