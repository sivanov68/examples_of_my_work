package Main;

/**
 * @author Ivanov Sergey
 */
public class Conclusion {

    private Statement statement;
    private double weight;

    public Conclusion(Variable var, FuzzySet fuz) {
        statement = new Statement(var, fuz);
        this.weight = 0;
    }
    
    public Conclusion(Variable var, FuzzySet fuz, double weight) {
        statement = new Statement(var, fuz);
        this.weight = weight;
    }

    public void addWeight(double number) {
        this.weight = number;
    }

    public Statement getStatement() {
        return statement;
    }

    public double getWeight() {
        return weight;
    }
}
