package Main;

/**
 * @author Ivanov Sergey
 */
public class Condition {

    private Statement statement;

    public Condition(Variable var, FuzzySet fuz) {
        statement = new Statement(var, fuz);
    }

    public Statement getStatement() {
        return statement;
    }
}
