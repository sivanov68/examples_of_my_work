package Main;

/**
 *
 * @author Ivanov Sergey
 */

public interface FuzzySetInterface {
    double getValue();
    
}
