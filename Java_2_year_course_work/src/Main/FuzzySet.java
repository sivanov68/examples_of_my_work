package Main;

/**
 * @author Ivanov Sergey
 */
//нечеткое множество, хранит имя и значение
public class FuzzySet {

    private String fuzzySetName;
    private Func func;

    @SuppressWarnings("empty-statement")
    public FuzzySet(String fuzzySetName, double a, double b, double c, double d) {
        this.func = new TrapezeFunc(a, b, c, d);;
        this.fuzzySetName = fuzzySetName;
    }

    @SuppressWarnings("empty-statement")
    public FuzzySet(String fuzzySetName, double a, double b, double c) {
        this.func = new TriangleFunc(a, b, c);;
        this.fuzzySetName = fuzzySetName;
    }

    public String getName() {
        return fuzzySetName;
    }

    public void setName(String name) {
        fuzzySetName = name;
    }

    public void setFunc(TriangleFunc func) {
            this.func = new TriangleFunc(func.a, func.b, func.c);
    }
    
    public void setFunc(TrapezeFunc func) {
            this.func = new TrapezeFunc(func.a, func.b, func.c, func.d);
    }

    public String getNameOfFunc() {
        return func.getNameOfClass();
    }

    public Func getFunc() {
        return func;
    }
}
