package Main;

/**
 * @author Ivanov Sergey
 */
public class TrapezeFunc extends Func{

    protected double d;

    public TrapezeFunc(double a, double b, double c, double d) {
        super(a,b,c);
        this.d = d;
    }

    @Override
    public double getValue(double x) {
        if (x <= a) {
            return 0;
        }
        if (d <= x) {
            return 0;
        }

        if (x >= a && x <= b) {
            return (x - a) / b - a;
        }
        if (c <= x && x <= d) {
            return (d - x) / (d - c);
        }
        if (b <= x && x <= c) {
            return 1;
        }
        return 0;
    }

    @Override
    public double[] getParametr() {
        double[] paramtr = new double[]{a, b, c, d};
        return paramtr;
    }
    
    @Override
    public String getNameOfClass() {
        return "TrapezeFunc";
    }
}