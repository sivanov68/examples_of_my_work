package Main;

/**
 * @author Ivanov Sergey
 */
import java.util.*;

//Лингвистическая переменная
public class Variable {

    private String name;
    private double x1, x2;
    private ArrayList<FuzzySet> list;

    public Variable(String name, double x1, double x2) {
        this.name = name;
        this.x1 = x1;
        this.x2 = x2;
        list = new ArrayList<>();
    }

    public Variable(String name) {
        this.name = name;
        this.x1 = 0;
        this.x2 = 0;
        list = new ArrayList<>();
    }

    public Variable(String name, double x1) {
        this(name, x1, x1);
    }

    public void addFuzzySet(FuzzySet fuzzyset) {
        list.add(fuzzyset);
    }

    public void removeFuzzySet(int index) {
        list.remove(index);
    }

    public ArrayList<FuzzySet> getList() {
        return list;
    }

    public String getName() {
        return name;
    }

    public String getNameFuzzy(int index) {
        return list.get(index).getName();
    }

    public FuzzySet getFuzzy(int index) {
        return list.get(index);
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setX1X2(double x1, double x2) {
        this.x1 = x1;
        this.x2 = x2;
    }
}
