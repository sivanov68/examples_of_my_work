package Main;

import java.util.ArrayList;

/**
 * @author Ivanov Sergey
 */
public class Rule {

    private ArrayList<Condition> listOfCond;
    private Conclusion conc;
    private int connect;//0 если or,1 если and

    public Rule(ArrayList<Condition> list, Conclusion conc, int connect) {
        listOfCond = new ArrayList<>();
        for (Condition cond : list) {
            listOfCond.add(cond);
        }
        this.conc = conc;
        this.connect = connect;
    }

    public Conclusion getConclusion() {
        return conc;
    }

    public ArrayList<Condition> getCondition() {
        return listOfCond;
    }

    public String printRule() {
        String stroka = " If";
        int number = 0;
        for (Condition cond : listOfCond) {
            number++;
            stroka += " ( " + cond.getStatement().getNameVar() + " is " + cond.getStatement().getNameFuzz() + " )";
            if (number != listOfCond.size()) {
                if (connect == 0) {
                    stroka += " or ";
                }
                if (connect == 1) {
                    stroka += " and ";
                }
            }
        }
        stroka += " then ( " + conc.getStatement().getNameVar() + " is " + conc.getStatement().getNameFuzz() + " ) ( " + conc.getWeight() + " )";
        return stroka;
    }
}
