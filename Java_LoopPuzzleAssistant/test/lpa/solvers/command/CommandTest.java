package lpa.solvers.command;

import junit.framework.TestCase;
import lpa.model.Edge;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
public class CommandTest extends TestCase {

    public CommandTest(String testName) {
        super(testName);
    }

    /**
     * Test of execute method, of class Command.
     */
    public void testExecute1() {
        System.out.println("execute");
        boolean result = false;
        Command command = new Command(new Edge(0, 0, "PRESENT"));
        assertEquals(command.getExecuted(), result);
    }

    /**
     * Test of execute method, of class Command.
     */
    public void testExecute2() {
        System.out.println("execute");
        boolean result = true;
        Command command = new Command(new Edge(0, 0, "PRESENT"));
        command.execute();
        assertEquals(command.getExecuted(), result);
    }

    /**
     * Test of execute method, of class Command.
     */
    public void testExecuteException1() {
        try {
            System.out.println("execute");
            Command command = new Command(new Edge(0, 0, "PRESENT"));
            command.execute();
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    /**
     * Test of undo method, of class Command.
     */
    public void testUndo1() {
        System.out.println("undo");
        Command command = new Command(new Edge(0, 0, "PRESENT"));
        command.execute();
        command.undo();
        boolean flag = false;
        assertEquals(command.isExecute(), flag);
    }

    /**
     * Test of undo method, of class Command.
     */
    public void testUndo2() {
        System.out.println("undo");
        Command command = new Command(new Edge(0, 0, "PRESENT"));
        command.execute();
        command.undo();
        command.execute();
        boolean flag = true;
        assertEquals(command.isExecute(), flag);
    }

    /**
     * Test of undo method, of class Command.
     */
    public void testUndoException1() {
        try {
            System.out.println("undoException");
            Command command = new Command(new Edge(0, 0, "PRESENT"));
            command.undo();
        } catch (IllegalStateException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    /**
     * Test of redo method, of class Command.
     */
    public void testRedo1() {
        System.out.println("redo");
        Command command = new Command(new Edge(0, 0, "PRESENT"));
        command.execute();
        command.redo();
        boolean flag = false;
        assertEquals(command.isExecute(), flag);
    }
    
     /**
     * Test of redo method, of class Command.
     */
    public void testRedo2() {
        System.out.println("redo");
        Command command = new Command(new Edge(0, 0, "PRESENT"));
        command.execute();
        command.redo();
        command.execute();
        boolean flag = true;
        assertEquals(command.isExecute(), flag);
    }
    
    public void testRedoException1() {
        try {
            System.out.println("redoException");
            Command command = new Command(new Edge(0, 0, "PRESENT"));
            command.redo();
        } catch (IllegalStateException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail(e.toString());
        }
    }
}
