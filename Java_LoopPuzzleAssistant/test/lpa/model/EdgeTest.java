package lpa.model;

import junit.framework.TestCase;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
public class EdgeTest extends TestCase {

    public EdgeTest(String testName) {
        super(testName);
    }

    /**
     * Test of getEdgeState method, of class Edge.
     */
    public void testGetEdgeState1() {
        System.out.println("getEdgeState");
        Edge edge = new Edge(0, 0);
        EdgeState edgeState = EdgeState.valueOf("ABSENT");
        assertEquals(edge.getEdgeState(), edgeState);
    }

    /**
     * Test of getEdgeState method, of class Edge.
     */
    public void testGetEdgeState2() {
        System.out.println("getEdgeState");
        Edge edge = new Edge(0, 0, "PRESENT");
        EdgeState edgeState = EdgeState.valueOf("PRESENT");
        assertEquals(edge.getEdgeState(), edgeState);
    }

    /**
     * Test of setEdgeState method, of class Edge.
     */
    public void testSetEdgeState1() {
        System.out.println("setEdgeState");
        Edge edge = new Edge(0, 0, "ABSENT");
        edge.setEdgeState(EdgeState.PRESENT.toString());
        EdgeState edgeState = EdgeState.valueOf("PRESENT");
        assertEquals(edge.getEdgeState(), edgeState);

    }

    /**
     * Test of setEdgeState method, of class Edge.
     */
    public void testSetEdgeState2() {
        System.out.println("setEdgeState");
        Edge edge = new Edge(0, 0, "ABSENT");
        edge.setEdgeState(EdgeState.UNDERTERMINED.toString());
        EdgeState edgeState = EdgeState.valueOf("UNDERTERMINED");
        assertEquals(edge.getEdgeState(), edgeState);

    }
}
