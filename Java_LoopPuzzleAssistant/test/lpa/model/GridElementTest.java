package lpa.model;

import junit.framework.TestCase;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
public class GridElementTest extends TestCase {

    public GridElementTest(String testName) {
        super(testName);
    }

    /**
     * Test of getX method, of class GridElement.
     */
    public void testGetX() {
        System.out.println("getX");
        GridElement instance = new GridElement(0, 0);
        int x = 0;
        assertEquals(x, instance.getX());
    }

    /**
     * Test of getY method, of class GridElement.
     */
    public void testGetY() {
        System.out.println("getY");
        GridElement instance = new GridElement(0, 0);
        int y = 0;
        assertEquals(y, instance.getX());
    }
}
