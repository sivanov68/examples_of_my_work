package lpa.model;

import java.util.Scanner;
import junit.framework.TestCase;
import lpa.solvers.Violation;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
public class GridTest extends TestCase {

    public GridTest(String testName) {
        super(testName);
    }

    /**
     * Test of getGridElement method, of class Grid. get vertex
     */
    public void testGetGridElement1() {
        System.out.println("getGridElement");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n 2     0  |"
                + "\n+-+ +-+ +-+\n  | | |2|  \n+ + +-+ +-+\n  |    0  |"
                + "\n+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        int x = 0;
        int y = 0;
        GridElement expResult = new Vertex(0, 0);
        GridElement result = instance.getGridElement(x, y);
        assertEquals(expResult.getX(), result.getX());
        assertEquals(expResult.getY(), result.getY());
        assertTrue(expResult.getClass() == result.getClass());
    }

    /**
     * Test of getGridElement method, of class Grid. get edge
     */
    public void testGetGridElement2() {
        System.out.println("getGridElement");
        System.out.println("Check");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n 2     0  |"
                + "\n+-+ +-+ +-+\n  | | |2|  \n+ + +-+ +-+\n  |    0  |"
                + "\n+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        int x = 0;
        int y = 1;
        GridElement expResult = new Edge(0, 1);
        GridElement result = instance.getGridElement(x, y);
        assertEquals(expResult.getX(), result.getX());
        assertEquals(expResult.getY(), result.getY());
        assertTrue(expResult instanceof Edge);
    }

    /**
     * Test of getGridElement method, of class Grid. get cell
     */
    public void testGetGridElement3() {
        System.out.println("getGridElement");
        System.out.println("Check");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n 2     0  |"
                + "\n+-+ +-+ +-+\n  | | |2|  \n+ + +-+ +-+\n  |    0  |"
                + "\n+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        int x = 1;
        int y = 1;
        GridElement expResult = new Cell(1, 1);
        GridElement result = instance.getGridElement(x, y);
        assertEquals(expResult.getX(), result.getX());
        assertEquals(expResult.getY(), result.getY());
        assertTrue(expResult instanceof Cell);
    }

    public void testGetGridElementException1() {
        try {
            System.out.println("GetGridElementException");
            Grid instance = new Grid(null);
            GridElement gridElement = instance.getGridElement(0, 0);
        } catch (NullPointerException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    /**
     * Test of Check method, of class Grid.
     */
    public void testCheck1() {
        System.out.println("Check");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n 2     0  |"
                + "\n+-+ +-+ +-+\n  | | |2|  \n+ + +-+ +-+\n  |    0  |"
                + "\n+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        boolean expResult = false;
        boolean result = instance.Check();
        assertEquals(expResult, result);
    }

    //Check for loop
    public void testCheck2() {
        System.out.println("Check");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        Violation myViolation = new Violation(instance);
        boolean checkCorrect = instance.Check();
        boolean loopFind = myViolation.checkGridForLoops();
        boolean result = false;
        assertEquals(loopFind & checkCorrect, result);
    }

    public void testCheckException1() {
        try {
            System.out.println("CheckException");
            Grid instance = new Grid(null);
            instance.Check();
        } catch (NullPointerException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    /**
     * Test of setRowPlusOne method, of class Grid.
     */
    public void setRowPlusOne(Grid grid) {
        int count = grid.getRow();
        count++;
        grid.setRowPlusOne();
        int result = grid.getRow();
        assertEquals(count, result);
    }

    public void testsetRowPlusOne1() {
        System.out.println("setRowPlusOne1");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        setRowPlusOne(instance);
    }

    public void setRowMinusOne(Grid grid) {
        int count = grid.getRow();
        count--;
        grid.setRowMinusOne();
        int result = grid.getRow();
        assertEquals(count, result);
    }

    /**
     * Test of serRowMinusOne method, of class Grid.
     */
    public void testSetRowMinusOne1() {
        System.out.println("setRowMinusOne1");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        setRowMinusOne(instance);
    }

    public void setColumnPlusOne(Grid grid) {
        int count = grid.getColumn();
        count++;
        grid.setColumnPlusOne();
        int result = grid.getColumn();
        assertEquals(count, result);
    }

    /**
     * Test of setColumnPlusOne method, of class Grid.
     */
    public void testSetColumnPlusOne1() {
        System.out.println("setColumnPlusOne1");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        setColumnPlusOne(instance);
    }

    public void setColumnMinusOne(Grid grid) {
        int count = grid.getColumn();
        count--;
        grid.setColumnMinusOne();
        int result = grid.getColumn();
        assertEquals(count, result);
    }

    /**
     * Test of serColumnMinusOne method, of class Grid.
     */
    public void testSetColumnMinusOne1() {
        System.out.println("setColumnMinusOne1");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        setColumnMinusOne(instance);
    }

    public void addRow(Grid grid) {
        int rows = grid.getRow();
        rows++;
        rows++;
        grid.addRow();
        assertEquals(rows, grid.getRow());
    }

    /**
     * Test of addRow method, of class Grid.
     */
    public void testAddRow1() {
        System.out.println("addRow");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        addRow(instance);
    }

    public void deleteRow(Grid grid) {
        int rows = grid.getRow();
        rows--;
        rows--;
        grid.deleteRow();
        assertEquals(rows, grid.getRow());
    }

    public void testdeleteRowException1() {
        try {
            System.out.println("deleteRowException");
            String s = "+ +-+ +-+ +";
            Scanner scanner = new Scanner(s);
            Grid instance = new Grid(scanner);
            instance.deleteRow();
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail(e.toString());
        }
    }

    /**
     * Test of deleteRow method, of class Grid.
     */
    public void testDeleteRow1() {
        System.out.println("deleteRow");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        deleteRow(instance);
    }

    public void addColumn(Grid grid) {
        int colmuns = grid.getColumn();
        colmuns++;
        colmuns++;
        grid.addColumn();
        assertEquals(colmuns, grid.getColumn());
    }

    /**
     * Test of addColumn method, of class Grid.
     */
    public void testAddColumn1() {
        System.out.println("addColumn");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        addColumn(instance);
    }

    public void deleteColumn(Grid grid) {
        int colmuns = grid.getColumn();
        colmuns--;
        colmuns--;
        grid.deleteColumn();
        assertEquals(colmuns, grid.getColumn());
    }

    /**
     * Test of deleteColumn method, of class Grid.
     */
    public void testDeleteColumn1() {
        System.out.println("deleteColumn");
        String s = "+ +-+ +-+ +\n 2|3|3|3|  \n+-+ +-+ +-+\n|2     0  |\n"
                + "+ + +-+ +-+\n|   | |2|  \n+-+ +-+ +-+\n  |    0  |\n"
                + "+ +-+-+ +-+\n 0    |3|  \n+ + + +-+ +";
        Scanner scanner = new Scanner(s);
        Grid instance = new Grid(scanner);
        deleteColumn(instance);
    }

    public void testdeleteColumnException1() {
        try {
            System.out.println("deleteColumnException");
            String s = "+\n \n+";
            Scanner scanner = new Scanner(s);
            Grid instance = new Grid(scanner);
            instance.deleteColumn();
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail(e.toString());
        }
    }
}
