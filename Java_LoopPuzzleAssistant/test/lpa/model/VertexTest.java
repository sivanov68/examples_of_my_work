package lpa.model;

import junit.framework.TestCase;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
public class VertexTest extends TestCase {

    public VertexTest(String testName) {
        super(testName);
    }

    /**
     * Test of addEdge method, of class Vertex.
     */
    public void testAddEdge1() {
        int countAdded = 0;
        Vertex vertex = new Vertex(0, 0);
        vertex.addEdge(new Edge(0, 0, "PRESENT"));
        countAdded++;
        Histrogram histrogram = vertex.elementGroup.kolvoEdges();
        int count = histrogram.get(EdgeState.PRESENT);
        assertEquals(count, countAdded);
    }

    public void testAddEdge2() {
        int countAdded = 0;
        Vertex vertex = new Vertex(0, 0);
        vertex.addEdge(new Edge(0, 0, "PRESENT"));
        vertex.addEdge(new Edge(0, 0, "PRESENT"));
        vertex.addEdge(new Edge(0, 0, "PRESENT"));
        vertex.addEdge(new Edge(0, 0, "PRESENT"));
        vertex.addEdge(new Edge(0, 0, "PRESENT"));
        vertex.addEdge(new Edge(0, 0, "PRESENT"));
        countAdded += 6;
        Histrogram histrogram = vertex.elementGroup.kolvoEdges();
        int count = histrogram.get(EdgeState.PRESENT);
        assertEquals(count, countAdded);
    }

    public void testAddEdge3() {
        int countAdded = 0;
        Vertex vertex = new Vertex(0, 0);
        vertex.addEdge(new Edge(0, 0, "ABSENT"));
        vertex.addEdge(new Edge(0, 0, "ABSENT"));
        countAdded += 2;
        Histrogram histrogram = vertex.elementGroup.kolvoEdges();
        int count = histrogram.get(EdgeState.ABSENT);
        assertEquals(count, countAdded);
    }

    public void testAddEdge4() {
        int countAdded = 0;
        Vertex vertex = new Vertex(0, 0);
        Histrogram histrogram = vertex.elementGroup.kolvoEdges();
        int count = histrogram.get(EdgeState.PRESENT);
        assertEquals(count, countAdded);
    }
}
