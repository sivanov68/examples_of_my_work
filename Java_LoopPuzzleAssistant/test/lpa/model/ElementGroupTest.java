package lpa.model;

import java.util.ArrayList;
import junit.framework.TestCase;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
public class ElementGroupTest extends TestCase {

    public ElementGroupTest(String testName) {
        super(testName);
    }

    /**
     * Test of addToList method, of class ElementGroup.
     */
    public void testAddToList1() {
        System.out.println("addToList");
        int countSended = 0;
        ElementGroup group = new ElementGroup();
        group.addToList(new Edge(0, 0, "PRESENT"));
        countSended++;
        ArrayList<Edge> list = group.getList();
        int count = list.size();
        assertEquals(count, countSended);
    }

    /**
     * Test of addToList method, of class ElementGroup.
     */
    public void testAddToList2() {
        System.out.println("addToList");
        int countSended = 0;
        ElementGroup group = new ElementGroup();
        group.addToList(new Edge(0, 0, "PRESENT"));
        countSended++;
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        group.addToList(new Edge(0, 0, "UNDERTERMINED"));
        countSended++;
        ArrayList<Edge> list = group.getList();
        int count = list.size();
        assertEquals(count, countSended);
    }

    /**
     * Test of addToList method, of class ElementGroup.
     */
    public void testAddToList3() {
        System.out.println("addToList");
        int countSended = 0;
        ElementGroup group = new ElementGroup();
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        ArrayList<Edge> list = group.getList();
        int count = list.size();
        assertEquals(count, countSended);
    }

    /**
     * Test of kolvoEdges method, of class ElementGroup.
     */
    public void testKolvoEdges1() {
        System.out.println("kolvoEdges");
        int countSended = 0;
        ElementGroup group = new ElementGroup();
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        Histrogram histogram = group.kolvoEdges();
        int absent = histogram.get(EdgeState.ABSENT);
        assertEquals(absent, countSended);
    }
    
    /**
     * Test of kolvoEdges method, of class ElementGroup.
     */
    public void testKolvoEdges2() {
        System.out.println("kolvoEdges");
        int countSended = 0;
        ElementGroup group = new ElementGroup();
        group.addToList(new Edge(0, 0, "ABSENT"));
        countSended++;
        group.addToList(new Edge(0, 0, "PRESENT"));
        group.addToList(new Edge(0, 0, "UNDERTERMINED"));
        Histrogram histogram = group.kolvoEdges();
        int absent = histogram.get(EdgeState.ABSENT);
        assertEquals(absent, countSended);
    }
}
