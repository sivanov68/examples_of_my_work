package lpa.model;

import junit.framework.TestCase;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
public class CellTest extends TestCase {

    public CellTest(String testName) {
        super(testName);
    }

    /**
     * Test of getCellState method, of class Cell.
     */
    public void testGetCellState1() {
        System.out.println("getCellState");
        Cell cell = new Cell(0, 0, 4);
        int count = 4;
        assertEquals(count, cell.getCellState());
    }

    /**
     * Test of getCellState method, of class Cell.
     */
    public void testGetCellState2() {
        System.out.println("getCellState");
        Cell cell = new Cell(0, 0, 0);
        int count = 0;
        assertEquals(count, cell.getCellState());
    }

    /**
     * Test of getCellState method, of class Cell.
     */
    public void testGetCellState3() {
        System.out.println("getCellState");
        Cell cell = new Cell(0, 0, 2);
        int count = 2;
        assertEquals(count, cell.getCellState());
    }

    /**
     * Test of setCellState method, of class Cell.
     */
    public void testSetCellState1() {
        System.out.println("setCellState");
        Cell cell = new Cell(0, 0, 2);
        cell.setCellState(0);
        int count = 0;
        assertEquals(count, cell.getCellState());
    }

    /**
     * Test of setCellState method, of class Cell.
     */
    public void testSetCellState2() {
        System.out.println("setCellState");
        Cell cell = new Cell(0, 0, 4);
        cell.setCellState(0);
        int count = 0;
        assertEquals(count, cell.getCellState());
    }
}
