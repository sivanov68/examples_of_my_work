package lpa.solvers;

import java.util.ArrayList;
import java.util.Scanner;
import lpa.model.Cell;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.Grid;
import lpa.model.Histrogram;
import lpa.model.Vertex;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class of possible violations in grid
 */
public class Violation {

    /*
     * grid is grid, which be use for find violations
     */
    private Grid grid;

     /**
     * Constructor of StrategyVertex
     *
     * @param grid is a input grid
     */
    public Violation(Grid grid) {
        this.grid = grid;
    }

     /**
     * Find violation for cell
     * A cell containing digit d is surrounded by more than d yes-edges.
     * 
     * @param cell is an input cell
     * @return \result which show grid is correct or not 
     * @pre {@code cell != null}
     * @throws NullPointerException if grid == null
     */
    public boolean findViolation1(Cell cell) throws NullPointerException {
        Cell myCell1;
        myCell1 = new Cell(cell.getX(), cell.getY());
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY()));
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY()));
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1));
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1));
        Histrogram histrogram1 = myCell1.elementGroup.kolvoEdges();
        int tekcount1 = histrogram1.get(EdgeState.PRESENT);

        if (tekcount1 != cell.getCellState()) {
            return false;
        } else {
            return true;
        }
    }

     /**
     * Find violation for cell
     * A cell containing digit d is surrounded by more than 4  d no-edges.
     *
     * @param cell is an input cell
     * @return \result which show grid is correct or not 
     * @pre {@code cell != null}
     * @throws NullPointerException if grid == null
     */
    public boolean findViolation2(Cell cell) throws NullPointerException {
        Cell myCell1;
        myCell1 = new Cell(cell.getX(), cell.getY());
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY()));
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY()));
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1));
        myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1));
        Histrogram histrogram1 = myCell1.elementGroup.kolvoEdges();
        int tekcount = histrogram1.get(EdgeState.ABSENT);
        if (4 - tekcount < cell.getCellState()) {
            return false;
        } else {
            return true;
        }
    }

     /**
     * Find violation for vertex
     * A vertex is surrounded by more than two yes-edges (branching path)
     *
     * @param vertex is an input cell
     * @return \result which show grid is correct or not 
     * @pre {@code vertex != null}
     * @throws NullPointerException if grid == null
     */
    public boolean findViolation3(Vertex vertex) {
        if(grid == null) {
            throw  new IllegalArgumentException("Grid is null");
        }
        Vertex newVertex = new Vertex(vertex.getX(), vertex.getY());

        if (newVertex.getX() + 1 < grid.getRow()) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX() + 1, newVertex.getY()));
        }
        if (newVertex.getX() - 1 >= 0) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX() - 1, newVertex.getY()));
        }
        if (newVertex.getY() + 1 < grid.getColumn()) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() + 1));
        }
        if (newVertex.getY() - 1 >= 0) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() - 1));
        }

        Histrogram histrogram1 = newVertex.elementGroup.kolvoEdges();
        int tekcount = histrogram1.get(EdgeState.PRESENT);
        if (tekcount > 2) {
            return false;
        } else {
            return true;
        }
    }
    
     /**
     * A vertex is surrounded by one yes-edge and the other edges are marked
     * as no-edges (dead end).
     *
     * @param vertex is an input cell
     * @return \result which show grid is correct or not 
     * @pre {@code vertex != null}
     * @throws NullPointerException if grid == null
     */ 
    public boolean findViolation4(Vertex vertex) throws NullPointerException {
        Vertex newVertex = new Vertex(vertex.getX(), vertex.getY());

        if (newVertex.getX() + 1 < grid.getRow()) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX() + 1, newVertex.getY()));
        }
        if (newVertex.getX() - 1 >= 0) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX() - 1, newVertex.getY()));
        }
        if (newVertex.getY() + 1 < grid.getColumn()) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() + 1));
        }
        if (newVertex.getY() - 1 >= 0) {
            newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() - 1));
        }

        Histrogram histrogram1 = newVertex.elementGroup.kolvoEdges();
        int countPresent = histrogram1.get(EdgeState.PRESENT);
        int countAbsent = histrogram1.get(EdgeState.ABSENT);
        int countAll = histrogram1.get(EdgeState.ABSENT) + histrogram1.get(EdgeState.UNDERTERMINED);
        if (countPresent == 1 && countAbsent == countAll) {
            return false;
        } else {
            return true;
        }
    }

     /**
     * There is a loop of yes-edges, and there are also some yes-edges outside
     * that loop
     *
     * @param edge is an input cell
     * @return \result which show grid is correct or not 
     * @pre {@code edge != null}
     * @throws NullPointerException if grid == null
     */ 
    public boolean findViolation5(Edge edge) throws NullPointerException {
        Grid finalGrid;
        String s = grid.toString();
        Scanner scan = new Scanner(s);
        finalGrid = new Grid(scan);
        int stopX, stopY;
        int startX, startY;
        startX = edge.getX();
        stopX = edge.getX();
        stopY = edge.getY();
        startY = edge.getY();
        boolean flag = false;
        int q = 0;
        Edge nowEdge = (Edge) finalGrid.getGridElement(stopX, stopY);
        nowEdge.setEdgeState(EdgeState.ABSENT.toString());


        while (true) {
            if (startX == stopX && startY == stopY && q != 0) {
                break;
            }
            flag = false;
            q++;
            if (startX % 2 == 0) {
                if (startX + 1 < finalGrid.getRow() && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX++;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX--;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startY + 2 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX, startY + 2) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX, startY + 2);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startY += 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startY - 2 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX, startY - 2) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX, startY - 2);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startY -= 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX--;
                            startY--;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 1 < finalGrid.getRow() && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX++;
                            startY--;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }
            }
            if (startX % 2 == 1) {
                if (startX - 2 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX - 2, startY) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 2, startY);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX -= 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 2 < finalGrid.getRow() && flag == false) {
                    if (finalGrid.getGridElement(startX + 2, startY) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 2, startY);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX += 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 1 < finalGrid.getRow() && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX++;
                            startY--;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 1 < finalGrid.getRow() && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX++;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX--;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            flag = true;
                            startX--;
                            startY--;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }
            }

            if (q == 2) {
                Edge finalEdge = (Edge) finalGrid.getGridElement(stopX, stopY);
                finalEdge.setEdgeState(EdgeState.PRESENT.toString());
            }



            if (flag == false) {
                break;
            }
        }
        if (flag == false) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * There is a loop of yes-edges, and there are also some yes-edges outside
     * that loop
     *
     * @param edge is an input cell
     * @return \result which contain edges in loop
     * @pre {@code edge != null}
     * @throws NullPointerException if grid == null
     */ 
    public ArrayList<Edge> findViolation5forAllGridList(Edge edge) throws NullPointerException {
        Grid finalGrid;
        ArrayList<Edge> list = new ArrayList<>();
        String s = grid.toString();
        Scanner scan = new Scanner(s);
        finalGrid = new Grid(scan);
        int stopX, stopY;
        int startX, startY;
        startX = edge.getX();
        stopX = edge.getX();
        stopY = edge.getY();
        startY = edge.getY();
        boolean flag = false;
        int q = 0;
        Edge nowEdge = (Edge) finalGrid.getGridElement(stopX, stopY);
        nowEdge.setEdgeState(EdgeState.ABSENT.toString());


        while (true) {

            if (startX == stopX && startY == stopY && q != 0) {
                break;
            }
            flag = false;
            q++;
            if (startX % 2 == 0) {
                if (startX + 1 < finalGrid.getRow() && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX++;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX--;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startY + 2 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX, startY + 2) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX, startY + 2);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startY += 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startY - 2 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX, startY - 2) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX, startY - 2);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startY -= 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX--;
                            startY--;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 1 < finalGrid.getRow() && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX++;
                            startY--;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }
            }
            if (startX % 2 == 1) {
                if (startX - 2 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX - 2, startY) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 2, startY);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX -= 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 2 < finalGrid.getRow() && flag == false) {
                    if (finalGrid.getGridElement(startX + 2, startY) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 2, startY);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX += 2;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 1 < finalGrid.getRow() && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX++;
                            startY--;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX + 1 < finalGrid.getRow() && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX + 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX + 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX++;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY + 1 < finalGrid.getColumn() && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY + 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY + 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX--;
                            startY++;
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }

                if (startX - 1 >= 0 && startY - 1 >= 0 && flag == false) {
                    if (finalGrid.getGridElement(startX - 1, startY - 1) instanceof Edge) {
                        Edge thisEdge = (Edge) finalGrid.getGridElement(startX - 1, startY - 1);
                        if (thisEdge.getEdgeState() == EdgeState.PRESENT) {
                            list.add(thisEdge);
                            flag = true;
                            startX--;
                            startY--;
                            //list.add((Edge)finalGrid.getGridElement(startX, startY - 2));
                            thisEdge.setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }
            }

            if (q == 2) {
                Edge finalEdge = (Edge) finalGrid.getGridElement(stopX, stopY);
                finalEdge.setEdgeState(EdgeState.PRESENT.toString());
            }

            if (flag == false) {
                break;
            }
        }
        if (flag == false) {
            list.clear();
            return list;
        } else {
            return list;
        }
    }

    /**
     * Check grid for come loops or for loop and something else
     *
     * @return \result which show grid is correct or not 
     * @throws NullPointerException if grid == null
     */ 
    public boolean checkGridForLoops() throws NullPointerException {
        boolean mySpecialFlag;
        ArrayList<Edge> list = new ArrayList<>();
        for (int i = 0; i < grid.getRow(); i++) {
            if (!list.isEmpty()) {
                break;
            }
            for (int j = 0; j < grid.getColumn(); j++) {
                if (!list.isEmpty()) {
                    break;
                }
                if (grid.getGridElement(i, j) instanceof Edge) {
                    Edge newEdge = (Edge) grid.getGridElement(i, j);
                    if (newEdge.getEdgeState() == EdgeState.PRESENT) {
                        boolean flag = findViolation5(newEdge);
                        if (flag == true) {
                            list = findViolation5forAllGridList(newEdge);
                        }
                    }
                }
            }
        }

        if (list.isEmpty()) {
            return true;
        } else {
            mySpecialFlag = true;
            boolean flafFind;
            for (int i = 0; i < grid.getRow(); i++) {
                if (mySpecialFlag == false) {
                    break;
                }
                for (int j = 0; j < grid.getColumn(); j++) {
                    if (mySpecialFlag == false) {
                        break;
                    }
                    if (grid.getGridElement(i, j) instanceof Edge) {
                        flafFind = false;
                        Edge newEdge = (Edge) grid.getGridElement(i, j);
                        if (newEdge.getEdgeState() != EdgeState.PRESENT) {
                            flafFind = true;
                        } else {
                            for (Edge my : list) {
                                if (newEdge.getX() == my.getX() && newEdge.getY() == my.getY()) {
                                    flafFind = true;
                                }
                            }
                            if (flafFind == false) {
                                mySpecialFlag = false;
                            }
                        }
                    }
                }
            }
            return mySpecialFlag;
        }
    }
}