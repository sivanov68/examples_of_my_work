package lpa.solvers.command;

import java.util.ArrayList;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class holdes some ChangeCommand for REDO/UNDO
 */
public class CompoundCommand {
    /*
     * list is a list which holdes some ChangeCommand for REDO/UNDO
     */
    private ArrayList<ChangeCommand> list = new ArrayList<>();

    /**
     * Constructor of CompoundCommand
     */
    public CompoundCommand() {
        list = new ArrayList<>();
    }

    /**
     * Add command to list
     *
     * @param command is a new command
     * @pre {@code command != null}
     */
    public void addNewCommand(ChangeCommand command) {
        list.add(command);
    }

    public ArrayList<ChangeCommand> getList() {
        return list;
    }
}