package lpa.solvers.command;

import lpa.model.Edge;
import lpa.model.EdgeState;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class using for UNDO/REDO and extends Command class
 */
public class ChangeCommand extends Command {

    /*
     * newEdgeState is a previous state of the receiver, for redo()
     */
    private EdgeState newEdgeState;
    /*
     * oldEdgeState is a previous state of the receiver, for undo()
     */
    private EdgeState oldEdgeState;

    /**
     * Constructor of ChangeCommand
     *
     * @pre {@code receiver != null && newEdgeState != null}
     */
    public ChangeCommand(final Edge receiver, final EdgeState newEdgeState) {
        super(receiver);
        this.newEdgeState = newEdgeState;
        this.oldEdgeState = receiver.getEdgeState();
    }

    @Override
    public void execute() {
        super.execute();
    }

    /**
     * Change oldEdgeState to newEdgeState;
     */
    @Override
    public void undo() {
        super.undo();
        receiver.setEdgeState(oldEdgeState.toString());
    }

    /**
     * Change newEdgeState to oldEdgeState;
     */
    @Override
    public void redo() {
        super.redo();
        receiver.setEdgeState(newEdgeState.toString());
    }
}
