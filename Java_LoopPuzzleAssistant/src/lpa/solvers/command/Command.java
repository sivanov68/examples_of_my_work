package lpa.solvers.command;

import lpa.model.Edge;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class using for UNDO/REDO
 */
public class Command {

    /*
     * receiver is a receiving Edge
     */
    protected final Edge receiver;
    /*
     * executed is an execution state
     */
    private boolean executed;

    /**
     * Constructs a command for a given receiver.
     *
     * @pre {@code receiver != null}
     */
    public Command(Edge receiver) throws NullPointerException {
        if (receiver == null) {
            throw new NullPointerException("Command(Counter).pre violated: "
                    + "receiver == null");
        }
        this.receiver = receiver;
        this.executed = false;
    }

    /**
     * Executes the command. A concrete command will override this method.
     *
     * @throws IllegalStateException if {@code executed}
     * @pre {@code ! executed && }
     * precondition of the command holds in the receiver
     * @post {@code executed}
     */
    public void execute() throws IllegalStateException {
        if (executed) {
            throw new IllegalStateException("Command.execute().pre violated: "
                    + "command was already executed");
        }
        executed = true;
    }

    /**
     * Show an execution state
     *
     * @return executed
     */
    public boolean isExecute() {
        if (executed == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Undoes the command. A concrete command will override this method.
     *
     * @pre {@code executed && }
     * precondition of the undo holds in the receiver
     * @post {@code ! executed}
     */
    public void undo() throws IllegalStateException {
        if (!executed) {
            throw new IllegalStateException("Command.undo().pre violated: "
                    + "command was not yet executed");
        }
        executed = false;
    }

    /**
     * Redoes the command. A concrete command will override this method.
     *
     * @pre {@code executed && }
     * precondition of the redo holds in the receiver
     * @post {@code ! executed}
     */
    public void redo() throws IllegalStateException {
        if (!executed) {
            throw new IllegalStateException("Command.redo().pre violated: "
                    + "command was not yet executed");
        }
        executed = false;
    }

    /**
     * Show an execution state
     *
     * @return executed
     */
    public boolean getExecuted() {
        return executed;
    }
}