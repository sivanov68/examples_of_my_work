package lpa.solvers;

import java.util.ArrayList;
import lpa.model.Cell;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.Grid;
import lpa.model.Histrogram;
import lpa.model.Vertex;
import lpa.solvers.command.ChangeCommand;
import lpa.solvers.command.CompoundCommand;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class of strategies depending on the vertex
 */
public class StrategyVertex implements StrategyInterface {

    /*
     * grid is grid, which be use for do strategies
     */
    public Grid grid;

    /**
     * Constructor of StrategyVertex
     *
     * @param grid is a input grid
     */
    public StrategyVertex(Grid grid) {
        this.grid = grid;
    }

    /**
     * If a vertex has two yes-edges, then its other edges can be marked as
     * no-edges. Just in time
     *
     * @param edge is an input edge
     * @return CompoundCommand for UNDO/REDO it than
     * @pre {@code edge != null}
     * @throws NullPointerException if grid == null
     */
    public CompoundCommand applyStrategyVertex1(Edge edge) throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        Vertex newVertex1, newVertex2;
        if (edge.getX() % 2 == 0) {
            newVertex1 = new Vertex(edge.getX(), edge.getY() + 1);

            if (newVertex1.getX() + 1 < grid.getRow()) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX() + 1, newVertex1.getY()));
            }
            if (newVertex1.getX() - 1 >= 0) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX() - 1, newVertex1.getY()));
            }
            if (newVertex1.getY() + 1 < grid.getColumn()) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() + 1));
            }
            if (newVertex1.getY() - 1 >= 0) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() - 1));
            }
            int countForVertex;
            Histrogram histrogram = newVertex1.elementGroup.kolvoEdges();
            countForVertex = histrogram.get(EdgeState.PRESENT);
            if (countForVertex == 2) {
                if (newVertex1.getX() + 1 < grid.getRow()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX() + 1, newVertex1.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex1.getX() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX() - 1, newVertex1.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex1.getY() + 1 < grid.getColumn()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() + 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex1.getY() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() - 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }


            newVertex2 = new Vertex(edge.getX(), edge.getY() - 1);

            if (newVertex2.getX() + 1 < grid.getRow()) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX() + 1, newVertex2.getY()));
            }
            if (newVertex2.getX() - 1 >= 0) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX() - 1, newVertex2.getY()));
            }
            if (newVertex2.getY() + 1 < grid.getColumn()) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() + 1));
            }
            if (newVertex2.getY() - 1 >= 0) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() - 1));
            }
            histrogram = newVertex2.elementGroup.kolvoEdges();
            countForVertex = histrogram.get(EdgeState.PRESENT);
            if (countForVertex == 2) {
                if (newVertex2.getX() + 1 < grid.getRow()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX() + 1, newVertex2.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex2.getX() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX() - 1, newVertex2.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex2.getY() + 1 < grid.getColumn()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() + 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex2.getY() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() - 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }
        }


        if (edge.getX() % 2 == 1) {
            newVertex1 = new Vertex(edge.getX() + 1, edge.getY());

            if (newVertex1.getX() + 1 < grid.getRow()) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX() + 1, newVertex1.getY()));
            }
            if (newVertex1.getX() - 1 >= 0) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX() - 1, newVertex1.getY()));
            }
            if (newVertex1.getY() + 1 < grid.getColumn()) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() + 1));
            }
            if (newVertex1.getY() - 1 >= 0) {
                newVertex1.addEdge((Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() - 1));
            }
            int countForVertex;
            Histrogram histrogram = newVertex1.elementGroup.kolvoEdges();
            countForVertex = histrogram.get(EdgeState.PRESENT);
            if (countForVertex == 2) {
                if (newVertex1.getX() + 1 < grid.getRow()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX() + 1, newVertex1.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex1.getX() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX() - 1, newVertex1.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex1.getY() + 1 < grid.getColumn()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() + 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex1.getY() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex1.getX(), newVertex1.getY() - 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }


            newVertex2 = new Vertex(edge.getX() - 1, edge.getY());

            if (newVertex2.getX() + 1 < grid.getRow()) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX() + 1, newVertex2.getY()));
            }
            if (newVertex2.getX() - 1 >= 0) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX() - 1, newVertex2.getY()));
            }
            if (newVertex2.getY() + 1 < grid.getColumn()) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() + 1));
            }
            if (newVertex2.getY() - 1 >= 0) {
                newVertex2.addEdge((Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() - 1));
            }
            histrogram = newVertex2.elementGroup.kolvoEdges();
            countForVertex = histrogram.get(EdgeState.PRESENT);
            if (countForVertex == 2) {
                if (newVertex2.getX() + 1 < grid.getRow()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX() + 1, newVertex2.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex2.getX() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX() - 1, newVertex2.getY());
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex2.getY() + 1 < grid.getColumn()) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() + 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
                if (newVertex2.getY() - 1 >= 0) {
                    Edge ed = (Edge) grid.getGridElement(newVertex2.getX(), newVertex2.getY() - 1);
                    if (ed.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(ed, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand.addNewCommand(command);

                        ed.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }
        }
        return compoundCommand;
    }

    /**
     * If corner cell contains digit 3, then its two outer edges can be marked
     * as yes-edges.
     *
     * @return CompoundCommand for UNDO/REDO it than
     * @throws NullPointerException if grid == null
     */
    public CompoundCommand applyStrategyVertex2() throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        Cell cell = (Cell) grid.getGridElement(1, 1);
        if (cell.getCellState() == 3) {
            Edge edge = (Edge) grid.getGridElement(0, 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.PRESENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.PRESENT.toString());
            edge = (Edge) grid.getGridElement(1, 0);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.PRESENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.PRESENT.toString());
        }

        cell = (Cell) grid.getGridElement(1, grid.getColumn() - 2);
        if (cell.getCellState() == 3) {
            Edge edge = (Edge) grid.getGridElement(1, grid.getColumn() - 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.PRESENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.PRESENT.toString());
            edge = (Edge) grid.getGridElement(0, grid.getColumn() - 2);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.PRESENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.PRESENT.toString());
        }

        cell = (Cell) grid.getGridElement(grid.getRow() - 2, 1);
        if (cell.getCellState() == 3) {
            Edge edge = (Edge) grid.getGridElement(grid.getRow() - 1, 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.PRESENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.PRESENT.toString());
            edge = (Edge) grid.getGridElement(grid.getRow() - 2, 0);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.PRESENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.PRESENT.toString());
        }

        cell = (Cell) grid.getGridElement(grid.getRow() - 2, grid.getColumn() - 2);
        if (cell.getCellState() == 3) {
            Edge edge = (Edge) grid.getGridElement(grid.getRow() - 2, grid.getColumn() - 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.PRESENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.PRESENT.toString());
            edge = (Edge) grid.getGridElement(grid.getRow() - 1, grid.getColumn() - 2);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.PRESENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.PRESENT.toString());
        }
        return compoundCommand;
    }

    /**
     * If corner cell contains digit 1, then its two outer edges can be marked
     * as no-edges.
     *
     * @return CompoundCommand for UNDO/REDO it than
     * @throws NullPointerException if grid == null
     */
    public CompoundCommand applyStrategyVertex3() throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        Cell cell = (Cell) grid.getGridElement(1, 1);
        if (cell.getCellState() == 1) {
            Edge edge = (Edge) grid.getGridElement(0, 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.ABSENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.ABSENT.toString());
            edge = (Edge) grid.getGridElement(1, 0);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.ABSENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.ABSENT.toString());
        }

        cell = (Cell) grid.getGridElement(1, grid.getColumn() - 2);
        if (cell.getCellState() == 1) {
            Edge edge = (Edge) grid.getGridElement(1, grid.getColumn() - 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.ABSENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.ABSENT.toString());
            edge = (Edge) grid.getGridElement(0, grid.getColumn() - 2);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.ABSENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.ABSENT.toString());
        }

        cell = (Cell) grid.getGridElement(grid.getRow() - 2, 1);
        if (cell.getCellState() == 1) {
            Edge edge = (Edge) grid.getGridElement(grid.getRow() - 1, 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.ABSENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.ABSENT.toString());
            edge = (Edge) grid.getGridElement(grid.getRow() - 2, 0);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.ABSENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.ABSENT.toString());
        }

        cell = (Cell) grid.getGridElement(grid.getRow() - 2, grid.getColumn() - 2);
        if (cell.getCellState() == 1) {
            Edge edge = (Edge) grid.getGridElement(grid.getRow() - 2, grid.getColumn() - 1);

            final ChangeCommand command1 = new ChangeCommand(edge, EdgeState.ABSENT);
            command1.execute();
            compoundCommand.addNewCommand(command1);

            edge.setEdgeState(EdgeState.ABSENT.toString());
            edge = (Edge) grid.getGridElement(grid.getRow() - 1, grid.getColumn() - 2);

            final ChangeCommand command2 = new ChangeCommand(edge, EdgeState.ABSENT);
            command2.execute();
            compoundCommand.addNewCommand(command2);

            edge.setEdgeState(EdgeState.ABSENT.toString());
        }
        return compoundCommand;
    }

    /**
     * If all but one edge at a vertex are no-edges, then the remaining edge can
     * be marked as no-edge as well.
     *
     * @return CompoundCommand for UNDO/REDO it than
     * @throws NullPointerException if grid == null
     */
    public CompoundCommand applyStrategyVertex4() {
        if (grid == null) {
            throw new IllegalArgumentException("Grid is null");
        }
        CompoundCommand compoundCommand = new CompoundCommand();
        Vertex newVertex;
        ArrayList<Edge> list;
        for (int q = 0; q < grid.getRow(); q += 1) {
            for (int w = 0; w < grid.getColumn(); w += 1) {
                if (grid.getGridElement(q, w) instanceof Vertex) {
                    newVertex = new Vertex(q, w);
                    list = new ArrayList<>();
                    if (newVertex.getX() + 1 < grid.getRow()) {
                        list.add((Edge) grid.getGridElement(newVertex.getX() + 1, newVertex.getY()));
                        newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX() + 1, newVertex.getY()));
                    }
                    if (newVertex.getX() - 1 >= 0) {
                        list.add((Edge) grid.getGridElement(newVertex.getX() - 1, newVertex.getY()));
                        newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX() - 1, newVertex.getY()));
                    }
                    if (newVertex.getY() + 1 < grid.getColumn()) {
                        list.add((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() + 1));
                        newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() + 1));
                    }
                    if (newVertex.getY() - 1 >= 0) {
                        list.add((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() - 1));
                        newVertex.addEdge((Edge) grid.getGridElement(newVertex.getX(), newVertex.getY() - 1));
                    }
                    Histrogram histrogram1 = newVertex.elementGroup.kolvoEdges();
                    if (histrogram1.get(EdgeState.ABSENT) == 3 && list.size() == 4) {
                        for (int i = 0; i < list.size(); i++) {

                            final ChangeCommand command2 = new ChangeCommand(list.get(i), EdgeState.ABSENT);
                            command2.execute();
                            compoundCommand.addNewCommand(command2);

                            list.get(i).setEdgeState(EdgeState.ABSENT.toString());
                        }
                    }
                }
            }
        }
        return compoundCommand;
    }

    /**
     * If a vertex has two yes-edges, then its other edges can be marked as
     * no-edges.
     *
     * @return CompoundCommand for UNDO/REDO it than
     * @throws NullPointerException if grid == null
     */
    @SuppressWarnings("empty-statement")
    public CompoundCommand applyStrategyVertex5() throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        ArrayList<Edge> list;;
        for (int q = 0; q < grid.getRow(); q += 1) {
            for (int w = 0; w < grid.getColumn(); w += 1) {
                if (grid.getGridElement(q, w) instanceof Vertex) {
                    list = new ArrayList();
                    Vertex newVertex = new Vertex(q, w);
                    Vertex oldVertex = (Vertex) grid.getGridElement(q, w);
                    if (oldVertex.getX() + 1 < grid.getRow()) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX() + 1, oldVertex.getY()));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX() + 1, oldVertex.getY()));
                    }
                    if (oldVertex.getX() - 1 >= 0) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX() - 1, oldVertex.getY()));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX() - 1, oldVertex.getY()));
                    }
                    if (oldVertex.getY() + 1 < grid.getColumn()) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() + 1));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() + 1));
                    }
                    if (oldVertex.getY() - 1 >= 0) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() - 1));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() - 1));
                    }
                    int countForVertex;
                    Histrogram histrogram = newVertex.elementGroup.kolvoEdges();
                    countForVertex = histrogram.get(EdgeState.PRESENT);
                    if (countForVertex == 2) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getEdgeState() != EdgeState.PRESENT) {

                                final ChangeCommand command2 = new ChangeCommand(list.get(i), EdgeState.ABSENT);
                                command2.execute();
                                compoundCommand.addNewCommand(command2);

                                list.get(i).setEdgeState(EdgeState.ABSENT.toString());
                            }
                        }
                    }
                }
            }
        }
        return compoundCommand;
    }

    /**
     * If all but one edge at a vertex are no-edges, then the remaining edge can
     * be marked as no-edge as well.
     *
     * @return CompoundCommand for UNDO/REDO it than
     * @throws NullPointerException if grid == null
     */
    public CompoundCommand applyStrategyVertex6() throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        ArrayList<Edge> list = new ArrayList();
        for (int q = 0; q < grid.getRow(); q += 1) {
            for (int w = 0; w < grid.getColumn(); w += 1) {
                if (grid.getGridElement(q, w) instanceof Vertex) {
                    list.clear();
                    Vertex newVertex = new Vertex(q, w);
                    Vertex oldVertex = (Vertex) grid.getGridElement(q, w);
                    if (oldVertex.getX() + 1 < grid.getRow()) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX() + 1, oldVertex.getY()));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX() + 1, oldVertex.getY()));
                    }
                    if (oldVertex.getX() - 1 >= 0) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX() - 1, oldVertex.getY()));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX() - 1, oldVertex.getY()));
                    }
                    if (oldVertex.getY() + 1 < grid.getColumn()) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() + 1));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() + 1));
                    }
                    if (oldVertex.getY() - 1 >= 0) {
                        list.add((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() - 1));
                        newVertex.addEdge((Edge) grid.getGridElement(oldVertex.getX(), oldVertex.getY() - 1));
                    }
                    int countForVertex;
                    Histrogram histrogram = newVertex.elementGroup.kolvoEdges();
                    countForVertex = histrogram.get(EdgeState.ABSENT);
                    if (countForVertex == 3) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getEdgeState() != EdgeState.ABSENT) {

                                final ChangeCommand command2 = new ChangeCommand(list.get(i), EdgeState.ABSENT);
                                command2.execute();
                                compoundCommand.addNewCommand(command2);

                                list.get(i).setEdgeState(EdgeState.ABSENT.toString());
                            }
                        }
                    }
                }
            }
        }
        return compoundCommand;
    }

    /**
     * Apply strategies to all grid
     *
     * @return CompoundCommand for UNDO/REDO it than
     */
    @Override
    public CompoundCommand apply() throws IllegalArgumentException {
        CompoundCommand compoundCommand1 = new CompoundCommand();
        CompoundCommand compoundCommand2 = new CompoundCommand();
        CompoundCommand compoundCommand3 = new CompoundCommand();
        CompoundCommand compoundCommand4 = new CompoundCommand();
        CompoundCommand compoundCommand5 = new CompoundCommand();
        CompoundCommand returnCompountCommand = new CompoundCommand();

        compoundCommand1 = applyStrategyVertex2();
        compoundCommand2 = applyStrategyVertex3();
        compoundCommand3 = applyStrategyVertex4();
        compoundCommand4 = applyStrategyVertex5();
        compoundCommand5 = applyStrategyVertex6();

        if (!compoundCommand1.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand1.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand1.getList().get(i));
            }
        }

        if (!compoundCommand2.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand2.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand2.getList().get(i));
            }
        }

        if (!compoundCommand3.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand3.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand3.getList().get(i));
            }
        }

        if (!compoundCommand4.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand4.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand4.getList().get(i));
            }
        }

        if (!compoundCommand5.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand5.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand5.getList().get(i));
            }
        }

        return returnCompountCommand;
    }
}