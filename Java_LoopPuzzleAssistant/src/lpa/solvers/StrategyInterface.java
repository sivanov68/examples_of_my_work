package lpa.solvers;

import lpa.solvers.command.CompoundCommand;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */

public interface StrategyInterface {
    public CompoundCommand apply();    
}
