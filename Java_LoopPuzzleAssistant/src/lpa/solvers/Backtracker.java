package lpa.solvers;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.Grid;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class that stores value of cell and surrounding edge
 */
public class Backtracker {
     /*
     * grid is grid, which be use for find solutions
     */
    Grid grid;
    /*
     * solutions is a list of solutions
     */
    List<String> solutions;

    /**
     * Constructor of Backtracker
     *
     * @param g is a input grid
     * @pre {@code g != null}
     */
    public Backtracker(Grid g) {
        String s = g.toString();
        Scanner scan = new Scanner(s);
        grid = new Grid(scan);
        for (int x = 0; x < grid.getRow(); x++) {
            for (int y = 0; y < grid.getColumn(); y++) {
                if (grid.getGridElement(x, y) instanceof Edge) {
                    Edge edge = (Edge) grid.getGridElement(x, y);
                    edge.setEdgeState(EdgeState.UNDERTERMINED.toString());
                }
            }
        }

        StrategyCell strategyCell;
        strategyCell = new StrategyCell(grid);
        strategyCell.apply();
        StrategyVertex strategyVertex;
        strategyVertex = new StrategyVertex(grid);
        strategyVertex.apply();
    }

    /**
     * Find all solutions for grid
     *
     * @return list of solutions
     * @post {@code (\forall i; \result.has(i); g[i] is a solution for grid)}
     */
    public List<String> solveAll() {
        solutions = new ArrayList<>();
        StrategyCell strategyCell;
        strategyCell = new StrategyCell(grid);
        strategyCell.apply();
        StrategyVertex strategyVertex;
        strategyVertex = new StrategyVertex(grid);
        strategyVertex.apply();
        backtracking();
        return solutions;
    }

    /**
     * Backtracking fof find solutions
     */
    private void backtracking() {
        Grid deleteGrid;
        for (int row = 0; row < grid.getRow(); row++) {
            int start = (row % 2 == 0) ? 1 : 0;
            for (int col = start; col < grid.getColumn(); col += 2) {
                Edge e = (Edge) grid.getGridElement(row, col);
                if (e.getEdgeState() == EdgeState.UNDERTERMINED) {
                    e.setEdgeState(EdgeState.ABSENT.toString());
                    String s = grid.toString();
                    Scanner scan = new Scanner(s);
                    deleteGrid = new Grid(scan);
                    StrategyCell strategyCell;
                    strategyCell = new StrategyCell(grid);
                    strategyCell.apply();
                    StrategyVertex strategyVertex;
                    strategyVertex = new StrategyVertex(grid);
                    strategyVertex.apply();
                    Violation myViolation = new Violation(grid);
                    if (grid.Check() == true && myViolation.checkGridForLoops() == true) {
                        backtracking();
                    }
                    s = deleteGrid.toString();
                    scan = new Scanner(s);
                    grid = new Grid(scan);

                    e = (Edge) grid.getGridElement(row, col);
                    e.setEdgeState(EdgeState.PRESENT.toString());
                    strategyCell = new StrategyCell(grid);
                    strategyCell.apply();
                    strategyVertex = new StrategyVertex(grid);
                    strategyVertex.apply();
                    myViolation = new Violation(grid);
                    if (grid.Check() == true && myViolation.checkGridForLoops() == true) {
                        backtracking();
                    }

                    s = deleteGrid.toString();
                    scan = new Scanner(s);
                    grid = new Grid(scan);

                    e = (Edge) grid.getGridElement(row, col);
                    e.setEdgeState(EdgeState.UNDERTERMINED.toString());
                    return;
                }
            }
        }

        Violation myViolation = new Violation(grid);
        if (grid.Check() == true && myViolation.checkGridForLoops() == true) {
            solutions.add(grid.toString());
        }
    }

    /**
     * Find one solutions for grid
     *
     * @return list of solutions
     * @post {@code List<String>[i] is a solution for grid if it exists}
     *
     */
    public List<String> solveOne() {
        solutions = new ArrayList<>();
        StrategyCell strategyCell;
        strategyCell = new StrategyCell(grid);
        strategyCell.apply();
        StrategyVertex strategyVertex;
        strategyVertex = new StrategyVertex(grid);
        strategyVertex.apply();
        backtrackingOne();
        return solutions;
    }

    /**
     * Backtracking fof find one solution
     */
    private void backtrackingOne() {
        Grid deleteGrid;
        if (solutions.size() == 1) {
            return;
        }
        for (int row = 0; row < grid.getRow(); row++) {
            int start = (row % 2 == 0) ? 1 : 0;
            for (int col = start; col < grid.getColumn(); col += 2) {
                Edge e = (Edge) grid.getGridElement(row, col);
                if (e.getEdgeState() == EdgeState.UNDERTERMINED) {
                    e.setEdgeState(EdgeState.ABSENT.toString());
                    String s = grid.toString();
                    Scanner scan = new Scanner(s);
                    deleteGrid = new Grid(scan);
                    StrategyCell strategyCell;
                    strategyCell = new StrategyCell(grid);
                    strategyCell.apply();
                    StrategyVertex strategyVertex;
                    strategyVertex = new StrategyVertex(grid);
                    strategyVertex.apply();
                    Violation myViolation = new Violation(grid);
                    if (grid.Check() == true && myViolation.checkGridForLoops() == true) {
                        backtrackingOne();
                    }
                    s = deleteGrid.toString();
                    scan = new Scanner(s);
                    grid = new Grid(scan);

                    e = (Edge) grid.getGridElement(row, col);
                    e.setEdgeState(EdgeState.PRESENT.toString());
                    strategyCell = new StrategyCell(grid);
                    strategyCell.apply();
                    strategyVertex = new StrategyVertex(grid);
                    strategyVertex.apply();
                    myViolation = new Violation(grid);
                    if (grid.Check() == true && myViolation.checkGridForLoops() == true) {
                        backtrackingOne();
                    }

                    s = deleteGrid.toString();
                    scan = new Scanner(s);
                    grid = new Grid(scan);

                    e = (Edge) grid.getGridElement(row, col);
                    e.setEdgeState(EdgeState.UNDERTERMINED.toString());
                    return;
                }
            }
        }

        Violation myViolation = new Violation(grid);
        if (grid.Check() == true && myViolation.checkGridForLoops() == true) {
            solutions.add(grid.toString());
        }
    }
}