package lpa.solvers;

import java.util.ArrayList;
import lpa.model.Cell;
import lpa.model.Edge;
import lpa.model.EdgeState;
import lpa.model.Grid;
import lpa.model.Histrogram;
import lpa.solvers.command.ChangeCommand;
import lpa.solvers.command.CompoundCommand;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class of strategies depending on the value of cell
 */
public class StrategyCell implements StrategyInterface {

    /*
     * grid is grid, which be use for do strategies
     */
    public Grid grid;

    /**
     * Constructor of StrategyCell
     *
     * @param grid is a input grid
     */
    public StrategyCell(Grid grid) {
        this.grid = grid;
    }

    /**
     * If a cell containing digit d is surrounded by d yes-edges, then all its
     * other edges can be marked as no-edges. Just in time
     *
     * @param edge is an input edge
     * @return CompoundCommand for UNDO/REDO it than
     * @pre {@code edge != null}
     * @throws NullPointerException if grid == null
     */
    public CompoundCommand applyStrategyCell1(Edge edge) throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        Cell myCell1;
        if (edge.getX() % 2 == 0) {
            if (edge.getX() - 1 >= 0) {
                myCell1 = new Cell(edge.getX() - 1, edge.getY());
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1));
                Histrogram histrogram1 = myCell1.elementGroup.kolvoEdges();
                int tekcount1 = histrogram1.get(EdgeState.PRESENT);
                Cell oldCell = (Cell) grid.getGridElement(edge.getX() - 1, edge.getY());
                if (tekcount1 == oldCell.getCellState()) {
                    Edge colorEdge;
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }

            if (edge.getX() + 1 < grid.getRow()) {
                myCell1 = new Cell(edge.getX() + 1, edge.getY());
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1));
                Histrogram histrogram1 = myCell1.elementGroup.kolvoEdges();
                int tekcount1 = histrogram1.get(EdgeState.PRESENT);
                Cell oldCell = (Cell) grid.getGridElement(edge.getX() + 1, edge.getY());
                if (tekcount1 == oldCell.getCellState()) {
                    Edge colorEdge;
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }
        }


        if (edge.getX() % 2 == 1) {
            if (edge.getY() - 1 >= 0) {
                myCell1 = new Cell(edge.getX(), edge.getY() - 1);
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1));
                Histrogram histrogram1 = myCell1.elementGroup.kolvoEdges();
                int tekcount1 = histrogram1.get(EdgeState.PRESENT);
                Cell oldCell = (Cell) grid.getGridElement(edge.getX(), edge.getY() - 1);
                if (tekcount1 == oldCell.getCellState()) {
                    Edge colorEdge;
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }

            if (edge.getY() + 1 < grid.getColumn()) {
                myCell1 = new Cell(edge.getX(), edge.getY() + 1);
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY()));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1));
                myCell1.addEdge((Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1));
                Histrogram histrogram1 = myCell1.elementGroup.kolvoEdges();
                int tekcount1 = histrogram1.get(EdgeState.PRESENT);
                Cell oldCell = (Cell) grid.getGridElement(edge.getX(), edge.getY() + 1);
                if (tekcount1 == oldCell.getCellState()) {
                    Edge colorEdge;
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() - 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX() + 1, myCell1.getY());
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() - 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                    colorEdge = (Edge) grid.getGridElement(myCell1.getX(), myCell1.getY() + 1);
                    if (colorEdge.getEdgeState() != EdgeState.PRESENT) {

                        final ChangeCommand command = new ChangeCommand(colorEdge, EdgeState.ABSENT);
                        command.execute();
                        compoundCommand = new CompoundCommand();
                        compoundCommand.addNewCommand(command);

                        colorEdge.setEdgeState(EdgeState.ABSENT.toString());
                    }
                }
            }
        }
        return compoundCommand;
    }

    /**
     * If a cell containing digit d is surrounded by 4 d no-edges, then the
     * remaining edges can be marked as yes-edges.
     *
     * @return CompoundCommand for UNDO/REDO it than
     * @throws NullPointerException if grid == null
     */
    @SuppressWarnings("empty-statement")
    public CompoundCommand applyStrategyCell2() throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        Cell myCell;
        ArrayList<Edge> list = new ArrayList();;
        for (int q = 0; q < grid.getRow(); q += 1) {
            for (int w = 0; w < grid.getColumn(); w += 1) {
                if (grid.getGridElement(q, w) instanceof Cell) {
                    Cell oldCell = (Cell) grid.getGridElement(q, w);
                    myCell = new Cell(q, w);
                    list.clear();
                    list.add((Edge) grid.getGridElement(q + 1, w));
                    myCell.addEdge((Edge) grid.getGridElement(q + 1, w));
                    list.add((Edge) grid.getGridElement(q - 1, w));
                    myCell.addEdge((Edge) grid.getGridElement(q - 1, w));
                    list.add((Edge) grid.getGridElement(q, w - 1));
                    myCell.addEdge((Edge) grid.getGridElement(q, w - 1));
                    list.add((Edge) grid.getGridElement(q, w + 1));
                    myCell.addEdge((Edge) grid.getGridElement(q, w + 1));
                    Histrogram histrogram1 = myCell.elementGroup.kolvoEdges();
                    int countPresent = histrogram1.get(EdgeState.PRESENT);
                    int count = oldCell.getCellState();
                    if (count == 2 && countPresent == 2) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getEdgeState() != EdgeState.PRESENT) {

                                final ChangeCommand command2 = new ChangeCommand(list.get(i), EdgeState.ABSENT);
                                command2.execute();
                                compoundCommand.addNewCommand(command2);

                                list.get(i).setEdgeState(EdgeState.ABSENT.toString());
                            }
                        }
                    }
                }
            }
        }
        return compoundCommand;
    }

    /**
     * If a cell containing digit d is surrounded by d yes-edges, then all its
     * other edges can be marked as no-edges.
     *
     * @return CompoundCommand for UNDO/REDO it than
     * @throws NullPointerException if grid == null
     */
    public CompoundCommand applyStrategyCell3() throws NullPointerException {
        CompoundCommand compoundCommand = new CompoundCommand();
        Cell myCell;
        ArrayList<Edge> list;
        for (int q = 0; q < grid.getRow(); q += 1) {
            for (int w = 0; w < grid.getColumn(); w += 1) {
                if (grid.getGridElement(q, w) instanceof Cell) {
                    myCell = new Cell(q, w);
                    list = new ArrayList();
                    Cell oldCell = (Cell) grid.getGridElement(q, w);
                    list.add((Edge) grid.getGridElement(q + 1, w));
                    myCell.addEdge((Edge) grid.getGridElement(q + 1, w));
                    list.add((Edge) grid.getGridElement(q - 1, w));
                    myCell.addEdge((Edge) grid.getGridElement(q - 1, w));
                    list.add((Edge) grid.getGridElement(q, w - 1));
                    myCell.addEdge((Edge) grid.getGridElement(q, w - 1));
                    list.add((Edge) grid.getGridElement(q, w + 1));
                    myCell.addEdge((Edge) grid.getGridElement(q, w + 1));
                    Histrogram histrogram1 = myCell.elementGroup.kolvoEdges();
                    int tekcount1 = histrogram1.get(EdgeState.PRESENT);
                    int count = oldCell.getCellState();
                    if (count == tekcount1 && count != 4) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getEdgeState() != EdgeState.PRESENT) {

                                final ChangeCommand command2 = new ChangeCommand(list.get(i), EdgeState.ABSENT);
                                command2.execute();
                                compoundCommand.addNewCommand(command2);

                                list.get(i).setEdgeState(EdgeState.ABSENT.toString());
                            }
                        }
                    }
                }
            }
        }
        return compoundCommand;
    }

    /**
     * Apply strategies to all grid
     *
     * @return CompoundCommand for UNDO/REDO it than
     */
    @Override
    public CompoundCommand apply() throws IllegalArgumentException {
        CompoundCommand compoundCommand1 = new CompoundCommand();
        CompoundCommand compoundCommand2 = new CompoundCommand();
        CompoundCommand returnCompountCommand = new CompoundCommand();

        compoundCommand1 = applyStrategyCell2();
        compoundCommand2 = applyStrategyCell3();

        if (!compoundCommand1.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand1.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand1.getList().get(i));
            }
        }

        if (!compoundCommand2.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand2.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand2.getList().get(i));
            }
        }
        return returnCompountCommand;
    }
}