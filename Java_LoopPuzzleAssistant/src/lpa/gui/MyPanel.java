package lpa.gui;

import java.awt.*;
import java.awt.geom.*;
import java.util.Stack;
import lpa.model.*;
import lpa.solvers.Backtracker;
import lpa.solvers.StrategyCell;
import lpa.solvers.StrategyVertex;
import lpa.solvers.Violation;
import lpa.solvers.command.ChangeCommand;
import lpa.solvers.command.CompoundCommand;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class for the grid display and event handling over the net
 */
public class MyPanel extends javax.swing.JPanel {

    /*
     * undoStack is a container of CompoundCommands
     */
    private static Stack<CompoundCommand> undoStack;
    /*
     * redoStack is a container of CompoundCommands
     */
    private static Stack<CompoundCommand> redoStack;
    public static boolean UNDO = true;
    public static boolean REDU = true;
    /*
     * flagCheck is a flag of checking grid
     */
    protected static boolean flagCheck = false;
    /*
     * flagForStrategies is a flag for apply all strategies
     */
    protected static boolean flagForStrategies = false;
    /*
     * flagForViolationA is a flag of ViolationA
     */
    protected static boolean flagForViolationA = false;
    /*
     * flagForViolationB is a flag of ViolationB
     */
    protected static boolean flagForViolationB = false;
    /*
     * flagForViolationC is a flag of ViolationC
     */
    protected static boolean flagForViolationC = false;
    /*
     * flagForViolationD is a flag of ViolationD
     */
    protected static boolean flagForViolationD = false;
    /*
     * flagForViolationE is a flag of ViolationE
     */
    protected static boolean flagForViolationE = false;
    /*
     * chandeCell is a flag of changing cells
     */
    protected static boolean changeCell = false;
    /*
     * editCell is a flag of changing cells
     */
    protected static boolean editCell = false;
    protected static int valueCell = 4;
    /*
     *  is a container of gridelements
     */
    static Grid panelGrid = null;

    /**
     * Creates new form MyPanel
     */
    public MyPanel() {
        initComponents();
        initPanel();
    }

    @Override
    @SuppressWarnings("empty-statement")
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D ris = (Graphics2D) g;
        int x = 20, y = 10;
        final int W = 43, H = 8, RAZ = 5;
        if (panelGrid != null) {
            int nRows = panelGrid.getRow();
            int nColumns = panelGrid.getColumn();
            for (int i = 0; i < panelGrid.getRow(); i++) {
                for (int j = 0; j < panelGrid.getColumn(); j++) {
                    if (panelGrid.getGridElement(i, j) instanceof lpa.model.Vertex) {
                        int count = 0;

                        if (i + 1 < panelGrid.getRow() && panelGrid.getGridElement(i, j) != null) {
                            Edge edge = (Edge) panelGrid.getGridElement(i + 1, j);
                            if (edge.getEdgeState() == EdgeState.PRESENT) {
                                count++;
                            }
                        }

                        if (j + 1 < panelGrid.getColumn() && panelGrid.getGridElement(i, j) != null) {
                            Edge edge = (Edge) panelGrid.getGridElement(i, j + 1);
                            if (edge.getEdgeState() == EdgeState.PRESENT) {
                                count++;
                            }
                        }

                        if (i - 1 >= 0 && panelGrid.getGridElement(i, j) != null) {
                            Edge edge = (Edge) panelGrid.getGridElement(i - 1, j);
                            if (edge.getEdgeState() == EdgeState.PRESENT) {
                                count++;
                            }
                        }

                        if (j - 1 >= 0 && panelGrid.getGridElement(i, j) != null) {
                            Edge edge = (Edge) panelGrid.getGridElement(i, j - 1);
                            if (edge.getEdgeState() == EdgeState.PRESENT) {
                                count++;
                            }
                        }
                        Ellipse2D circle = new Ellipse2D.Double(x, y, H, H);
                        if (count >= 2) {
                            ris.setColor(Color.BLACK);
                            ris.draw(circle);
                            ris.fill(circle);
                        } else {
                            ris.setColor(Color.LIGHT_GRAY);
                            ris.draw(circle);
                            ris.fill(circle);
                            ris.setColor(Color.BLACK);
                        }

                        if (flagForViolationC == true) {
                            Violation violation = new Violation(panelGrid);
                            boolean fl = violation.findViolation3((Vertex) panelGrid.getGridElement(i, j));
                            if (fl == false) {
                                ris.setColor(Color.RED);
                                ris.draw(circle);
                                ris.fill(circle);
                                ris.setColor(Color.BLACK);
                            }
                        }

                        if (flagForViolationD == true) {
                            Violation violation = new Violation(panelGrid);
                            boolean fl = violation.findViolation4((Vertex) panelGrid.getGridElement(i, j));
                            if (fl == false) {
                                ris.setColor(Color.RED);
                                ris.draw(circle);
                                ris.fill(circle);
                                ris.setColor(Color.BLACK);
                            }
                        }

                        x += H + RAZ;
                    }
                    if (panelGrid.getGridElement(i, j) instanceof lpa.model.Edge) {
                        if (" ".equals(panelGrid.getGridElement(i, j).ToString())) {
                            if (i % 2 == 1) {
                                Edge edge = (Edge) panelGrid.getGridElement(i, j);
                                if (edge.getEdgeState() == EdgeState.ABSENT) {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, H, W);
                                    ris.setColor(Color.WHITE);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                    Font a = new Font("Times New Roman", Font.ROMAN_BASELINE, 18);
                                    ris.setFont(a);
                                    ris.drawString("x", x, y+ W/2 + 5);
                                    ris.setColor(Color.BLACK);
                                }
                                x += H + RAZ;
                            } else {
                                Edge edge = (Edge) panelGrid.getGridElement(i, j);
                                if (edge.getEdgeState() == EdgeState.ABSENT) {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, W, H);
                                    ris.setColor(Color.WHITE);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                    Font a = new Font("Times New Roman", Font.ROMAN_BASELINE, 18);
                                    ris.setFont(a);
                                    ris.drawString("x", x + W/2 - 3, y + H/2 + 5);
                                    ris.setColor(Color.BLACK);
                                }
                                x += W + RAZ;
                            }
                        }
                        if (".".equals(panelGrid.getGridElement(i, j).ToString())) {
                            if (i % 2 == 1) {
                                Edge edge = (Edge) panelGrid.getGridElement(i, j);
                                if (edge.getEdgeState() == EdgeState.UNDERTERMINED) {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, H, W);
                                    ris.setColor(Color.LIGHT_GRAY);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                }
                                x += H + RAZ;
                            } else {
                                Edge edge = (Edge) panelGrid.getGridElement(i, j);
                                if (edge.getEdgeState() == EdgeState.UNDERTERMINED) {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, W, H);
                                    ris.setColor(Color.LIGHT_GRAY);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                }
                                x += W + RAZ;
                            }
                        }
                        if ("|".equals(panelGrid.getGridElement(i, j).ToString())) {
                            Edge edge = (Edge) panelGrid.getGridElement(i, j);
                            if (edge.getEdgeState() == EdgeState.PRESENT) {
                                if (flagCheck == true) {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, H, W);
                                    ris.setColor(Color.GREEN);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                } else {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, H, W);
                                    ris.setColor(Color.BLACK);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                }

                                if (flagForViolationE == true) {
                                    Violation violation = new Violation(panelGrid);

                                    boolean fl = violation.findViolation5(edge);
                                    if (fl == false) {
                                        Rectangle2D rect = new Rectangle2D.Double(x, y, H, W);
                                        ris.setColor(Color.RED);
                                        ris.draw(rect);
                                        ris.fill(rect);
                                        ris.setColor(Color.BLACK);
                                    } else {
                                        Rectangle2D rect = new Rectangle2D.Double(x, y, H, W);
                                        ris.setColor(Color.BLACK);
                                        ris.draw(rect);
                                        ris.fill(rect);
                                        ris.setColor(Color.BLACK);

                                    }
                                }
                            }
                            x += H + RAZ;
                        }
                        if ("-".equals(panelGrid.getGridElement(i, j).ToString())) {
                            Edge edge = (Edge) panelGrid.getGridElement(i, j);
                            if (edge.getEdgeState() == EdgeState.PRESENT) {
                                if (flagCheck == true) {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, W, H);
                                    ris.setColor(Color.GREEN);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                } else {
                                    Rectangle2D rect = new Rectangle2D.Double(x, y, W, H);
                                    ris.setColor(Color.BLACK);
                                    ris.draw(rect);
                                    ris.fill(rect);
                                    ris.setColor(Color.BLACK);
                                }

                                if (flagForViolationE == true) {
                                    Violation violation = new Violation(panelGrid);
                                    boolean fl = violation.findViolation5(edge);
                                    if (fl == false) {
                                        Rectangle2D rect = new Rectangle2D.Double(x, y, W, H);
                                        ris.setColor(Color.RED);
                                        ris.draw(rect);
                                        ris.fill(rect);
                                        ris.setColor(Color.BLACK);
                                    } else {
                                        Rectangle2D rect = new Rectangle2D.Double(x, y, W, H);
                                        ris.setColor(Color.BLACK);
                                        ris.draw(rect);
                                        ris.fill(rect);
                                        ris.setColor(Color.BLACK);

                                    }
                                }
                            }
                            x += W + RAZ;
                        }
                    }
                    if (panelGrid.getGridElement(i, j) instanceof lpa.model.Cell) {
                        if (!"4".equals(panelGrid.getGridElement(i, j).ToString())) {
                            Font a = new Font("Times New Roman", Font.BOLD, 30);
                            ris.setColor(Color.BLUE);
                            if (flagForViolationA == true) {
                                Violation violation = new Violation(panelGrid);
                                boolean fl = violation.findViolation1((Cell) panelGrid.getGridElement(i, j));
                                if (fl == false) {
                                    ris.setColor(Color.RED);
                                }
                            }
                            if (flagForViolationB == true) {
                                Violation violation = new Violation(panelGrid);
                                boolean fl = violation.findViolation2((Cell) panelGrid.getGridElement(i, j));
                                if (fl == false) {
                                    ris.setColor(Color.RED);
                                }
                            }
                            ris.setFont(a);
                            ris.drawString(panelGrid.getGridElement(i, j).ToString(), x + W / 3 + 4, y + W / 2 + 10);
                            ris.setColor(Color.BLACK);
                            x += W + RAZ;
                        } else {
                            x += W + RAZ;
                        }
                    }
                }
                if (i % 2 == 0) {
                    x = 20;
                    y += H + RAZ;
                } else {
                    x = 20;
                    y += W + RAZ;
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 306, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables

    /**
     * @param a is a coordinate of a mouse click
     * @param b is a coordinate of a mouse click
     */
    void doubledClickMouse(int a, int b) {
        boolean flagForHit = true;
        a -= 20;
        b -= 10;
        final int W = 43;
        final int H = 8;
        final int RAZ = 5;
        if (a < 0 || b < 0) {
            flagForHit = false;
        }
        int matrx = 0;
        int matry = 0;
        while (a >= 0) {
            if (a - H >= 0) {
                a -= H;
                matry++;
            } else {
                break;
            }
            if (a - RAZ >= 0) {
                a -= RAZ;
            } else {
                break;
            }
            if (a - W >= 0) {
                a -= W;
                matry++;
            } else {
                break;
            }
            a -= RAZ;
        }
        while (b >= 0) {
            if (b - H >= 0) {
                b -= H;
                matrx++;
            } else {
                break;
            }
            if (b - RAZ >= 0) {
                b -= RAZ;
            } else {
                break;
            }
            if (b - W >= 0) {
                b -= W;
                matrx++;
            } else {
                break;
            }
            b -= RAZ;
        }
        if (matrx < panelGrid.getRow() && matry < panelGrid.getColumn() && flagForHit == true) {
            if (panelGrid.getGridElement(matrx, matry) instanceof Edge) {
                Edge edge = (Edge) panelGrid.getGridElement(matrx, matry);
                boolean flag = true;
                if (edge.getEdgeState() == EdgeState.PRESENT && flag == true) {
                    edge.setEdgeState("ABSENT");
                    flag = false;
                }
                if (edge.getEdgeState() == EdgeState.ABSENT && flag == true) {
                    edge.setEdgeState("ABSENT");
                    flag = false;
                }
                if (edge.getEdgeState() == EdgeState.UNDERTERMINED && flag == true) {
                    edge.setEdgeState("ABSENT");
                }
            }
        }
        this.repaint();
    }

    public void setGrid(Grid q) {
        panelGrid = q;
        this.repaint();
    }

    /**
     * 
     * @param a is a coordinate of a mouse click
     * @param b is a coordinate of a mouse click
     */
    public void clickMouse(int a, int b) {
        if (panelGrid == null) {
            throw new IllegalArgumentException("Grid is null");
        }

        CompoundCommand compoundCommand = new CompoundCommand();
        final int W = 43;
        final int H = 8;
        final int RAZ = 5;
        boolean flagForHit = true;
        a -= 20;
        b -= 10;
        if (a < 0 || b < 0) {
            flagForHit = false;
        }
        int matrx = 0;
        int matry = 0;
        while (a >= 0) {
            if (a - H >= 0) {
                a -= H;
                matry++;
            } else {
                break;
            }
            if (a - RAZ >= 0) {
                a -= RAZ;
            } else {
                break;
            }
            if (a - W >= 0) {
                a -= W;
                matry++;
            } else {
                break;
            }
            a -= RAZ;
        }
        while (b >= 0) {
            if (b - H >= 0) {
                b -= H;
                matrx++;
            } else {
                break;
            }
            if (b - RAZ >= 0) {
                b -= RAZ;
            } else {
                break;
            }
            if (b - W >= 0) {
                b -= W;
                matrx++;
            } else {
                break;
            }
            b -= RAZ;
        }
        if (matrx < panelGrid.getRow() && matry < panelGrid.getColumn() && flagForHit == true) {
            flagCheck = false;


            if (changeCell == true) {
                if (panelGrid.getGridElement(matrx, matry) instanceof Cell) {
                    Cell newCell = (Cell) panelGrid.getGridElement(matrx, matry);
                    newCell.setCellState(valueCell);
                }
            }
            
            if(editCell == true) {
                if (panelGrid.getGridElement(matrx, matry) instanceof Cell) {
                    Cell newCell = (Cell) panelGrid.getGridElement(matrx, matry);
                    newCell.setCellStatePlus();
                }                
            }


            if (panelGrid.getGridElement(matrx, matry) instanceof Edge) {
                Edge edge = (Edge) panelGrid.getGridElement(matrx, matry);

                boolean flag = true;
                if (edge.getEdgeState() == EdgeState.ABSENT && flag == true) {

                    final ChangeCommand command = new ChangeCommand(edge, EdgeState.PRESENT);
                    command.execute();
                    compoundCommand.addNewCommand(command);
                    undoStack.push(compoundCommand);

                    edge.setEdgeState(EdgeState.PRESENT.toString());
                    flag = false;
                }
                if (edge.getEdgeState() == EdgeState.PRESENT && flag == true) {

                    final ChangeCommand command = new ChangeCommand(edge, EdgeState.UNDERTERMINED);
                    command.execute();
                    compoundCommand.addNewCommand(command);
                    undoStack.push(compoundCommand);

                    edge.setEdgeState(EdgeState.UNDERTERMINED.toString());
                    flag = false;
                }
                if (edge.getEdgeState() == EdgeState.UNDERTERMINED && flag == true) {

                    final ChangeCommand command = new ChangeCommand(edge, EdgeState.PRESENT);
                    command.execute();
                    compoundCommand.addNewCommand(command);
                    undoStack.push(compoundCommand);

                    edge.setEdgeState(EdgeState.PRESENT.toString());
                }
                if (flagForStrategies == true) {
                    StrategyCell strategyCell;
                    strategyCell = new StrategyCell(panelGrid);
                    CompoundCommand newCompoundCommand1 = strategyCell.applyStrategyCell1(edge);
                    StrategyVertex strategyVertex;
                    strategyVertex = new StrategyVertex(panelGrid);
                    CompoundCommand newCompoundCommand2 = strategyVertex.applyStrategyVertex1(edge);
                    if (!newCompoundCommand2.getList().isEmpty()) {
                        for (int i = 0; i < newCompoundCommand2.getList().size(); i++) {
                            compoundCommand.addNewCommand(newCompoundCommand2.getList().get(i));
                        }
                    }
                    if (!newCompoundCommand1.getList().isEmpty()) {
                        for (int i = 0; i < newCompoundCommand1.getList().size(); i++) {
                            compoundCommand.addNewCommand(newCompoundCommand1.getList().get(i));
                        }
                    }
                    if (!compoundCommand.getList().isEmpty()) {
                        undoStack.push(compoundCommand);
                    }
                }
            }
        }

        this.repaint();
    }

    private void initPanel() {
        undoStack = new Stack<>();
        redoStack = new Stack<>();
    }

    public void Undo() {
        if (!undoStack.isEmpty()) {
            final CompoundCommand compoundCommand = undoStack.pop();
            for (int i = 0; i < compoundCommand.getList().size(); i++) {
                ChangeCommand changeCommand = compoundCommand.getList().get(i);
                if (changeCommand.isExecute() == false) {
                    changeCommand.execute();
                }
                changeCommand.undo();
            }
            redoStack.push(compoundCommand);
            this.repaint();
        }
    }

    public void Redo() {
        if (!redoStack.isEmpty()) {
            final CompoundCommand compoundCommand = redoStack.pop();
            for (int i = 0; i < compoundCommand.getList().size(); i++) {
                ChangeCommand changeCommand = compoundCommand.getList().get(i);
                changeCommand.execute();
                changeCommand.redo();
            }
            undoStack.push(compoundCommand);
        }
        this.repaint();
    }

    public void UndoAll() {
        while (!undoStack.isEmpty()) {
            final CompoundCommand compoundCommand = undoStack.pop();
            for (int i = 0; i < compoundCommand.getList().size(); i++) {
                ChangeCommand changeCommand = compoundCommand.getList().get(i);
                if (changeCommand.isExecute() == false) {
                    changeCommand.execute();
                }
                changeCommand.undo();
            }
            redoStack.push(compoundCommand);
        }
        this.repaint();
    }

    public void RedoAll() {
        while (!redoStack.isEmpty()) {
            final CompoundCommand compoundCommand = redoStack.pop();
            for (int i = 0; i < compoundCommand.getList().size(); i++) {
                ChangeCommand changeCommand = compoundCommand.getList().get(i);
                changeCommand.execute();
                changeCommand.redo();
            }
            undoStack.push(compoundCommand);
        }
        this.repaint();
    }

    /**
     * Solve the grid
     */
    public void Solve() {
        Backtracker back = new Backtracker(panelGrid);
        back.solveAll();
    }

    /**
     * Check grid for correct
     */
    public boolean Check() {
        Violation myViolation = new Violation(panelGrid);
        if (myViolation.checkGridForLoops() == true && panelGrid.Check() == true) {
            flagCheck = true;
        } else {
            flagCheck = false;
        }
        this.repaint();
        return flagCheck;
    }

    public void setFlagForStrategies(boolean a) {
        flagForStrategies = a;
    }

    public void setFlagForViolationA(boolean a) {
        flagForViolationA = a;
    }

    public void AppStrategyForAllGrid() throws NullPointerException {
        CompoundCommand compoundCommand1 = new CompoundCommand();
        CompoundCommand compoundCommand2 = new CompoundCommand();
        CompoundCommand returnCompountCommand = new CompoundCommand();

        StrategyCell strategyCell;
        strategyCell = new StrategyCell(panelGrid);
        compoundCommand1 = strategyCell.apply();
        StrategyVertex strategyVertex;
        strategyVertex = new StrategyVertex(panelGrid);
        compoundCommand2 = strategyVertex.apply();

        if (!compoundCommand1.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand1.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand1.getList().get(i));
            }
        }

        if (!compoundCommand2.getList().isEmpty()) {
            for (int i = 0; i < compoundCommand2.getList().size(); i++) {
                returnCompountCommand.addNewCommand(compoundCommand2.getList().get(i));
            }
        }

        if (!returnCompountCommand.getList().isEmpty()) {
            undoStack.push(returnCompountCommand);
        }

        this.repaint();
    }

    public void ViolationA(boolean a) {
        flagForViolationA = a;
        this.repaint();
    }

    public void ViolationB(boolean a) {
        flagForViolationB = a;
        this.repaint();
    }

    public void ViolationC(boolean a) {
        flagForViolationC = a;
        this.repaint();
    }

    public void ViolationD(boolean a) {
        flagForViolationD = a;
        this.repaint();
    }

    public void ViolationE(boolean a) {
        flagForViolationE = a;
        this.repaint();
    }

    /**
     * Add row to grid
     */
    public void addRow() {
        if (panelGrid == null) {
            throw new IllegalArgumentException("Grid is null");
        }
        panelGrid.addRow();
        this.repaint();
    }

    /**
     * Delete row from grid
     */
    public void deleteRow() {
        if (panelGrid == null) {
            throw new IllegalArgumentException("Grid is null");
        }
        panelGrid.deleteRow();
        this.repaint();
    }

    /**
     * Add column to grid
     */
    public void addColumn() {
        if (panelGrid == null) {
            throw new IllegalArgumentException("Grid is null");
        }
        panelGrid.addColumn();
        this.repaint();
    }

    /**
     * Delete column from grid
     */
    public void deleteColumn() {
        if (panelGrid == null) {
            throw new IllegalArgumentException("Grid is null");
        }
        panelGrid.deleteColumn();
        this.repaint();
    }

    /**
     * Change the cell
     * 
     * @param a is a flag for chanding cellstate
     * @param b is a new value of a cellstate
     */
    public void changeCell(boolean a, int b) {
        if (panelGrid == null) {
            throw new IllegalArgumentException("Grid is null");
        }
        valueCell = b;
        changeCell = a;
    }
    
    /**
     * Edit the cell
     * 
     * @param a is a flag for chanding cellstate
     */
    public void editCell(boolean a) {
        if (panelGrid == null) {
            throw new IllegalArgumentException("Grid is null");
        }
        editCell = a;
    }
}