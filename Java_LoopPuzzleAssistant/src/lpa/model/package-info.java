/**
 * Package with model elements that define the abstract state of a loop puzzle.
 *
 * Overview of functionality, and development status:
 * <ul>
 * <li> Model to create a grid and its restructuring
 *   TODO: create classes for elements of a looppuzzle;
 *   TODO: create methods for all classes;
 *   TODO: create testes;
 *   FIXME: 
 * <li>...
 * </ul>
 */
package lpa.model;
