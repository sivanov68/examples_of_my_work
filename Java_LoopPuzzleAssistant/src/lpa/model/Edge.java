package lpa.model;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class that stores face is to keep its view state
 */
public class Edge extends GridElement {
    //edgeState can be undetermined, present, absent

    /*
     * edgeState state of edge
     */
    private EdgeState edgeState;

    /**
     * Constructor of Edge
     * 
     * @param x is index of row
     * @param y is index of column
     * @param state is a state of edge
     * @pre {@code x >= 0 && y >= 0 && (state == PRESENT || state == ABSENT ||
     * state ==UNDERTERMINE)}
     */
    public Edge(int x, int y, String state) {
        super(x, y);
        this.edgeState = EdgeState.valueOf(state);
    }

    /**
     * Constructor of Edge
     * 
     * @param x is index of row
     * @param y is index of column
     * @pre {@code x >= 0 && y >= 0}
     */
    public Edge(int x, int y) {
        super(x, y);
        this.edgeState = EdgeState.ABSENT;
    }

    /**
     * @return state of edge
     */
    public EdgeState getEdgeState() {
        return edgeState;
    }

    /**
     * @param state is a state of edge
     */
    public void setEdgeState(String state) {
        this.edgeState = EdgeState.valueOf(state);
    }

    @Override
    public String ToString() {
        if (getEdgeState() == EdgeState.ABSENT) {
            return " ";
        }
        if (getEdgeState() == EdgeState.UNDERTERMINED) {
            return ".";
        }
        if (getEdgeState() == EdgeState.PRESENT) {
            if (x % 2 == 0) {
                return "-";
            } else {
                return "|";
            }
        }
        return null;
    }
}