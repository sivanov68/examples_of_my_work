package lpa.model;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class that stores value of cell and surrounding edge
 */
public class Cell extends GridElement {

    //if cellState = 4, it means that the number 
    //of edges can be anything
    /*
     * elementGroup is a container which stores around edges
     */
    public ElementGroup elementGroup;
    /*
     * cellState is a value of cell
     */
    private int cellState;

    /**
     * Constructor of Cell
     * 
     * @param x is index of row
     * @param y is index of column
     * @param cellState is a value of cell
     * @pre {@code x >= 0 && y >= 0 && 0 <= cellState <= 4}
     */
    public Cell(int x, int y, int cellState) {
        super(x, y);
        this.cellState = cellState;
        elementGroup = new ElementGroup();
    }

    /**
     * Constructor of Cell
     * 
     * @param x is index of row
     * @param y is index of column
     * @pre {@code x >= 0 && y >= 0}
     */
    public Cell(int x, int y) {
        super(x, y);
        this.cellState = 4;
        elementGroup = new ElementGroup();
    }

    /**
     * @return value of cell
     */
    public int getCellState() {
        return cellState;
    }

    /**
     * @param state is a value of cell
     * @pre {@code 0 <= cellState <= 4}
     */
    public void setCellState(int state) {
        this.cellState = state;
    }

    /**
     * Add surrounding edge to elementGroup
     *
     * @param edge is a surrounding edge
     * @pre {@code edge != null}
     */
    public void addEdge(Edge edge) {
        elementGroup.addToList(edge);
    }
    
    /**
     * Inc the cellState
     */
    public void setCellStatePlus() {
        switch (cellState) {
            case 0:
                cellState ++;
                break;
            case 1:
                cellState ++;
                break;
            case 2:
                cellState ++;
                break;
            case 3:
                cellState ++;
                break;
            case 4:
                cellState = 0;
                break;
        }
    }

    @Override
    public String ToString() {
        if (cellState == 4) {
            return " ";
        } else {
            return cellState + "";
        }
    }
}
