package lpa.model;

/**
 * @author Ibanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Enum stores possible states of edge
 */
public enum EdgeState {

    UNDERTERMINED, PRESENT, ABSENT
}
