package lpa.model;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Collection of edges
 */
public class ElementGroup implements Iterable<Edge> {

    /*
     * listOfEdge is a list, which stores edges
     */
    private ArrayList<Edge> listOfEdge;

    /**
     * Constructor of ElementGroup
     */
    public ElementGroup() {
        listOfEdge = new ArrayList<>();
    }

    /**
     * Add edge to list
     *
     * @param edge wich sent to list
     */
    public void addToList(Edge edge) {
        listOfEdge.add(edge);
    }

    /**
     * @return iterator
     */
    @Override
    public Iterator<Edge> iterator() {
        return listOfEdge.iterator();
    }

    /**
     * Represents count number of undetermined/present/absent edges in group
     *
     * @return Histogram of ElementGroup
     */
    public Histrogram kolvoEdges() {
        int undetermined = 0;
        int present = 0;
        int absent = 0;
        Histrogram histogram = new Histrogram(EdgeState.class);
        for (Edge j : listOfEdge) {
            if (j.getEdgeState() == EdgeState.UNDERTERMINED) {
                undetermined++;
            }
            if (j.getEdgeState() == EdgeState.ABSENT) {
                absent++;
            }
            if (j.getEdgeState() == EdgeState.PRESENT) {
                present++;
            }
        }
        histogram.put(EdgeState.ABSENT, absent);
        histogram.put(EdgeState.PRESENT, present);
        histogram.put(EdgeState.UNDERTERMINED, undetermined);
        return histogram;
    }

    /**
     * @return list, which stores edges
     */
    public ArrayList<Edge> getList() {
        return listOfEdge;
    }
}