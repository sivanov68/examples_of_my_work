package lpa.model;

import java.util.EnumMap;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class keeps edge which can be undetermined, present, absent
 */
public class Histrogram extends EnumMap<EdgeState, Integer> {

    public Histrogram(Class<EdgeState> keyType) {
        super(keyType);
    }
}
