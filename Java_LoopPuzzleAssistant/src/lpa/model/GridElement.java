package lpa.model;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Generalizes Edge, Cell, Vertex Has coordinates within the grid
 */
public class GridElement {

    /*
     * x index of row
     */
    protected int x;
    /*
     * y index of column
     */
    protected int y;

    /**
     * Constructor of GridElement
     * 
     * @param x index of row
     * @param y index of column
     */
    public GridElement(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String ToString() {
        return "";
    }
}
