package lpa.model;

/**
 * @author Ivanov Sergey
 * Date: 07.12.2012 21:30
 */
/**
 * Class that stores the intersection of edges
 */
public class Vertex extends GridElement {

    /*
     * elementGroup is a container which stores around edges
     */
    public ElementGroup elementGroup;

    /**
     * Constructor of Vertex
     *
     * @param x is index of row
     * @param y if index of column
     */
    public Vertex(int x, int y) {
        super(x, y);
        elementGroup = new ElementGroup();
    }

    /**
     * Add edge to vertex
     *
     * @pre {@code edge != null}
     * @param edge edge to add to list
     */
    public void addEdge(Edge edge) {
        elementGroup.addToList(edge);
    }

    @Override
    public String ToString() {
        return "+";
    }
}
