/**
* The Loop Puzzle Assistant application.
*
* Overview of functionality, and development status:
* <ul>
* <li>Model classes to create and change a loop puzzle grid
* TODO: check rule violation;
* TODO: add further test cases;
* <li>...
* </ul>
*/

package lpa;